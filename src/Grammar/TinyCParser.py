# Generated from ./Grammar/TinyC.g4 by ANTLR 4.5.2
# encoding: utf-8
from __future__ import print_function
from antlr4 import *
from io import StringIO

def serializedATN():
    with StringIO() as buf:
        buf.write(u"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3")
        buf.write(u"<\u0165\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t")
        buf.write(u"\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r\t\r")
        buf.write(u"\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22\4")
        buf.write(u"\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30")
        buf.write(u"\t\30\4\31\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t")
        buf.write(u"\35\4\36\t\36\4\37\t\37\4 \t \3\2\3\2\3\2\3\3\3\3\3\3")
        buf.write(u"\3\3\7\3H\n\3\f\3\16\3K\13\3\3\4\3\4\3\4\3\4\3\4\3\4")
        buf.write(u"\3\4\3\5\3\5\3\6\3\6\3\7\3\7\3\7\5\7[\n\7\3\b\3\b\3\t")
        buf.write(u"\3\t\5\ta\n\t\3\n\3\n\3\n\5\nf\n\n\3\13\3\13\5\13j\n")
        buf.write(u"\13\3\f\3\f\5\fn\n\f\3\f\3\f\3\r\3\r\3\r\3\r\3\r\3\r")
        buf.write(u"\3\r\3\r\3\r\3\r\5\r|\n\r\3\16\3\16\3\16\7\16\u0081\n")
        buf.write(u"\16\f\16\16\16\u0084\13\16\5\16\u0086\n\16\3\16\5\16")
        buf.write(u"\u0089\n\16\3\17\3\17\3\17\5\17\u008e\n\17\3\20\7\20")
        buf.write(u"\u0091\n\20\f\20\16\20\u0094\13\20\3\21\3\21\5\21\u0098")
        buf.write(u"\n\21\3\21\3\21\5\21\u009c\n\21\3\21\3\21\5\21\u00a0")
        buf.write(u"\n\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3\21\3")
        buf.write(u"\21\3\21\3\21\3\21\5\21\u00af\n\21\3\22\3\22\3\22\3\22")
        buf.write(u"\3\22\3\22\3\22\3\22\3\22\5\22\u00ba\n\22\3\22\5\22\u00bd")
        buf.write(u"\n\22\3\23\3\23\3\23\3\23\3\23\3\23\5\23\u00c5\n\23\3")
        buf.write(u"\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\3\24\5\24\u00d0")
        buf.write(u"\n\24\3\25\3\25\3\25\5\25\u00d5\n\25\3\25\5\25\u00d8")
        buf.write(u"\n\25\3\25\3\25\3\25\5\25\u00dd\n\25\3\25\3\25\3\25\3")
        buf.write(u"\25\3\25\3\25\5\25\u00e5\n\25\3\26\3\26\5\26\u00e9\n")
        buf.write(u"\26\3\27\3\27\3\27\3\27\3\27\5\27\u00f0\n\27\3\30\3\30")
        buf.write(u"\3\30\3\30\3\30\3\30\7\30\u00f8\n\30\f\30\16\30\u00fb")
        buf.write(u"\13\30\3\31\3\31\3\31\3\31\3\31\3\31\7\31\u0103\n\31")
        buf.write(u"\f\31\16\31\u0106\13\31\3\32\3\32\3\32\3\32\3\32\3\32")
        buf.write(u"\7\32\u010e\n\32\f\32\16\32\u0111\13\32\3\33\3\33\3\33")
        buf.write(u"\3\33\3\33\3\33\7\33\u0119\n\33\f\33\16\33\u011c\13\33")
        buf.write(u"\3\34\3\34\3\34\3\34\3\34\3\34\7\34\u0124\n\34\f\34\16")
        buf.write(u"\34\u0127\13\34\3\35\3\35\3\35\3\35\3\35\5\35\u012e\n")
        buf.write(u"\35\3\35\3\35\5\35\u0132\n\35\3\35\3\35\3\35\5\35\u0137")
        buf.write(u"\n\35\7\35\u0139\n\35\f\35\16\35\u013c\13\35\3\36\3\36")
        buf.write(u"\3\36\5\36\u0141\n\36\3\36\3\36\5\36\u0145\n\36\3\37")
        buf.write(u"\3\37\3\37\7\37\u014a\n\37\f\37\16\37\u014d\13\37\3 ")
        buf.write(u"\5 \u0150\n \3 \3 \5 \u0154\n \3 \3 \3 \3 \3 \3 \3 \3")
        buf.write(u" \3 \3 \3 \3 \3 \5 \u0163\n \3 \2\b.\60\62\64\668!\2")
        buf.write(u"\4\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$&(*,.\60\62")
        buf.write(u"\64\668:<>\2\b\6\2\3\3\7\b\13\13\r\r\3\2\',\4\2\25\30")
        buf.write(u"-.\4\2\31\31\33\33\4\2\35\35\37 \3\2\35\36\u017f\2@\3")
        buf.write(u"\2\2\2\4I\3\2\2\2\6L\3\2\2\2\bS\3\2\2\2\nU\3\2\2\2\f")
        buf.write(u"W\3\2\2\2\16\\\3\2\2\2\20^\3\2\2\2\22b\3\2\2\2\24g\3")
        buf.write(u"\2\2\2\26k\3\2\2\2\30q\3\2\2\2\32\u0088\3\2\2\2\34\u008a")
        buf.write(u"\3\2\2\2\36\u0092\3\2\2\2 \u00ae\3\2\2\2\"\u00b0\3\2")
        buf.write(u"\2\2$\u00be\3\2\2\2&\u00c6\3\2\2\2(\u00d1\3\2\2\2*\u00e6")
        buf.write(u"\3\2\2\2,\u00ef\3\2\2\2.\u00f1\3\2\2\2\60\u00fc\3\2\2")
        buf.write(u"\2\62\u0107\3\2\2\2\64\u0112\3\2\2\2\66\u011d\3\2\2\2")
        buf.write(u"8\u0131\3\2\2\2:\u0144\3\2\2\2<\u0146\3\2\2\2>\u0162")
        buf.write(u"\3\2\2\2@A\5\4\3\2AB\7\2\2\3B\3\3\2\2\2CH\5\b\5\2DH\5")
        buf.write(u"\6\4\2EH\5\n\6\2FH\5\30\r\2GC\3\2\2\2GD\3\2\2\2GE\3\2")
        buf.write(u"\2\2GF\3\2\2\2HK\3\2\2\2IG\3\2\2\2IJ\3\2\2\2J\5\3\2\2")
        buf.write(u"\2KI\3\2\2\2LM\5\16\b\2MN\7\64\2\2NO\7\17\2\2OP\5\32")
        buf.write(u"\16\2PQ\7\20\2\2QR\7%\2\2R\7\3\2\2\2ST\7\61\2\2T\t\3")
        buf.write(u"\2\2\2UV\5\f\7\2V\13\3\2\2\2WX\5\16\b\2XZ\5\22\n\2Y[")
        buf.write(u"\7%\2\2ZY\3\2\2\2Z[\3\2\2\2[\r\3\2\2\2\\]\5\20\t\2]\17")
        buf.write(u"\3\2\2\2^`\t\2\2\2_a\7\35\2\2`_\3\2\2\2`a\3\2\2\2a\21")
        buf.write(u"\3\2\2\2be\5\24\13\2cd\7\'\2\2df\5,\27\2ec\3\2\2\2ef")
        buf.write(u"\3\2\2\2f\23\3\2\2\2gi\7\64\2\2hj\5\26\f\2ih\3\2\2\2")
        buf.write(u"ij\3\2\2\2j\25\3\2\2\2km\7\21\2\2ln\5,\27\2ml\3\2\2\2")
        buf.write(u"mn\3\2\2\2no\3\2\2\2op\7\22\2\2p\27\3\2\2\2qr\5\16\b")
        buf.write(u"\2rs\7\64\2\2st\7\17\2\2tu\5\32\16\2u{\7\20\2\2vw\7\23")
        buf.write(u"\2\2wx\5\36\20\2xy\7\24\2\2y|\3\2\2\2z|\7%\2\2{v\3\2")
        buf.write(u"\2\2{z\3\2\2\2|\31\3\2\2\2}\u0082\5\34\17\2~\177\7&\2")
        buf.write(u"\2\177\u0081\5\34\17\2\u0080~\3\2\2\2\u0081\u0084\3\2")
        buf.write(u"\2\2\u0082\u0080\3\2\2\2\u0082\u0083\3\2\2\2\u0083\u0086")
        buf.write(u"\3\2\2\2\u0084\u0082\3\2\2\2\u0085}\3\2\2\2\u0085\u0086")
        buf.write(u"\3\2\2\2\u0086\u0089\3\2\2\2\u0087\u0089\7\r\2\2\u0088")
        buf.write(u"\u0085\3\2\2\2\u0088\u0087\3\2\2\2\u0089\33\3\2\2\2\u008a")
        buf.write(u"\u008b\5\16\b\2\u008b\u008d\7\64\2\2\u008c\u008e\5\26")
        buf.write(u"\f\2\u008d\u008c\3\2\2\2\u008d\u008e\3\2\2\2\u008e\35")
        buf.write(u"\3\2\2\2\u008f\u0091\5 \21\2\u0090\u008f\3\2\2\2\u0091")
        buf.write(u"\u0094\3\2\2\2\u0092\u0090\3\2\2\2\u0092\u0093\3\2\2")
        buf.write(u"\2\u0093\37\3\2\2\2\u0094\u0092\3\2\2\2\u0095\u0097\5")
        buf.write(u"\"\22\2\u0096\u0098\7%\2\2\u0097\u0096\3\2\2\2\u0097")
        buf.write(u"\u0098\3\2\2\2\u0098\u00af\3\2\2\2\u0099\u009b\5&\24")
        buf.write(u"\2\u009a\u009c\7%\2\2\u009b\u009a\3\2\2\2\u009b\u009c")
        buf.write(u"\3\2\2\2\u009c\u00af\3\2\2\2\u009d\u009f\5(\25\2\u009e")
        buf.write(u"\u00a0\7%\2\2\u009f\u009e\3\2\2\2\u009f\u00a0\3\2\2\2")
        buf.write(u"\u00a0\u00af\3\2\2\2\u00a1\u00a2\5*\26\2\u00a2\u00a3")
        buf.write(u"\7%\2\2\u00a3\u00af\3\2\2\2\u00a4\u00a5\5,\27\2\u00a5")
        buf.write(u"\u00a6\7%\2\2\u00a6\u00af\3\2\2\2\u00a7\u00a8\5\n\6\2")
        buf.write(u"\u00a8\u00a9\7%\2\2\u00a9\u00af\3\2\2\2\u00aa\u00ab\7")
        buf.write(u"\62\2\2\u00ab\u00af\7%\2\2\u00ac\u00ad\7\63\2\2\u00ad")
        buf.write(u"\u00af\7%\2\2\u00ae\u0095\3\2\2\2\u00ae\u0099\3\2\2\2")
        buf.write(u"\u00ae\u009d\3\2\2\2\u00ae\u00a1\3\2\2\2\u00ae\u00a4")
        buf.write(u"\3\2\2\2\u00ae\u00a7\3\2\2\2\u00ae\u00aa\3\2\2\2\u00ae")
        buf.write(u"\u00ac\3\2\2\2\u00af!\3\2\2\2\u00b0\u00b1\7\6\2\2\u00b1")
        buf.write(u"\u00b2\7\17\2\2\u00b2\u00b3\5,\27\2\u00b3\u00b9\7\20")
        buf.write(u"\2\2\u00b4\u00ba\5 \21\2\u00b5\u00b6\7\23\2\2\u00b6\u00b7")
        buf.write(u"\5\36\20\2\u00b7\u00b8\7\24\2\2\u00b8\u00ba\3\2\2\2\u00b9")
        buf.write(u"\u00b4\3\2\2\2\u00b9\u00b5\3\2\2\2\u00ba\u00bc\3\2\2")
        buf.write(u"\2\u00bb\u00bd\5$\23\2\u00bc\u00bb\3\2\2\2\u00bc\u00bd")
        buf.write(u"\3\2\2\2\u00bd#\3\2\2\2\u00be\u00c4\7\4\2\2\u00bf\u00c5")
        buf.write(u"\5 \21\2\u00c0\u00c1\7\23\2\2\u00c1\u00c2\5\36\20\2\u00c2")
        buf.write(u"\u00c3\7\24\2\2\u00c3\u00c5\3\2\2\2\u00c4\u00bf\3\2\2")
        buf.write(u"\2\u00c4\u00c0\3\2\2\2\u00c5%\3\2\2\2\u00c6\u00c7\7\16")
        buf.write(u"\2\2\u00c7\u00c8\7\17\2\2\u00c8\u00c9\5,\27\2\u00c9\u00cf")
        buf.write(u"\7\20\2\2\u00ca\u00d0\5 \21\2\u00cb\u00cc\7\23\2\2\u00cc")
        buf.write(u"\u00cd\5\36\20\2\u00cd\u00ce\7\24\2\2\u00ce\u00d0\3\2")
        buf.write(u"\2\2\u00cf\u00ca\3\2\2\2\u00cf\u00cb\3\2\2\2\u00d0\'")
        buf.write(u"\3\2\2\2\u00d1\u00d2\7\5\2\2\u00d2\u00d4\7\17\2\2\u00d3")
        buf.write(u"\u00d5\5 \21\2\u00d4\u00d3\3\2\2\2\u00d4\u00d5\3\2\2")
        buf.write(u"\2\u00d5\u00d7\3\2\2\2\u00d6\u00d8\7%\2\2\u00d7\u00d6")
        buf.write(u"\3\2\2\2\u00d7\u00d8\3\2\2\2\u00d8\u00d9\3\2\2\2\u00d9")
        buf.write(u"\u00da\5,\27\2\u00da\u00dc\7%\2\2\u00db\u00dd\5,\27\2")
        buf.write(u"\u00dc\u00db\3\2\2\2\u00dc\u00dd\3\2\2\2\u00dd\u00de")
        buf.write(u"\3\2\2\2\u00de\u00e4\7\20\2\2\u00df\u00e5\5 \21\2\u00e0")
        buf.write(u"\u00e1\7\23\2\2\u00e1\u00e2\5\36\20\2\u00e2\u00e3\7\24")
        buf.write(u"\2\2\u00e3\u00e5\3\2\2\2\u00e4\u00df\3\2\2\2\u00e4\u00e0")
        buf.write(u"\3\2\2\2\u00e5)\3\2\2\2\u00e6\u00e8\7\f\2\2\u00e7\u00e9")
        buf.write(u"\5,\27\2\u00e8\u00e7\3\2\2\2\u00e8\u00e9\3\2\2\2\u00e9")
        buf.write(u"+\3\2\2\2\u00ea\u00f0\5.\30\2\u00eb\u00ec\5> \2\u00ec")
        buf.write(u"\u00ed\t\3\2\2\u00ed\u00ee\5,\27\2\u00ee\u00f0\3\2\2")
        buf.write(u"\2\u00ef\u00ea\3\2\2\2\u00ef\u00eb\3\2\2\2\u00f0-\3\2")
        buf.write(u"\2\2\u00f1\u00f2\b\30\1\2\u00f2\u00f3\5\60\31\2\u00f3")
        buf.write(u"\u00f9\3\2\2\2\u00f4\u00f5\f\3\2\2\u00f5\u00f6\7\"\2")
        buf.write(u"\2\u00f6\u00f8\5\60\31\2\u00f7\u00f4\3\2\2\2\u00f8\u00fb")
        buf.write(u"\3\2\2\2\u00f9\u00f7\3\2\2\2\u00f9\u00fa\3\2\2\2\u00fa")
        buf.write(u"/\3\2\2\2\u00fb\u00f9\3\2\2\2\u00fc\u00fd\b\31\1\2\u00fd")
        buf.write(u"\u00fe\5\62\32\2\u00fe\u0104\3\2\2\2\u00ff\u0100\f\3")
        buf.write(u"\2\2\u0100\u0101\7!\2\2\u0101\u0103\5\62\32\2\u0102\u00ff")
        buf.write(u"\3\2\2\2\u0103\u0106\3\2\2\2\u0104\u0102\3\2\2\2\u0104")
        buf.write(u"\u0105\3\2\2\2\u0105\61\3\2\2\2\u0106\u0104\3\2\2\2\u0107")
        buf.write(u"\u0108\b\32\1\2\u0108\u0109\5\64\33\2\u0109\u010f\3\2")
        buf.write(u"\2\2\u010a\u010b\f\3\2\2\u010b\u010c\t\4\2\2\u010c\u010e")
        buf.write(u"\5\64\33\2\u010d\u010a\3\2\2\2\u010e\u0111\3\2\2\2\u010f")
        buf.write(u"\u010d\3\2\2\2\u010f\u0110\3\2\2\2\u0110\63\3\2\2\2\u0111")
        buf.write(u"\u010f\3\2\2\2\u0112\u0113\b\33\1\2\u0113\u0114\5\66")
        buf.write(u"\34\2\u0114\u011a\3\2\2\2\u0115\u0116\f\3\2\2\u0116\u0117")
        buf.write(u"\t\5\2\2\u0117\u0119\5\66\34\2\u0118\u0115\3\2\2\2\u0119")
        buf.write(u"\u011c\3\2\2\2\u011a\u0118\3\2\2\2\u011a\u011b\3\2\2")
        buf.write(u"\2\u011b\65\3\2\2\2\u011c\u011a\3\2\2\2\u011d\u011e\b")
        buf.write(u"\34\1\2\u011e\u011f\58\35\2\u011f\u0125\3\2\2\2\u0120")
        buf.write(u"\u0121\f\3\2\2\u0121\u0122\t\6\2\2\u0122\u0124\58\35")
        buf.write(u"\2\u0123\u0120\3\2\2\2\u0124\u0127\3\2\2\2\u0125\u0123")
        buf.write(u"\3\2\2\2\u0125\u0126\3\2\2\2\u0126\67\3\2\2\2\u0127\u0125")
        buf.write(u"\3\2\2\2\u0128\u012d\b\35\1\2\u0129\u012e\7\33\2\2\u012a")
        buf.write(u"\u012e\7$\2\2\u012b\u012e\7\32\2\2\u012c\u012e\7\34\2")
        buf.write(u"\2\u012d\u0129\3\2\2\2\u012d\u012a\3\2\2\2\u012d\u012b")
        buf.write(u"\3\2\2\2\u012d\u012c\3\2\2\2\u012e\u012f\3\2\2\2\u012f")
        buf.write(u"\u0132\58\35\4\u0130\u0132\5:\36\2\u0131\u0128\3\2\2")
        buf.write(u"\2\u0131\u0130\3\2\2\2\u0132\u013a\3\2\2\2\u0133\u0136")
        buf.write(u"\f\3\2\2\u0134\u0137\7\32\2\2\u0135\u0137\7\34\2\2\u0136")
        buf.write(u"\u0134\3\2\2\2\u0136\u0135\3\2\2\2\u0137\u0139\3\2\2")
        buf.write(u"\2\u0138\u0133\3\2\2\2\u0139\u013c\3\2\2\2\u013a\u0138")
        buf.write(u"\3\2\2\2\u013a\u013b\3\2\2\2\u013b9\3\2\2\2\u013c\u013a")
        buf.write(u"\3\2\2\2\u013d\u013e\7\64\2\2\u013e\u0140\7\17\2\2\u013f")
        buf.write(u"\u0141\5<\37\2\u0140\u013f\3\2\2\2\u0140\u0141\3\2\2")
        buf.write(u"\2\u0141\u0142\3\2\2\2\u0142\u0145\7\20\2\2\u0143\u0145")
        buf.write(u"\5> \2\u0144\u013d\3\2\2\2\u0144\u0143\3\2\2\2\u0145")
        buf.write(u";\3\2\2\2\u0146\u014b\5,\27\2\u0147\u0148\7&\2\2\u0148")
        buf.write(u"\u014a\5,\27\2\u0149\u0147\3\2\2\2\u014a\u014d\3\2\2")
        buf.write(u"\2\u014b\u0149\3\2\2\2\u014b\u014c\3\2\2\2\u014c=\3\2")
        buf.write(u"\2\2\u014d\u014b\3\2\2\2\u014e\u0150\t\7\2\2\u014f\u014e")
        buf.write(u"\3\2\2\2\u014f\u0150\3\2\2\2\u0150\u0151\3\2\2\2\u0151")
        buf.write(u"\u0153\7\64\2\2\u0152\u0154\5\26\f\2\u0153\u0152\3\2")
        buf.write(u"\2\2\u0153\u0154\3\2\2\2\u0154\u0163\3\2\2\2\u0155\u0163")
        buf.write(u"\7\65\2\2\u0156\u0163\7\67\2\2\u0157\u0163\78\2\2\u0158")
        buf.write(u"\u0163\7\t\2\2\u0159\u0163\7\n\2\2\u015a\u015b\7\17\2")
        buf.write(u"\2\u015b\u015c\5,\27\2\u015c\u015d\7\20\2\2\u015d\u0163")
        buf.write(u"\3\2\2\2\u015e\u015f\7\23\2\2\u015f\u0160\5<\37\2\u0160")
        buf.write(u"\u0161\7\24\2\2\u0161\u0163\3\2\2\2\u0162\u014f\3\2\2")
        buf.write(u"\2\u0162\u0155\3\2\2\2\u0162\u0156\3\2\2\2\u0162\u0157")
        buf.write(u"\3\2\2\2\u0162\u0158\3\2\2\2\u0162\u0159\3\2\2\2\u0162")
        buf.write(u"\u015a\3\2\2\2\u0162\u015e\3\2\2\2\u0163?\3\2\2\2,GI")
        buf.write(u"Z`eim{\u0082\u0085\u0088\u008d\u0092\u0097\u009b\u009f")
        buf.write(u"\u00ae\u00b9\u00bc\u00c4\u00cf\u00d4\u00d7\u00dc\u00e4")
        buf.write(u"\u00e8\u00ef\u00f9\u0104\u010f\u011a\u0125\u012d\u0131")
        buf.write(u"\u0136\u013a\u0140\u0144\u014b\u014f\u0153\u0162")
        return buf.getvalue()


class TinyCParser ( Parser ):

    grammarFileName = "TinyC.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [ u"<INVALID>", u"'char'", u"'else'", u"'for'", u"'if'", 
                     u"'bool'", u"'int'", u"'true'", u"'false'", u"'float'", 
                     u"'return'", u"'void'", u"'while'", u"'('", u"')'", 
                     u"'['", u"']'", u"'{'", u"'}'", u"'<'", u"'<='", u"'>'", 
                     u"'>='", u"'+'", u"'++'", u"'-'", u"'--'", u"'*'", 
                     u"'&'", u"'/'", u"'%'", u"'&&'", u"'||'", u"'^'", u"'!'", 
                     u"';'", u"','", u"'='", u"'*='", u"'/='", u"'%='", 
                     u"'+='", u"'-='", u"'=='", u"'!='", u"'->'", u"'.'", 
                     u"'#include <stdio.h>'", u"'continue'", u"'break'" ]

    symbolicNames = [ u"<INVALID>", u"Char", u"Else", u"For", u"If", u"Bool", 
                      u"Int", u"MTrue", u"MFalse", u"Float", u"Return", 
                      u"Void", u"While", u"LeftParen", u"RightParen", u"LeftBracket", 
                      u"RightBracket", u"LeftBrace", u"RightBrace", u"Less", 
                      u"LessEqual", u"Greater", u"GreaterEqual", u"Plus", 
                      u"PlusPlus", u"Minus", u"MinusMinus", u"Star", u"Ref", 
                      u"Div", u"Mod", u"AndAnd", u"OrOr", u"Caret", u"Not", 
                      u"Semi", u"Comma", u"Assign", u"StarAssign", u"DivAssign", 
                      u"ModAssign", u"PlusAssign", u"MinusAssign", u"Equal", 
                      u"NotEqual", u"Arrow", u"Dot", u"Include", u"Continue", 
                      u"Break", u"Identifier", u"Number", u"Boolean", u"String", 
                      u"Character", u"Whitespace", u"Newline", u"BlockComment", 
                      u"LineComment" ]

    RULE_parse = 0
    RULE_compilation_unit = 1
    RULE_forward_declaration = 2
    RULE_m_include = 3
    RULE_declaration = 4
    RULE_simpleDecl = 5
    RULE_m_type = 6
    RULE_varType = 7
    RULE_variable_decl = 8
    RULE_variable_id = 9
    RULE_array_index = 10
    RULE_function_declaration = 11
    RULE_parameter_list = 12
    RULE_parameter = 13
    RULE_block = 14
    RULE_statement = 15
    RULE_if_statement = 16
    RULE_else_statement = 17
    RULE_while_statement = 18
    RULE_for_statement = 19
    RULE_return_statement = 20
    RULE_expression = 21
    RULE_or_expr = 22
    RULE_and_expr = 23
    RULE_rel_expr = 24
    RULE_add_expr = 25
    RULE_mult_expr = 26
    RULE_unary_expr = 27
    RULE_pf_expr = 28
    RULE_arg_list = 29
    RULE_pr_expr = 30

    ruleNames =  [ u"parse", u"compilation_unit", u"forward_declaration", 
                   u"m_include", u"declaration", u"simpleDecl", u"m_type", 
                   u"varType", u"variable_decl", u"variable_id", u"array_index", 
                   u"function_declaration", u"parameter_list", u"parameter", 
                   u"block", u"statement", u"if_statement", u"else_statement", 
                   u"while_statement", u"for_statement", u"return_statement", 
                   u"expression", u"or_expr", u"and_expr", u"rel_expr", 
                   u"add_expr", u"mult_expr", u"unary_expr", u"pf_expr", 
                   u"arg_list", u"pr_expr" ]

    EOF = Token.EOF
    Char=1
    Else=2
    For=3
    If=4
    Bool=5
    Int=6
    MTrue=7
    MFalse=8
    Float=9
    Return=10
    Void=11
    While=12
    LeftParen=13
    RightParen=14
    LeftBracket=15
    RightBracket=16
    LeftBrace=17
    RightBrace=18
    Less=19
    LessEqual=20
    Greater=21
    GreaterEqual=22
    Plus=23
    PlusPlus=24
    Minus=25
    MinusMinus=26
    Star=27
    Ref=28
    Div=29
    Mod=30
    AndAnd=31
    OrOr=32
    Caret=33
    Not=34
    Semi=35
    Comma=36
    Assign=37
    StarAssign=38
    DivAssign=39
    ModAssign=40
    PlusAssign=41
    MinusAssign=42
    Equal=43
    NotEqual=44
    Arrow=45
    Dot=46
    Include=47
    Continue=48
    Break=49
    Identifier=50
    Number=51
    Boolean=52
    String=53
    Character=54
    Whitespace=55
    Newline=56
    BlockComment=57
    LineComment=58

    def __init__(self, input):
        super(TinyCParser, self).__init__(input)
        self.checkVersion("4.5.2")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None



    class ParseContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(TinyCParser.ParseContext, self).__init__(parent, invokingState)
            self.parser = parser

        def compilation_unit(self):
            return self.getTypedRuleContext(TinyCParser.Compilation_unitContext,0)


        def EOF(self):
            return self.getToken(TinyCParser.EOF, 0)

        def getRuleIndex(self):
            return TinyCParser.RULE_parse

        def enterRule(self, listener):
            if hasattr(listener, "enterParse"):
                listener.enterParse(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitParse"):
                listener.exitParse(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitParse"):
                return visitor.visitParse(self)
            else:
                return visitor.visitChildren(self)




    def parse(self):

        localctx = TinyCParser.ParseContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_parse)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 62
            self.compilation_unit()
            self.state = 63
            self.match(TinyCParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Compilation_unitContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(TinyCParser.Compilation_unitContext, self).__init__(parent, invokingState)
            self.parser = parser

        def m_include(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(TinyCParser.M_includeContext)
            else:
                return self.getTypedRuleContext(TinyCParser.M_includeContext,i)


        def forward_declaration(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(TinyCParser.Forward_declarationContext)
            else:
                return self.getTypedRuleContext(TinyCParser.Forward_declarationContext,i)


        def declaration(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(TinyCParser.DeclarationContext)
            else:
                return self.getTypedRuleContext(TinyCParser.DeclarationContext,i)


        def function_declaration(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(TinyCParser.Function_declarationContext)
            else:
                return self.getTypedRuleContext(TinyCParser.Function_declarationContext,i)


        def getRuleIndex(self):
            return TinyCParser.RULE_compilation_unit

        def enterRule(self, listener):
            if hasattr(listener, "enterCompilation_unit"):
                listener.enterCompilation_unit(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitCompilation_unit"):
                listener.exitCompilation_unit(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitCompilation_unit"):
                return visitor.visitCompilation_unit(self)
            else:
                return visitor.visitChildren(self)




    def compilation_unit(self):

        localctx = TinyCParser.Compilation_unitContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_compilation_unit)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 71
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << TinyCParser.Char) | (1 << TinyCParser.Bool) | (1 << TinyCParser.Int) | (1 << TinyCParser.Float) | (1 << TinyCParser.Void) | (1 << TinyCParser.Include))) != 0):
                self.state = 69
                self._errHandler.sync(self);
                la_ = self._interp.adaptivePredict(self._input,0,self._ctx)
                if la_ == 1:
                    self.state = 65
                    self.m_include()
                    pass

                elif la_ == 2:
                    self.state = 66
                    self.forward_declaration()
                    pass

                elif la_ == 3:
                    self.state = 67
                    self.declaration()
                    pass

                elif la_ == 4:
                    self.state = 68
                    self.function_declaration()
                    pass


                self.state = 73
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Forward_declarationContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(TinyCParser.Forward_declarationContext, self).__init__(parent, invokingState)
            self.parser = parser

        def m_type(self):
            return self.getTypedRuleContext(TinyCParser.M_typeContext,0)


        def Identifier(self):
            return self.getToken(TinyCParser.Identifier, 0)

        def LeftParen(self):
            return self.getToken(TinyCParser.LeftParen, 0)

        def parameter_list(self):
            return self.getTypedRuleContext(TinyCParser.Parameter_listContext,0)


        def RightParen(self):
            return self.getToken(TinyCParser.RightParen, 0)

        def Semi(self):
            return self.getToken(TinyCParser.Semi, 0)

        def getRuleIndex(self):
            return TinyCParser.RULE_forward_declaration

        def enterRule(self, listener):
            if hasattr(listener, "enterForward_declaration"):
                listener.enterForward_declaration(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitForward_declaration"):
                listener.exitForward_declaration(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitForward_declaration"):
                return visitor.visitForward_declaration(self)
            else:
                return visitor.visitChildren(self)




    def forward_declaration(self):

        localctx = TinyCParser.Forward_declarationContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_forward_declaration)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 74
            self.m_type()
            self.state = 75
            self.match(TinyCParser.Identifier)
            self.state = 76
            self.match(TinyCParser.LeftParen)
            self.state = 77
            self.parameter_list()
            self.state = 78
            self.match(TinyCParser.RightParen)
            self.state = 79
            self.match(TinyCParser.Semi)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class M_includeContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(TinyCParser.M_includeContext, self).__init__(parent, invokingState)
            self.parser = parser

        def Include(self):
            return self.getToken(TinyCParser.Include, 0)

        def getRuleIndex(self):
            return TinyCParser.RULE_m_include

        def enterRule(self, listener):
            if hasattr(listener, "enterM_include"):
                listener.enterM_include(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitM_include"):
                listener.exitM_include(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitM_include"):
                return visitor.visitM_include(self)
            else:
                return visitor.visitChildren(self)




    def m_include(self):

        localctx = TinyCParser.M_includeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 6, self.RULE_m_include)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 81
            self.match(TinyCParser.Include)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class DeclarationContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(TinyCParser.DeclarationContext, self).__init__(parent, invokingState)
            self.parser = parser

        def simpleDecl(self):
            return self.getTypedRuleContext(TinyCParser.SimpleDeclContext,0)


        def getRuleIndex(self):
            return TinyCParser.RULE_declaration

        def enterRule(self, listener):
            if hasattr(listener, "enterDeclaration"):
                listener.enterDeclaration(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitDeclaration"):
                listener.exitDeclaration(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitDeclaration"):
                return visitor.visitDeclaration(self)
            else:
                return visitor.visitChildren(self)




    def declaration(self):

        localctx = TinyCParser.DeclarationContext(self, self._ctx, self.state)
        self.enterRule(localctx, 8, self.RULE_declaration)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 83
            self.simpleDecl()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class SimpleDeclContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(TinyCParser.SimpleDeclContext, self).__init__(parent, invokingState)
            self.parser = parser

        def m_type(self):
            return self.getTypedRuleContext(TinyCParser.M_typeContext,0)


        def variable_decl(self):
            return self.getTypedRuleContext(TinyCParser.Variable_declContext,0)


        def Semi(self):
            return self.getToken(TinyCParser.Semi, 0)

        def getRuleIndex(self):
            return TinyCParser.RULE_simpleDecl

        def enterRule(self, listener):
            if hasattr(listener, "enterSimpleDecl"):
                listener.enterSimpleDecl(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitSimpleDecl"):
                listener.exitSimpleDecl(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitSimpleDecl"):
                return visitor.visitSimpleDecl(self)
            else:
                return visitor.visitChildren(self)




    def simpleDecl(self):

        localctx = TinyCParser.SimpleDeclContext(self, self._ctx, self.state)
        self.enterRule(localctx, 10, self.RULE_simpleDecl)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 85
            self.m_type()
            self.state = 86
            self.variable_decl()
            self.state = 88
            self._errHandler.sync(self);
            la_ = self._interp.adaptivePredict(self._input,2,self._ctx)
            if la_ == 1:
                self.state = 87
                self.match(TinyCParser.Semi)


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class M_typeContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(TinyCParser.M_typeContext, self).__init__(parent, invokingState)
            self.parser = parser

        def varType(self):
            return self.getTypedRuleContext(TinyCParser.VarTypeContext,0)


        def getRuleIndex(self):
            return TinyCParser.RULE_m_type

        def enterRule(self, listener):
            if hasattr(listener, "enterM_type"):
                listener.enterM_type(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitM_type"):
                listener.exitM_type(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitM_type"):
                return visitor.visitM_type(self)
            else:
                return visitor.visitChildren(self)




    def m_type(self):

        localctx = TinyCParser.M_typeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 12, self.RULE_m_type)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 90
            self.varType()
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class VarTypeContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(TinyCParser.VarTypeContext, self).__init__(parent, invokingState)
            self.parser = parser

        def Char(self):
            return self.getToken(TinyCParser.Char, 0)

        def Int(self):
            return self.getToken(TinyCParser.Int, 0)

        def Void(self):
            return self.getToken(TinyCParser.Void, 0)

        def Float(self):
            return self.getToken(TinyCParser.Float, 0)

        def Bool(self):
            return self.getToken(TinyCParser.Bool, 0)

        def Star(self):
            return self.getToken(TinyCParser.Star, 0)

        def getRuleIndex(self):
            return TinyCParser.RULE_varType

        def enterRule(self, listener):
            if hasattr(listener, "enterVarType"):
                listener.enterVarType(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitVarType"):
                listener.exitVarType(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitVarType"):
                return visitor.visitVarType(self)
            else:
                return visitor.visitChildren(self)




    def varType(self):

        localctx = TinyCParser.VarTypeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 14, self.RULE_varType)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 92
            _la = self._input.LA(1)
            if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << TinyCParser.Char) | (1 << TinyCParser.Bool) | (1 << TinyCParser.Int) | (1 << TinyCParser.Float) | (1 << TinyCParser.Void))) != 0)):
                self._errHandler.recoverInline(self)
            else:
                self.consume()
            self.state = 94
            _la = self._input.LA(1)
            if _la==TinyCParser.Star:
                self.state = 93
                self.match(TinyCParser.Star)


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Variable_declContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(TinyCParser.Variable_declContext, self).__init__(parent, invokingState)
            self.parser = parser

        def variable_id(self):
            return self.getTypedRuleContext(TinyCParser.Variable_idContext,0)


        def Assign(self):
            return self.getToken(TinyCParser.Assign, 0)

        def expression(self):
            return self.getTypedRuleContext(TinyCParser.ExpressionContext,0)


        def getRuleIndex(self):
            return TinyCParser.RULE_variable_decl

        def enterRule(self, listener):
            if hasattr(listener, "enterVariable_decl"):
                listener.enterVariable_decl(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitVariable_decl"):
                listener.exitVariable_decl(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitVariable_decl"):
                return visitor.visitVariable_decl(self)
            else:
                return visitor.visitChildren(self)




    def variable_decl(self):

        localctx = TinyCParser.Variable_declContext(self, self._ctx, self.state)
        self.enterRule(localctx, 16, self.RULE_variable_decl)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 96
            self.variable_id()
            self.state = 99
            _la = self._input.LA(1)
            if _la==TinyCParser.Assign:
                self.state = 97
                self.match(TinyCParser.Assign)
                self.state = 98
                self.expression()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Variable_idContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(TinyCParser.Variable_idContext, self).__init__(parent, invokingState)
            self.parser = parser

        def Identifier(self):
            return self.getToken(TinyCParser.Identifier, 0)

        def array_index(self):
            return self.getTypedRuleContext(TinyCParser.Array_indexContext,0)


        def getRuleIndex(self):
            return TinyCParser.RULE_variable_id

        def enterRule(self, listener):
            if hasattr(listener, "enterVariable_id"):
                listener.enterVariable_id(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitVariable_id"):
                listener.exitVariable_id(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitVariable_id"):
                return visitor.visitVariable_id(self)
            else:
                return visitor.visitChildren(self)




    def variable_id(self):

        localctx = TinyCParser.Variable_idContext(self, self._ctx, self.state)
        self.enterRule(localctx, 18, self.RULE_variable_id)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 101
            self.match(TinyCParser.Identifier)
            self.state = 103
            _la = self._input.LA(1)
            if _la==TinyCParser.LeftBracket:
                self.state = 102
                self.array_index()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Array_indexContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(TinyCParser.Array_indexContext, self).__init__(parent, invokingState)
            self.parser = parser

        def LeftBracket(self):
            return self.getToken(TinyCParser.LeftBracket, 0)

        def RightBracket(self):
            return self.getToken(TinyCParser.RightBracket, 0)

        def expression(self):
            return self.getTypedRuleContext(TinyCParser.ExpressionContext,0)


        def getRuleIndex(self):
            return TinyCParser.RULE_array_index

        def enterRule(self, listener):
            if hasattr(listener, "enterArray_index"):
                listener.enterArray_index(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitArray_index"):
                listener.exitArray_index(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitArray_index"):
                return visitor.visitArray_index(self)
            else:
                return visitor.visitChildren(self)




    def array_index(self):

        localctx = TinyCParser.Array_indexContext(self, self._ctx, self.state)
        self.enterRule(localctx, 20, self.RULE_array_index)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 105
            self.match(TinyCParser.LeftBracket)
            self.state = 107
            _la = self._input.LA(1)
            if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << TinyCParser.MTrue) | (1 << TinyCParser.MFalse) | (1 << TinyCParser.LeftParen) | (1 << TinyCParser.LeftBrace) | (1 << TinyCParser.PlusPlus) | (1 << TinyCParser.Minus) | (1 << TinyCParser.MinusMinus) | (1 << TinyCParser.Star) | (1 << TinyCParser.Ref) | (1 << TinyCParser.Not) | (1 << TinyCParser.Identifier) | (1 << TinyCParser.Number) | (1 << TinyCParser.String) | (1 << TinyCParser.Character))) != 0):
                self.state = 106
                self.expression()


            self.state = 109
            self.match(TinyCParser.RightBracket)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Function_declarationContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(TinyCParser.Function_declarationContext, self).__init__(parent, invokingState)
            self.parser = parser
            self.blck = None # BlockContext

        def m_type(self):
            return self.getTypedRuleContext(TinyCParser.M_typeContext,0)


        def Identifier(self):
            return self.getToken(TinyCParser.Identifier, 0)

        def LeftParen(self):
            return self.getToken(TinyCParser.LeftParen, 0)

        def parameter_list(self):
            return self.getTypedRuleContext(TinyCParser.Parameter_listContext,0)


        def RightParen(self):
            return self.getToken(TinyCParser.RightParen, 0)

        def Semi(self):
            return self.getToken(TinyCParser.Semi, 0)

        def LeftBrace(self):
            return self.getToken(TinyCParser.LeftBrace, 0)

        def RightBrace(self):
            return self.getToken(TinyCParser.RightBrace, 0)

        def block(self):
            return self.getTypedRuleContext(TinyCParser.BlockContext,0)


        def getRuleIndex(self):
            return TinyCParser.RULE_function_declaration

        def enterRule(self, listener):
            if hasattr(listener, "enterFunction_declaration"):
                listener.enterFunction_declaration(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitFunction_declaration"):
                listener.exitFunction_declaration(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitFunction_declaration"):
                return visitor.visitFunction_declaration(self)
            else:
                return visitor.visitChildren(self)




    def function_declaration(self):

        localctx = TinyCParser.Function_declarationContext(self, self._ctx, self.state)
        self.enterRule(localctx, 22, self.RULE_function_declaration)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 111
            self.m_type()
            self.state = 112
            self.match(TinyCParser.Identifier)
            self.state = 113
            self.match(TinyCParser.LeftParen)
            self.state = 114
            self.parameter_list()
            self.state = 115
            self.match(TinyCParser.RightParen)
            self.state = 121
            token = self._input.LA(1)
            if token in [TinyCParser.LeftBrace]:
                self.state = 116
                self.match(TinyCParser.LeftBrace)
                self.state = 117
                localctx.blck = self.block()
                self.state = 118
                self.match(TinyCParser.RightBrace)

            elif token in [TinyCParser.Semi]:
                self.state = 120
                self.match(TinyCParser.Semi)

            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Parameter_listContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(TinyCParser.Parameter_listContext, self).__init__(parent, invokingState)
            self.parser = parser

        def parameter(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(TinyCParser.ParameterContext)
            else:
                return self.getTypedRuleContext(TinyCParser.ParameterContext,i)


        def Comma(self, i=None):
            if i is None:
                return self.getTokens(TinyCParser.Comma)
            else:
                return self.getToken(TinyCParser.Comma, i)

        def Void(self):
            return self.getToken(TinyCParser.Void, 0)

        def getRuleIndex(self):
            return TinyCParser.RULE_parameter_list

        def enterRule(self, listener):
            if hasattr(listener, "enterParameter_list"):
                listener.enterParameter_list(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitParameter_list"):
                listener.exitParameter_list(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitParameter_list"):
                return visitor.visitParameter_list(self)
            else:
                return visitor.visitChildren(self)




    def parameter_list(self):

        localctx = TinyCParser.Parameter_listContext(self, self._ctx, self.state)
        self.enterRule(localctx, 24, self.RULE_parameter_list)
        self._la = 0 # Token type
        try:
            self.state = 134
            self._errHandler.sync(self);
            la_ = self._interp.adaptivePredict(self._input,10,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 131
                _la = self._input.LA(1)
                if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << TinyCParser.Char) | (1 << TinyCParser.Bool) | (1 << TinyCParser.Int) | (1 << TinyCParser.Float) | (1 << TinyCParser.Void))) != 0):
                    self.state = 123
                    self.parameter()
                    self.state = 128
                    self._errHandler.sync(self)
                    _la = self._input.LA(1)
                    while _la==TinyCParser.Comma:
                        self.state = 124
                        self.match(TinyCParser.Comma)
                        self.state = 125
                        self.parameter()
                        self.state = 130
                        self._errHandler.sync(self)
                        _la = self._input.LA(1)



                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 133
                self.match(TinyCParser.Void)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ParameterContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(TinyCParser.ParameterContext, self).__init__(parent, invokingState)
            self.parser = parser

        def m_type(self):
            return self.getTypedRuleContext(TinyCParser.M_typeContext,0)


        def Identifier(self):
            return self.getToken(TinyCParser.Identifier, 0)

        def array_index(self):
            return self.getTypedRuleContext(TinyCParser.Array_indexContext,0)


        def getRuleIndex(self):
            return TinyCParser.RULE_parameter

        def enterRule(self, listener):
            if hasattr(listener, "enterParameter"):
                listener.enterParameter(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitParameter"):
                listener.exitParameter(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitParameter"):
                return visitor.visitParameter(self)
            else:
                return visitor.visitChildren(self)




    def parameter(self):

        localctx = TinyCParser.ParameterContext(self, self._ctx, self.state)
        self.enterRule(localctx, 26, self.RULE_parameter)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 136
            self.m_type()
            self.state = 137
            self.match(TinyCParser.Identifier)
            self.state = 139
            _la = self._input.LA(1)
            if _la==TinyCParser.LeftBracket:
                self.state = 138
                self.array_index()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class BlockContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(TinyCParser.BlockContext, self).__init__(parent, invokingState)
            self.parser = parser

        def statement(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(TinyCParser.StatementContext)
            else:
                return self.getTypedRuleContext(TinyCParser.StatementContext,i)


        def getRuleIndex(self):
            return TinyCParser.RULE_block

        def enterRule(self, listener):
            if hasattr(listener, "enterBlock"):
                listener.enterBlock(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitBlock"):
                listener.exitBlock(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitBlock"):
                return visitor.visitBlock(self)
            else:
                return visitor.visitChildren(self)




    def block(self):

        localctx = TinyCParser.BlockContext(self, self._ctx, self.state)
        self.enterRule(localctx, 28, self.RULE_block)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 144
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << TinyCParser.Char) | (1 << TinyCParser.For) | (1 << TinyCParser.If) | (1 << TinyCParser.Bool) | (1 << TinyCParser.Int) | (1 << TinyCParser.MTrue) | (1 << TinyCParser.MFalse) | (1 << TinyCParser.Float) | (1 << TinyCParser.Return) | (1 << TinyCParser.Void) | (1 << TinyCParser.While) | (1 << TinyCParser.LeftParen) | (1 << TinyCParser.LeftBrace) | (1 << TinyCParser.PlusPlus) | (1 << TinyCParser.Minus) | (1 << TinyCParser.MinusMinus) | (1 << TinyCParser.Star) | (1 << TinyCParser.Ref) | (1 << TinyCParser.Not) | (1 << TinyCParser.Continue) | (1 << TinyCParser.Break) | (1 << TinyCParser.Identifier) | (1 << TinyCParser.Number) | (1 << TinyCParser.String) | (1 << TinyCParser.Character))) != 0):
                self.state = 141
                self.statement()
                self.state = 146
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class StatementContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(TinyCParser.StatementContext, self).__init__(parent, invokingState)
            self.parser = parser

        def if_statement(self):
            return self.getTypedRuleContext(TinyCParser.If_statementContext,0)


        def Semi(self):
            return self.getToken(TinyCParser.Semi, 0)

        def while_statement(self):
            return self.getTypedRuleContext(TinyCParser.While_statementContext,0)


        def for_statement(self):
            return self.getTypedRuleContext(TinyCParser.For_statementContext,0)


        def return_statement(self):
            return self.getTypedRuleContext(TinyCParser.Return_statementContext,0)


        def expression(self):
            return self.getTypedRuleContext(TinyCParser.ExpressionContext,0)


        def declaration(self):
            return self.getTypedRuleContext(TinyCParser.DeclarationContext,0)


        def Continue(self):
            return self.getToken(TinyCParser.Continue, 0)

        def Break(self):
            return self.getToken(TinyCParser.Break, 0)

        def getRuleIndex(self):
            return TinyCParser.RULE_statement

        def enterRule(self, listener):
            if hasattr(listener, "enterStatement"):
                listener.enterStatement(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitStatement"):
                listener.exitStatement(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitStatement"):
                return visitor.visitStatement(self)
            else:
                return visitor.visitChildren(self)




    def statement(self):

        localctx = TinyCParser.StatementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 30, self.RULE_statement)
        try:
            self.state = 172
            token = self._input.LA(1)
            if token in [TinyCParser.If]:
                self.enterOuterAlt(localctx, 1)
                self.state = 147
                self.if_statement()
                self.state = 149
                self._errHandler.sync(self);
                la_ = self._interp.adaptivePredict(self._input,13,self._ctx)
                if la_ == 1:
                    self.state = 148
                    self.match(TinyCParser.Semi)



            elif token in [TinyCParser.While]:
                self.enterOuterAlt(localctx, 2)
                self.state = 151
                self.while_statement()
                self.state = 153
                self._errHandler.sync(self);
                la_ = self._interp.adaptivePredict(self._input,14,self._ctx)
                if la_ == 1:
                    self.state = 152
                    self.match(TinyCParser.Semi)



            elif token in [TinyCParser.For]:
                self.enterOuterAlt(localctx, 3)
                self.state = 155
                self.for_statement()
                self.state = 157
                self._errHandler.sync(self);
                la_ = self._interp.adaptivePredict(self._input,15,self._ctx)
                if la_ == 1:
                    self.state = 156
                    self.match(TinyCParser.Semi)



            elif token in [TinyCParser.Return]:
                self.enterOuterAlt(localctx, 4)
                self.state = 159
                self.return_statement()
                self.state = 160
                self.match(TinyCParser.Semi)

            elif token in [TinyCParser.MTrue, TinyCParser.MFalse, TinyCParser.LeftParen, TinyCParser.LeftBrace, TinyCParser.PlusPlus, TinyCParser.Minus, TinyCParser.MinusMinus, TinyCParser.Star, TinyCParser.Ref, TinyCParser.Not, TinyCParser.Identifier, TinyCParser.Number, TinyCParser.String, TinyCParser.Character]:
                self.enterOuterAlt(localctx, 5)
                self.state = 162
                self.expression()
                self.state = 163
                self.match(TinyCParser.Semi)

            elif token in [TinyCParser.Char, TinyCParser.Bool, TinyCParser.Int, TinyCParser.Float, TinyCParser.Void]:
                self.enterOuterAlt(localctx, 6)
                self.state = 165
                self.declaration()
                self.state = 166
                self.match(TinyCParser.Semi)

            elif token in [TinyCParser.Continue]:
                self.enterOuterAlt(localctx, 7)
                self.state = 168
                self.match(TinyCParser.Continue)
                self.state = 169
                self.match(TinyCParser.Semi)

            elif token in [TinyCParser.Break]:
                self.enterOuterAlt(localctx, 8)
                self.state = 170
                self.match(TinyCParser.Break)
                self.state = 171
                self.match(TinyCParser.Semi)

            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class If_statementContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(TinyCParser.If_statementContext, self).__init__(parent, invokingState)
            self.parser = parser

        def If(self):
            return self.getToken(TinyCParser.If, 0)

        def LeftParen(self):
            return self.getToken(TinyCParser.LeftParen, 0)

        def expression(self):
            return self.getTypedRuleContext(TinyCParser.ExpressionContext,0)


        def RightParen(self):
            return self.getToken(TinyCParser.RightParen, 0)

        def statement(self):
            return self.getTypedRuleContext(TinyCParser.StatementContext,0)


        def LeftBrace(self):
            return self.getToken(TinyCParser.LeftBrace, 0)

        def block(self):
            return self.getTypedRuleContext(TinyCParser.BlockContext,0)


        def RightBrace(self):
            return self.getToken(TinyCParser.RightBrace, 0)

        def else_statement(self):
            return self.getTypedRuleContext(TinyCParser.Else_statementContext,0)


        def getRuleIndex(self):
            return TinyCParser.RULE_if_statement

        def enterRule(self, listener):
            if hasattr(listener, "enterIf_statement"):
                listener.enterIf_statement(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitIf_statement"):
                listener.exitIf_statement(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitIf_statement"):
                return visitor.visitIf_statement(self)
            else:
                return visitor.visitChildren(self)




    def if_statement(self):

        localctx = TinyCParser.If_statementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 32, self.RULE_if_statement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 174
            self.match(TinyCParser.If)
            self.state = 175
            self.match(TinyCParser.LeftParen)
            self.state = 176
            self.expression()
            self.state = 177
            self.match(TinyCParser.RightParen)
            self.state = 183
            self._errHandler.sync(self);
            la_ = self._interp.adaptivePredict(self._input,17,self._ctx)
            if la_ == 1:
                self.state = 178
                self.statement()
                pass

            elif la_ == 2:
                self.state = 179
                self.match(TinyCParser.LeftBrace)
                self.state = 180
                self.block()
                self.state = 181
                self.match(TinyCParser.RightBrace)
                pass


            self.state = 186
            self._errHandler.sync(self);
            la_ = self._interp.adaptivePredict(self._input,18,self._ctx)
            if la_ == 1:
                self.state = 185
                self.else_statement()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Else_statementContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(TinyCParser.Else_statementContext, self).__init__(parent, invokingState)
            self.parser = parser

        def Else(self):
            return self.getToken(TinyCParser.Else, 0)

        def statement(self):
            return self.getTypedRuleContext(TinyCParser.StatementContext,0)


        def LeftBrace(self):
            return self.getToken(TinyCParser.LeftBrace, 0)

        def block(self):
            return self.getTypedRuleContext(TinyCParser.BlockContext,0)


        def RightBrace(self):
            return self.getToken(TinyCParser.RightBrace, 0)

        def getRuleIndex(self):
            return TinyCParser.RULE_else_statement

        def enterRule(self, listener):
            if hasattr(listener, "enterElse_statement"):
                listener.enterElse_statement(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitElse_statement"):
                listener.exitElse_statement(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitElse_statement"):
                return visitor.visitElse_statement(self)
            else:
                return visitor.visitChildren(self)




    def else_statement(self):

        localctx = TinyCParser.Else_statementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 34, self.RULE_else_statement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 188
            self.match(TinyCParser.Else)
            self.state = 194
            self._errHandler.sync(self);
            la_ = self._interp.adaptivePredict(self._input,19,self._ctx)
            if la_ == 1:
                self.state = 189
                self.statement()
                pass

            elif la_ == 2:
                self.state = 190
                self.match(TinyCParser.LeftBrace)
                self.state = 191
                self.block()
                self.state = 192
                self.match(TinyCParser.RightBrace)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class While_statementContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(TinyCParser.While_statementContext, self).__init__(parent, invokingState)
            self.parser = parser

        def While(self):
            return self.getToken(TinyCParser.While, 0)

        def LeftParen(self):
            return self.getToken(TinyCParser.LeftParen, 0)

        def expression(self):
            return self.getTypedRuleContext(TinyCParser.ExpressionContext,0)


        def RightParen(self):
            return self.getToken(TinyCParser.RightParen, 0)

        def statement(self):
            return self.getTypedRuleContext(TinyCParser.StatementContext,0)


        def LeftBrace(self):
            return self.getToken(TinyCParser.LeftBrace, 0)

        def block(self):
            return self.getTypedRuleContext(TinyCParser.BlockContext,0)


        def RightBrace(self):
            return self.getToken(TinyCParser.RightBrace, 0)

        def getRuleIndex(self):
            return TinyCParser.RULE_while_statement

        def enterRule(self, listener):
            if hasattr(listener, "enterWhile_statement"):
                listener.enterWhile_statement(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitWhile_statement"):
                listener.exitWhile_statement(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitWhile_statement"):
                return visitor.visitWhile_statement(self)
            else:
                return visitor.visitChildren(self)




    def while_statement(self):

        localctx = TinyCParser.While_statementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 36, self.RULE_while_statement)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 196
            self.match(TinyCParser.While)
            self.state = 197
            self.match(TinyCParser.LeftParen)
            self.state = 198
            self.expression()
            self.state = 199
            self.match(TinyCParser.RightParen)
            self.state = 205
            self._errHandler.sync(self);
            la_ = self._interp.adaptivePredict(self._input,20,self._ctx)
            if la_ == 1:
                self.state = 200
                self.statement()
                pass

            elif la_ == 2:
                self.state = 201
                self.match(TinyCParser.LeftBrace)
                self.state = 202
                self.block()
                self.state = 203
                self.match(TinyCParser.RightBrace)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class For_statementContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(TinyCParser.For_statementContext, self).__init__(parent, invokingState)
            self.parser = parser
            self.e1 = None # StatementContext
            self.e2 = None # ExpressionContext
            self.e3 = None # ExpressionContext

        def For(self):
            return self.getToken(TinyCParser.For, 0)

        def LeftParen(self):
            return self.getToken(TinyCParser.LeftParen, 0)

        def Semi(self, i=None):
            if i is None:
                return self.getTokens(TinyCParser.Semi)
            else:
                return self.getToken(TinyCParser.Semi, i)

        def RightParen(self):
            return self.getToken(TinyCParser.RightParen, 0)

        def statement(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(TinyCParser.StatementContext)
            else:
                return self.getTypedRuleContext(TinyCParser.StatementContext,i)


        def LeftBrace(self):
            return self.getToken(TinyCParser.LeftBrace, 0)

        def block(self):
            return self.getTypedRuleContext(TinyCParser.BlockContext,0)


        def RightBrace(self):
            return self.getToken(TinyCParser.RightBrace, 0)

        def expression(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(TinyCParser.ExpressionContext)
            else:
                return self.getTypedRuleContext(TinyCParser.ExpressionContext,i)


        def getRuleIndex(self):
            return TinyCParser.RULE_for_statement

        def enterRule(self, listener):
            if hasattr(listener, "enterFor_statement"):
                listener.enterFor_statement(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitFor_statement"):
                listener.exitFor_statement(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitFor_statement"):
                return visitor.visitFor_statement(self)
            else:
                return visitor.visitChildren(self)




    def for_statement(self):

        localctx = TinyCParser.For_statementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 38, self.RULE_for_statement)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 207
            self.match(TinyCParser.For)
            self.state = 208
            self.match(TinyCParser.LeftParen)
            self.state = 210
            self._errHandler.sync(self);
            la_ = self._interp.adaptivePredict(self._input,21,self._ctx)
            if la_ == 1:
                self.state = 209
                localctx.e1 = self.statement()


            self.state = 213
            _la = self._input.LA(1)
            if _la==TinyCParser.Semi:
                self.state = 212
                self.match(TinyCParser.Semi)


            self.state = 215
            localctx.e2 = self.expression()
            self.state = 216
            self.match(TinyCParser.Semi)
            self.state = 218
            _la = self._input.LA(1)
            if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << TinyCParser.MTrue) | (1 << TinyCParser.MFalse) | (1 << TinyCParser.LeftParen) | (1 << TinyCParser.LeftBrace) | (1 << TinyCParser.PlusPlus) | (1 << TinyCParser.Minus) | (1 << TinyCParser.MinusMinus) | (1 << TinyCParser.Star) | (1 << TinyCParser.Ref) | (1 << TinyCParser.Not) | (1 << TinyCParser.Identifier) | (1 << TinyCParser.Number) | (1 << TinyCParser.String) | (1 << TinyCParser.Character))) != 0):
                self.state = 217
                localctx.e3 = self.expression()


            self.state = 220
            self.match(TinyCParser.RightParen)
            self.state = 226
            self._errHandler.sync(self);
            la_ = self._interp.adaptivePredict(self._input,24,self._ctx)
            if la_ == 1:
                self.state = 221
                self.statement()
                pass

            elif la_ == 2:
                self.state = 222
                self.match(TinyCParser.LeftBrace)
                self.state = 223
                self.block()
                self.state = 224
                self.match(TinyCParser.RightBrace)
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Return_statementContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(TinyCParser.Return_statementContext, self).__init__(parent, invokingState)
            self.parser = parser

        def Return(self):
            return self.getToken(TinyCParser.Return, 0)

        def expression(self):
            return self.getTypedRuleContext(TinyCParser.ExpressionContext,0)


        def getRuleIndex(self):
            return TinyCParser.RULE_return_statement

        def enterRule(self, listener):
            if hasattr(listener, "enterReturn_statement"):
                listener.enterReturn_statement(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitReturn_statement"):
                listener.exitReturn_statement(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitReturn_statement"):
                return visitor.visitReturn_statement(self)
            else:
                return visitor.visitChildren(self)




    def return_statement(self):

        localctx = TinyCParser.Return_statementContext(self, self._ctx, self.state)
        self.enterRule(localctx, 40, self.RULE_return_statement)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 228
            self.match(TinyCParser.Return)
            self.state = 230
            _la = self._input.LA(1)
            if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << TinyCParser.MTrue) | (1 << TinyCParser.MFalse) | (1 << TinyCParser.LeftParen) | (1 << TinyCParser.LeftBrace) | (1 << TinyCParser.PlusPlus) | (1 << TinyCParser.Minus) | (1 << TinyCParser.MinusMinus) | (1 << TinyCParser.Star) | (1 << TinyCParser.Ref) | (1 << TinyCParser.Not) | (1 << TinyCParser.Identifier) | (1 << TinyCParser.Number) | (1 << TinyCParser.String) | (1 << TinyCParser.Character))) != 0):
                self.state = 229
                self.expression()


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class ExpressionContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(TinyCParser.ExpressionContext, self).__init__(parent, invokingState)
            self.parser = parser

        def or_expr(self):
            return self.getTypedRuleContext(TinyCParser.Or_exprContext,0)


        def pr_expr(self):
            return self.getTypedRuleContext(TinyCParser.Pr_exprContext,0)


        def expression(self):
            return self.getTypedRuleContext(TinyCParser.ExpressionContext,0)


        def Assign(self):
            return self.getToken(TinyCParser.Assign, 0)

        def StarAssign(self):
            return self.getToken(TinyCParser.StarAssign, 0)

        def DivAssign(self):
            return self.getToken(TinyCParser.DivAssign, 0)

        def ModAssign(self):
            return self.getToken(TinyCParser.ModAssign, 0)

        def PlusAssign(self):
            return self.getToken(TinyCParser.PlusAssign, 0)

        def MinusAssign(self):
            return self.getToken(TinyCParser.MinusAssign, 0)

        def getRuleIndex(self):
            return TinyCParser.RULE_expression

        def enterRule(self, listener):
            if hasattr(listener, "enterExpression"):
                listener.enterExpression(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitExpression"):
                listener.exitExpression(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitExpression"):
                return visitor.visitExpression(self)
            else:
                return visitor.visitChildren(self)




    def expression(self):

        localctx = TinyCParser.ExpressionContext(self, self._ctx, self.state)
        self.enterRule(localctx, 42, self.RULE_expression)
        self._la = 0 # Token type
        try:
            self.state = 237
            self._errHandler.sync(self);
            la_ = self._interp.adaptivePredict(self._input,26,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 232
                self.or_expr(0)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 233
                self.pr_expr()
                self.state = 234
                _la = self._input.LA(1)
                if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << TinyCParser.Assign) | (1 << TinyCParser.StarAssign) | (1 << TinyCParser.DivAssign) | (1 << TinyCParser.ModAssign) | (1 << TinyCParser.PlusAssign) | (1 << TinyCParser.MinusAssign))) != 0)):
                    self._errHandler.recoverInline(self)
                else:
                    self.consume()
                self.state = 235
                self.expression()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Or_exprContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(TinyCParser.Or_exprContext, self).__init__(parent, invokingState)
            self.parser = parser

        def and_expr(self):
            return self.getTypedRuleContext(TinyCParser.And_exprContext,0)


        def or_expr(self):
            return self.getTypedRuleContext(TinyCParser.Or_exprContext,0)


        def OrOr(self):
            return self.getToken(TinyCParser.OrOr, 0)

        def getRuleIndex(self):
            return TinyCParser.RULE_or_expr

        def enterRule(self, listener):
            if hasattr(listener, "enterOr_expr"):
                listener.enterOr_expr(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitOr_expr"):
                listener.exitOr_expr(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitOr_expr"):
                return visitor.visitOr_expr(self)
            else:
                return visitor.visitChildren(self)



    def or_expr(self, _p=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = TinyCParser.Or_exprContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 44
        self.enterRecursionRule(localctx, 44, self.RULE_or_expr, _p)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 240
            self.and_expr(0)
            self._ctx.stop = self._input.LT(-1)
            self.state = 247
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,27,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    localctx = TinyCParser.Or_exprContext(self, _parentctx, _parentState)
                    self.pushNewRecursionContext(localctx, _startState, self.RULE_or_expr)
                    self.state = 242
                    if not self.precpred(self._ctx, 1):
                        from antlr4.error.Errors import FailedPredicateException
                        raise FailedPredicateException(self, "self.precpred(self._ctx, 1)")
                    self.state = 243
                    self.match(TinyCParser.OrOr)
                    self.state = 244
                    self.and_expr(0) 
                self.state = 249
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,27,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx

    class And_exprContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(TinyCParser.And_exprContext, self).__init__(parent, invokingState)
            self.parser = parser

        def rel_expr(self):
            return self.getTypedRuleContext(TinyCParser.Rel_exprContext,0)


        def and_expr(self):
            return self.getTypedRuleContext(TinyCParser.And_exprContext,0)


        def AndAnd(self):
            return self.getToken(TinyCParser.AndAnd, 0)

        def getRuleIndex(self):
            return TinyCParser.RULE_and_expr

        def enterRule(self, listener):
            if hasattr(listener, "enterAnd_expr"):
                listener.enterAnd_expr(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitAnd_expr"):
                listener.exitAnd_expr(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitAnd_expr"):
                return visitor.visitAnd_expr(self)
            else:
                return visitor.visitChildren(self)



    def and_expr(self, _p=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = TinyCParser.And_exprContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 46
        self.enterRecursionRule(localctx, 46, self.RULE_and_expr, _p)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 251
            self.rel_expr(0)
            self._ctx.stop = self._input.LT(-1)
            self.state = 258
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,28,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    localctx = TinyCParser.And_exprContext(self, _parentctx, _parentState)
                    self.pushNewRecursionContext(localctx, _startState, self.RULE_and_expr)
                    self.state = 253
                    if not self.precpred(self._ctx, 1):
                        from antlr4.error.Errors import FailedPredicateException
                        raise FailedPredicateException(self, "self.precpred(self._ctx, 1)")
                    self.state = 254
                    self.match(TinyCParser.AndAnd)
                    self.state = 255
                    self.rel_expr(0) 
                self.state = 260
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,28,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx

    class Rel_exprContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(TinyCParser.Rel_exprContext, self).__init__(parent, invokingState)
            self.parser = parser

        def add_expr(self):
            return self.getTypedRuleContext(TinyCParser.Add_exprContext,0)


        def rel_expr(self):
            return self.getTypedRuleContext(TinyCParser.Rel_exprContext,0)


        def Less(self):
            return self.getToken(TinyCParser.Less, 0)

        def LessEqual(self):
            return self.getToken(TinyCParser.LessEqual, 0)

        def Greater(self):
            return self.getToken(TinyCParser.Greater, 0)

        def GreaterEqual(self):
            return self.getToken(TinyCParser.GreaterEqual, 0)

        def Equal(self):
            return self.getToken(TinyCParser.Equal, 0)

        def NotEqual(self):
            return self.getToken(TinyCParser.NotEqual, 0)

        def getRuleIndex(self):
            return TinyCParser.RULE_rel_expr

        def enterRule(self, listener):
            if hasattr(listener, "enterRel_expr"):
                listener.enterRel_expr(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitRel_expr"):
                listener.exitRel_expr(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitRel_expr"):
                return visitor.visitRel_expr(self)
            else:
                return visitor.visitChildren(self)



    def rel_expr(self, _p=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = TinyCParser.Rel_exprContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 48
        self.enterRecursionRule(localctx, 48, self.RULE_rel_expr, _p)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 262
            self.add_expr(0)
            self._ctx.stop = self._input.LT(-1)
            self.state = 269
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,29,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    localctx = TinyCParser.Rel_exprContext(self, _parentctx, _parentState)
                    self.pushNewRecursionContext(localctx, _startState, self.RULE_rel_expr)
                    self.state = 264
                    if not self.precpred(self._ctx, 1):
                        from antlr4.error.Errors import FailedPredicateException
                        raise FailedPredicateException(self, "self.precpred(self._ctx, 1)")
                    self.state = 265
                    _la = self._input.LA(1)
                    if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << TinyCParser.Less) | (1 << TinyCParser.LessEqual) | (1 << TinyCParser.Greater) | (1 << TinyCParser.GreaterEqual) | (1 << TinyCParser.Equal) | (1 << TinyCParser.NotEqual))) != 0)):
                        self._errHandler.recoverInline(self)
                    else:
                        self.consume()
                    self.state = 266
                    self.add_expr(0) 
                self.state = 271
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,29,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx

    class Add_exprContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(TinyCParser.Add_exprContext, self).__init__(parent, invokingState)
            self.parser = parser

        def mult_expr(self):
            return self.getTypedRuleContext(TinyCParser.Mult_exprContext,0)


        def add_expr(self):
            return self.getTypedRuleContext(TinyCParser.Add_exprContext,0)


        def Plus(self):
            return self.getToken(TinyCParser.Plus, 0)

        def Minus(self):
            return self.getToken(TinyCParser.Minus, 0)

        def getRuleIndex(self):
            return TinyCParser.RULE_add_expr

        def enterRule(self, listener):
            if hasattr(listener, "enterAdd_expr"):
                listener.enterAdd_expr(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitAdd_expr"):
                listener.exitAdd_expr(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitAdd_expr"):
                return visitor.visitAdd_expr(self)
            else:
                return visitor.visitChildren(self)



    def add_expr(self, _p=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = TinyCParser.Add_exprContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 50
        self.enterRecursionRule(localctx, 50, self.RULE_add_expr, _p)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 273
            self.mult_expr(0)
            self._ctx.stop = self._input.LT(-1)
            self.state = 280
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,30,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    localctx = TinyCParser.Add_exprContext(self, _parentctx, _parentState)
                    self.pushNewRecursionContext(localctx, _startState, self.RULE_add_expr)
                    self.state = 275
                    if not self.precpred(self._ctx, 1):
                        from antlr4.error.Errors import FailedPredicateException
                        raise FailedPredicateException(self, "self.precpred(self._ctx, 1)")
                    self.state = 276
                    _la = self._input.LA(1)
                    if not(_la==TinyCParser.Plus or _la==TinyCParser.Minus):
                        self._errHandler.recoverInline(self)
                    else:
                        self.consume()
                    self.state = 277
                    self.mult_expr(0) 
                self.state = 282
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,30,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx

    class Mult_exprContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(TinyCParser.Mult_exprContext, self).__init__(parent, invokingState)
            self.parser = parser

        def unary_expr(self):
            return self.getTypedRuleContext(TinyCParser.Unary_exprContext,0)


        def mult_expr(self):
            return self.getTypedRuleContext(TinyCParser.Mult_exprContext,0)


        def Star(self):
            return self.getToken(TinyCParser.Star, 0)

        def Div(self):
            return self.getToken(TinyCParser.Div, 0)

        def Mod(self):
            return self.getToken(TinyCParser.Mod, 0)

        def getRuleIndex(self):
            return TinyCParser.RULE_mult_expr

        def enterRule(self, listener):
            if hasattr(listener, "enterMult_expr"):
                listener.enterMult_expr(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitMult_expr"):
                listener.exitMult_expr(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitMult_expr"):
                return visitor.visitMult_expr(self)
            else:
                return visitor.visitChildren(self)



    def mult_expr(self, _p=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = TinyCParser.Mult_exprContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 52
        self.enterRecursionRule(localctx, 52, self.RULE_mult_expr, _p)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 284
            self.unary_expr(0)
            self._ctx.stop = self._input.LT(-1)
            self.state = 291
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,31,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    localctx = TinyCParser.Mult_exprContext(self, _parentctx, _parentState)
                    self.pushNewRecursionContext(localctx, _startState, self.RULE_mult_expr)
                    self.state = 286
                    if not self.precpred(self._ctx, 1):
                        from antlr4.error.Errors import FailedPredicateException
                        raise FailedPredicateException(self, "self.precpred(self._ctx, 1)")
                    self.state = 287
                    _la = self._input.LA(1)
                    if not((((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << TinyCParser.Star) | (1 << TinyCParser.Div) | (1 << TinyCParser.Mod))) != 0)):
                        self._errHandler.recoverInline(self)
                    else:
                        self.consume()
                    self.state = 288
                    self.unary_expr(0) 
                self.state = 293
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,31,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx

    class Unary_exprContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(TinyCParser.Unary_exprContext, self).__init__(parent, invokingState)
            self.parser = parser
            self.prepp = None # Token
            self.premm = None # Token
            self.postpp = None # Token
            self.postmm = None # Token

        def unary_expr(self):
            return self.getTypedRuleContext(TinyCParser.Unary_exprContext,0)


        def Minus(self):
            return self.getToken(TinyCParser.Minus, 0)

        def Not(self):
            return self.getToken(TinyCParser.Not, 0)

        def PlusPlus(self):
            return self.getToken(TinyCParser.PlusPlus, 0)

        def MinusMinus(self):
            return self.getToken(TinyCParser.MinusMinus, 0)

        def pf_expr(self):
            return self.getTypedRuleContext(TinyCParser.Pf_exprContext,0)


        def getRuleIndex(self):
            return TinyCParser.RULE_unary_expr

        def enterRule(self, listener):
            if hasattr(listener, "enterUnary_expr"):
                listener.enterUnary_expr(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitUnary_expr"):
                listener.exitUnary_expr(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitUnary_expr"):
                return visitor.visitUnary_expr(self)
            else:
                return visitor.visitChildren(self)



    def unary_expr(self, _p=0):
        _parentctx = self._ctx
        _parentState = self.state
        localctx = TinyCParser.Unary_exprContext(self, self._ctx, _parentState)
        _prevctx = localctx
        _startState = 54
        self.enterRecursionRule(localctx, 54, self.RULE_unary_expr, _p)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 303
            token = self._input.LA(1)
            if token in [TinyCParser.PlusPlus, TinyCParser.Minus, TinyCParser.MinusMinus, TinyCParser.Not]:
                self.state = 299
                token = self._input.LA(1)
                if token in [TinyCParser.Minus]:
                    self.state = 295
                    self.match(TinyCParser.Minus)

                elif token in [TinyCParser.Not]:
                    self.state = 296
                    self.match(TinyCParser.Not)

                elif token in [TinyCParser.PlusPlus]:
                    self.state = 297
                    localctx.prepp = self.match(TinyCParser.PlusPlus)

                elif token in [TinyCParser.MinusMinus]:
                    self.state = 298
                    localctx.premm = self.match(TinyCParser.MinusMinus)

                else:
                    raise NoViableAltException(self)

                self.state = 301
                self.unary_expr(2)

            elif token in [TinyCParser.MTrue, TinyCParser.MFalse, TinyCParser.LeftParen, TinyCParser.LeftBrace, TinyCParser.Star, TinyCParser.Ref, TinyCParser.Identifier, TinyCParser.Number, TinyCParser.String, TinyCParser.Character]:
                self.state = 302
                self.pf_expr()

            else:
                raise NoViableAltException(self)

            self._ctx.stop = self._input.LT(-1)
            self.state = 312
            self._errHandler.sync(self)
            _alt = self._interp.adaptivePredict(self._input,35,self._ctx)
            while _alt!=2 and _alt!=ATN.INVALID_ALT_NUMBER:
                if _alt==1:
                    if self._parseListeners is not None:
                        self.triggerExitRuleEvent()
                    _prevctx = localctx
                    localctx = TinyCParser.Unary_exprContext(self, _parentctx, _parentState)
                    self.pushNewRecursionContext(localctx, _startState, self.RULE_unary_expr)
                    self.state = 305
                    if not self.precpred(self._ctx, 1):
                        from antlr4.error.Errors import FailedPredicateException
                        raise FailedPredicateException(self, "self.precpred(self._ctx, 1)")
                    self.state = 308
                    token = self._input.LA(1)
                    if token in [TinyCParser.PlusPlus]:
                        self.state = 306
                        localctx.postpp = self.match(TinyCParser.PlusPlus)

                    elif token in [TinyCParser.MinusMinus]:
                        self.state = 307
                        localctx.postmm = self.match(TinyCParser.MinusMinus)

                    else:
                        raise NoViableAltException(self)
             
                self.state = 314
                self._errHandler.sync(self)
                _alt = self._interp.adaptivePredict(self._input,35,self._ctx)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.unrollRecursionContexts(_parentctx)
        return localctx

    class Pf_exprContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(TinyCParser.Pf_exprContext, self).__init__(parent, invokingState)
            self.parser = parser

        def Identifier(self):
            return self.getToken(TinyCParser.Identifier, 0)

        def LeftParen(self):
            return self.getToken(TinyCParser.LeftParen, 0)

        def RightParen(self):
            return self.getToken(TinyCParser.RightParen, 0)

        def arg_list(self):
            return self.getTypedRuleContext(TinyCParser.Arg_listContext,0)


        def pr_expr(self):
            return self.getTypedRuleContext(TinyCParser.Pr_exprContext,0)


        def getRuleIndex(self):
            return TinyCParser.RULE_pf_expr

        def enterRule(self, listener):
            if hasattr(listener, "enterPf_expr"):
                listener.enterPf_expr(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitPf_expr"):
                listener.exitPf_expr(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitPf_expr"):
                return visitor.visitPf_expr(self)
            else:
                return visitor.visitChildren(self)




    def pf_expr(self):

        localctx = TinyCParser.Pf_exprContext(self, self._ctx, self.state)
        self.enterRule(localctx, 56, self.RULE_pf_expr)
        self._la = 0 # Token type
        try:
            self.state = 322
            self._errHandler.sync(self);
            la_ = self._interp.adaptivePredict(self._input,37,self._ctx)
            if la_ == 1:
                self.enterOuterAlt(localctx, 1)
                self.state = 315
                self.match(TinyCParser.Identifier)
                self.state = 316
                self.match(TinyCParser.LeftParen)
                self.state = 318
                _la = self._input.LA(1)
                if (((_la) & ~0x3f) == 0 and ((1 << _la) & ((1 << TinyCParser.MTrue) | (1 << TinyCParser.MFalse) | (1 << TinyCParser.LeftParen) | (1 << TinyCParser.LeftBrace) | (1 << TinyCParser.PlusPlus) | (1 << TinyCParser.Minus) | (1 << TinyCParser.MinusMinus) | (1 << TinyCParser.Star) | (1 << TinyCParser.Ref) | (1 << TinyCParser.Not) | (1 << TinyCParser.Identifier) | (1 << TinyCParser.Number) | (1 << TinyCParser.String) | (1 << TinyCParser.Character))) != 0):
                    self.state = 317
                    self.arg_list()


                self.state = 320
                self.match(TinyCParser.RightParen)
                pass

            elif la_ == 2:
                self.enterOuterAlt(localctx, 2)
                self.state = 321
                self.pr_expr()
                pass


        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Arg_listContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(TinyCParser.Arg_listContext, self).__init__(parent, invokingState)
            self.parser = parser

        def expression(self, i=None):
            if i is None:
                return self.getTypedRuleContexts(TinyCParser.ExpressionContext)
            else:
                return self.getTypedRuleContext(TinyCParser.ExpressionContext,i)


        def Comma(self, i=None):
            if i is None:
                return self.getTokens(TinyCParser.Comma)
            else:
                return self.getToken(TinyCParser.Comma, i)

        def getRuleIndex(self):
            return TinyCParser.RULE_arg_list

        def enterRule(self, listener):
            if hasattr(listener, "enterArg_list"):
                listener.enterArg_list(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitArg_list"):
                listener.exitArg_list(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitArg_list"):
                return visitor.visitArg_list(self)
            else:
                return visitor.visitChildren(self)




    def arg_list(self):

        localctx = TinyCParser.Arg_listContext(self, self._ctx, self.state)
        self.enterRule(localctx, 58, self.RULE_arg_list)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 324
            self.expression()
            self.state = 329
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==TinyCParser.Comma:
                self.state = 325
                self.match(TinyCParser.Comma)
                self.state = 326
                self.expression()
                self.state = 331
                self._errHandler.sync(self)
                _la = self._input.LA(1)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx

    class Pr_exprContext(ParserRuleContext):

        def __init__(self, parser, parent=None, invokingState=-1):
            super(TinyCParser.Pr_exprContext, self).__init__(parent, invokingState)
            self.parser = parser

        def Identifier(self):
            return self.getToken(TinyCParser.Identifier, 0)

        def array_index(self):
            return self.getTypedRuleContext(TinyCParser.Array_indexContext,0)


        def Star(self):
            return self.getToken(TinyCParser.Star, 0)

        def Ref(self):
            return self.getToken(TinyCParser.Ref, 0)

        def Number(self):
            return self.getToken(TinyCParser.Number, 0)

        def String(self):
            return self.getToken(TinyCParser.String, 0)

        def Character(self):
            return self.getToken(TinyCParser.Character, 0)

        def MTrue(self):
            return self.getToken(TinyCParser.MTrue, 0)

        def MFalse(self):
            return self.getToken(TinyCParser.MFalse, 0)

        def LeftParen(self):
            return self.getToken(TinyCParser.LeftParen, 0)

        def expression(self):
            return self.getTypedRuleContext(TinyCParser.ExpressionContext,0)


        def RightParen(self):
            return self.getToken(TinyCParser.RightParen, 0)

        def LeftBrace(self):
            return self.getToken(TinyCParser.LeftBrace, 0)

        def arg_list(self):
            return self.getTypedRuleContext(TinyCParser.Arg_listContext,0)


        def RightBrace(self):
            return self.getToken(TinyCParser.RightBrace, 0)

        def getRuleIndex(self):
            return TinyCParser.RULE_pr_expr

        def enterRule(self, listener):
            if hasattr(listener, "enterPr_expr"):
                listener.enterPr_expr(self)

        def exitRule(self, listener):
            if hasattr(listener, "exitPr_expr"):
                listener.exitPr_expr(self)

        def accept(self, visitor):
            if hasattr(visitor, "visitPr_expr"):
                return visitor.visitPr_expr(self)
            else:
                return visitor.visitChildren(self)




    def pr_expr(self):

        localctx = TinyCParser.Pr_exprContext(self, self._ctx, self.state)
        self.enterRule(localctx, 60, self.RULE_pr_expr)
        self._la = 0 # Token type
        try:
            self.state = 352
            token = self._input.LA(1)
            if token in [TinyCParser.Star, TinyCParser.Ref, TinyCParser.Identifier]:
                self.enterOuterAlt(localctx, 1)
                self.state = 333
                _la = self._input.LA(1)
                if _la==TinyCParser.Star or _la==TinyCParser.Ref:
                    self.state = 332
                    _la = self._input.LA(1)
                    if not(_la==TinyCParser.Star or _la==TinyCParser.Ref):
                        self._errHandler.recoverInline(self)
                    else:
                        self.consume()


                self.state = 335
                self.match(TinyCParser.Identifier)
                self.state = 337
                self._errHandler.sync(self);
                la_ = self._interp.adaptivePredict(self._input,40,self._ctx)
                if la_ == 1:
                    self.state = 336
                    self.array_index()



            elif token in [TinyCParser.Number]:
                self.enterOuterAlt(localctx, 2)
                self.state = 339
                self.match(TinyCParser.Number)

            elif token in [TinyCParser.String]:
                self.enterOuterAlt(localctx, 3)
                self.state = 340
                self.match(TinyCParser.String)

            elif token in [TinyCParser.Character]:
                self.enterOuterAlt(localctx, 4)
                self.state = 341
                self.match(TinyCParser.Character)

            elif token in [TinyCParser.MTrue]:
                self.enterOuterAlt(localctx, 5)
                self.state = 342
                self.match(TinyCParser.MTrue)

            elif token in [TinyCParser.MFalse]:
                self.enterOuterAlt(localctx, 6)
                self.state = 343
                self.match(TinyCParser.MFalse)

            elif token in [TinyCParser.LeftParen]:
                self.enterOuterAlt(localctx, 7)
                self.state = 344
                self.match(TinyCParser.LeftParen)
                self.state = 345
                self.expression()
                self.state = 346
                self.match(TinyCParser.RightParen)

            elif token in [TinyCParser.LeftBrace]:
                self.enterOuterAlt(localctx, 8)
                self.state = 348
                self.match(TinyCParser.LeftBrace)
                self.state = 349
                self.arg_list()
                self.state = 350
                self.match(TinyCParser.RightBrace)

            else:
                raise NoViableAltException(self)

        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx



    def sempred(self, localctx, ruleIndex, predIndex):
        if self._predicates == None:
            self._predicates = dict()
        self._predicates[22] = self.or_expr_sempred
        self._predicates[23] = self.and_expr_sempred
        self._predicates[24] = self.rel_expr_sempred
        self._predicates[25] = self.add_expr_sempred
        self._predicates[26] = self.mult_expr_sempred
        self._predicates[27] = self.unary_expr_sempred
        pred = self._predicates.get(ruleIndex, None)
        if pred is None:
            raise Exception("No predicate with index:" + str(ruleIndex))
        else:
            return pred(localctx, predIndex)

    def or_expr_sempred(self, localctx, predIndex):
            if predIndex == 0:
                return self.precpred(self._ctx, 1)
         

    def and_expr_sempred(self, localctx, predIndex):
            if predIndex == 1:
                return self.precpred(self._ctx, 1)
         

    def rel_expr_sempred(self, localctx, predIndex):
            if predIndex == 2:
                return self.precpred(self._ctx, 1)
         

    def add_expr_sempred(self, localctx, predIndex):
            if predIndex == 3:
                return self.precpred(self._ctx, 1)
         

    def mult_expr_sempred(self, localctx, predIndex):
            if predIndex == 4:
                return self.precpred(self._ctx, 1)
         

    def unary_expr_sempred(self, localctx, predIndex):
            if predIndex == 5:
                return self.precpred(self._ctx, 1)
         




