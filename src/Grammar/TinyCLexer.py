# Generated from ./Grammar/TinyC.g4 by ANTLR 4.5.2
# encoding: utf-8
from __future__ import print_function
from antlr4 import *
from io import StringIO


def serializedATN():
    with StringIO() as buf:
        buf.write(u"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2")
        buf.write(u"<\u0180\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4")
        buf.write(u"\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t\13\4\f\t\f\4\r")
        buf.write(u"\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22")
        buf.write(u"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4")
        buf.write(u"\30\t\30\4\31\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35")
        buf.write(u"\t\35\4\36\t\36\4\37\t\37\4 \t \4!\t!\4\"\t\"\4#\t#\4")
        buf.write(u"$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4,\t")
        buf.write(u",\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63")
        buf.write(u"\t\63\4\64\t\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\4")
        buf.write(u"9\t9\4:\t:\4;\t;\4<\t<\4=\t=\3\2\3\2\3\2\3\2\3\2\3\3")
        buf.write(u"\3\3\3\3\3\3\3\3\3\4\3\4\3\4\3\4\3\5\3\5\3\5\3\6\3\6")
        buf.write(u"\3\6\3\6\3\6\3\7\3\7\3\7\3\7\3\b\3\b\3\b\3\b\3\b\3\t")
        buf.write(u"\3\t\3\t\3\t\3\t\3\t\3\n\3\n\3\n\3\n\3\n\3\n\3\13\3\13")
        buf.write(u"\3\13\3\13\3\13\3\13\3\13\3\f\3\f\3\f\3\f\3\f\3\r\3\r")
        buf.write(u"\3\r\3\r\3\r\3\r\3\16\3\16\3\17\3\17\3\20\3\20\3\21\3")
        buf.write(u"\21\3\22\3\22\3\23\3\23\3\24\3\24\3\25\3\25\3\25\3\26")
        buf.write(u"\3\26\3\27\3\27\3\27\3\30\3\30\3\31\3\31\3\31\3\32\3")
        buf.write(u"\32\3\33\3\33\3\33\3\34\3\34\3\35\3\35\3\36\3\36\3\37")
        buf.write(u"\3\37\3 \3 \3 \3!\3!\3!\3\"\3\"\3#\3#\3$\3$\3%\3%\3&")
        buf.write(u"\3&\3\'\3\'\3\'\3(\3(\3(\3)\3)\3)\3*\3*\3*\3+\3+\3+\3")
        buf.write(u",\3,\3,\3-\3-\3-\3.\3.\3.\3/\3/\3\60\3\60\3\60\3\60\3")
        buf.write(u"\60\3\60\3\60\3\60\3\60\3\60\3\60\3\60\3\60\3\60\3\60")
        buf.write(u"\3\60\3\60\3\60\3\60\3\61\3\61\3\61\3\61\3\61\3\61\3")
        buf.write(u"\61\3\61\3\61\3\62\3\62\3\62\3\62\3\62\3\62\3\63\3\63")
        buf.write(u"\3\63\7\63\u0130\n\63\f\63\16\63\u0133\13\63\3\64\7\64")
        buf.write(u"\u0136\n\64\f\64\16\64\u0139\13\64\3\64\5\64\u013c\n")
        buf.write(u"\64\3\64\6\64\u013f\n\64\r\64\16\64\u0140\3\65\3\65\5")
        buf.write(u"\65\u0145\n\65\3\66\3\66\7\66\u0149\n\66\f\66\16\66\u014c")
        buf.write(u"\13\66\3\66\3\66\3\67\3\67\3\67\3\67\38\38\39\39\3:\6")
        buf.write(u":\u0159\n:\r:\16:\u015a\3:\3:\3;\3;\5;\u0161\n;\3;\5")
        buf.write(u";\u0164\n;\3;\3;\3<\3<\3<\3<\7<\u016c\n<\f<\16<\u016f")
        buf.write(u"\13<\3<\3<\3<\3<\3<\3=\3=\3=\3=\7=\u017a\n=\f=\16=\u017d")
        buf.write(u"\13=\3=\3=\4\u014a\u016d\2>\3\3\5\4\7\5\t\6\13\7\r\b")
        buf.write(u"\17\t\21\n\23\13\25\f\27\r\31\16\33\17\35\20\37\21!\22")
        buf.write(u"#\23%\24\'\25)\26+\27-\30/\31\61\32\63\33\65\34\67\35")
        buf.write(u"9\36;\37= ?!A\"C#E$G%I&K\'M(O)Q*S+U,W-Y.[/]\60_\61a\62")
        buf.write(u"c\63e\64g\65i\66k\67m8o\2q\2s9u:w;y<\3\2\6\3\2\62;\5")
        buf.write(u"\2C\\aac|\4\2\13\13\"\"\4\2\f\f\17\17\u0189\2\3\3\2\2")
        buf.write(u"\2\2\5\3\2\2\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2")
        buf.write(u"\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3\2\2\2\2\25")
        buf.write(u"\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35")
        buf.write(u"\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2")
        buf.write(u"\2\2\'\3\2\2\2\2)\3\2\2\2\2+\3\2\2\2\2-\3\2\2\2\2/\3")
        buf.write(u"\2\2\2\2\61\3\2\2\2\2\63\3\2\2\2\2\65\3\2\2\2\2\67\3")
        buf.write(u"\2\2\2\29\3\2\2\2\2;\3\2\2\2\2=\3\2\2\2\2?\3\2\2\2\2")
        buf.write(u"A\3\2\2\2\2C\3\2\2\2\2E\3\2\2\2\2G\3\2\2\2\2I\3\2\2\2")
        buf.write(u"\2K\3\2\2\2\2M\3\2\2\2\2O\3\2\2\2\2Q\3\2\2\2\2S\3\2\2")
        buf.write(u"\2\2U\3\2\2\2\2W\3\2\2\2\2Y\3\2\2\2\2[\3\2\2\2\2]\3\2")
        buf.write(u"\2\2\2_\3\2\2\2\2a\3\2\2\2\2c\3\2\2\2\2e\3\2\2\2\2g\3")
        buf.write(u"\2\2\2\2i\3\2\2\2\2k\3\2\2\2\2m\3\2\2\2\2s\3\2\2\2\2")
        buf.write(u"u\3\2\2\2\2w\3\2\2\2\2y\3\2\2\2\3{\3\2\2\2\5\u0080\3")
        buf.write(u"\2\2\2\7\u0085\3\2\2\2\t\u0089\3\2\2\2\13\u008c\3\2\2")
        buf.write(u"\2\r\u0091\3\2\2\2\17\u0095\3\2\2\2\21\u009a\3\2\2\2")
        buf.write(u"\23\u00a0\3\2\2\2\25\u00a6\3\2\2\2\27\u00ad\3\2\2\2\31")
        buf.write(u"\u00b2\3\2\2\2\33\u00b8\3\2\2\2\35\u00ba\3\2\2\2\37\u00bc")
        buf.write(u"\3\2\2\2!\u00be\3\2\2\2#\u00c0\3\2\2\2%\u00c2\3\2\2\2")
        buf.write(u"\'\u00c4\3\2\2\2)\u00c6\3\2\2\2+\u00c9\3\2\2\2-\u00cb")
        buf.write(u"\3\2\2\2/\u00ce\3\2\2\2\61\u00d0\3\2\2\2\63\u00d3\3\2")
        buf.write(u"\2\2\65\u00d5\3\2\2\2\67\u00d8\3\2\2\29\u00da\3\2\2\2")
        buf.write(u";\u00dc\3\2\2\2=\u00de\3\2\2\2?\u00e0\3\2\2\2A\u00e3")
        buf.write(u"\3\2\2\2C\u00e6\3\2\2\2E\u00e8\3\2\2\2G\u00ea\3\2\2\2")
        buf.write(u"I\u00ec\3\2\2\2K\u00ee\3\2\2\2M\u00f0\3\2\2\2O\u00f3")
        buf.write(u"\3\2\2\2Q\u00f6\3\2\2\2S\u00f9\3\2\2\2U\u00fc\3\2\2\2")
        buf.write(u"W\u00ff\3\2\2\2Y\u0102\3\2\2\2[\u0105\3\2\2\2]\u0108")
        buf.write(u"\3\2\2\2_\u010a\3\2\2\2a\u011d\3\2\2\2c\u0126\3\2\2\2")
        buf.write(u"e\u012c\3\2\2\2g\u013b\3\2\2\2i\u0144\3\2\2\2k\u0146")
        buf.write(u"\3\2\2\2m\u014f\3\2\2\2o\u0153\3\2\2\2q\u0155\3\2\2\2")
        buf.write(u"s\u0158\3\2\2\2u\u0163\3\2\2\2w\u0167\3\2\2\2y\u0175")
        buf.write(u"\3\2\2\2{|\7e\2\2|}\7j\2\2}~\7c\2\2~\177\7t\2\2\177\4")
        buf.write(u"\3\2\2\2\u0080\u0081\7g\2\2\u0081\u0082\7n\2\2\u0082")
        buf.write(u"\u0083\7u\2\2\u0083\u0084\7g\2\2\u0084\6\3\2\2\2\u0085")
        buf.write(u"\u0086\7h\2\2\u0086\u0087\7q\2\2\u0087\u0088\7t\2\2\u0088")
        buf.write(u"\b\3\2\2\2\u0089\u008a\7k\2\2\u008a\u008b\7h\2\2\u008b")
        buf.write(u"\n\3\2\2\2\u008c\u008d\7d\2\2\u008d\u008e\7q\2\2\u008e")
        buf.write(u"\u008f\7q\2\2\u008f\u0090\7n\2\2\u0090\f\3\2\2\2\u0091")
        buf.write(u"\u0092\7k\2\2\u0092\u0093\7p\2\2\u0093\u0094\7v\2\2\u0094")
        buf.write(u"\16\3\2\2\2\u0095\u0096\7v\2\2\u0096\u0097\7t\2\2\u0097")
        buf.write(u"\u0098\7w\2\2\u0098\u0099\7g\2\2\u0099\20\3\2\2\2\u009a")
        buf.write(u"\u009b\7h\2\2\u009b\u009c\7c\2\2\u009c\u009d\7n\2\2\u009d")
        buf.write(u"\u009e\7u\2\2\u009e\u009f\7g\2\2\u009f\22\3\2\2\2\u00a0")
        buf.write(u"\u00a1\7h\2\2\u00a1\u00a2\7n\2\2\u00a2\u00a3\7q\2\2\u00a3")
        buf.write(u"\u00a4\7c\2\2\u00a4\u00a5\7v\2\2\u00a5\24\3\2\2\2\u00a6")
        buf.write(u"\u00a7\7t\2\2\u00a7\u00a8\7g\2\2\u00a8\u00a9\7v\2\2\u00a9")
        buf.write(u"\u00aa\7w\2\2\u00aa\u00ab\7t\2\2\u00ab\u00ac\7p\2\2\u00ac")
        buf.write(u"\26\3\2\2\2\u00ad\u00ae\7x\2\2\u00ae\u00af\7q\2\2\u00af")
        buf.write(u"\u00b0\7k\2\2\u00b0\u00b1\7f\2\2\u00b1\30\3\2\2\2\u00b2")
        buf.write(u"\u00b3\7y\2\2\u00b3\u00b4\7j\2\2\u00b4\u00b5\7k\2\2\u00b5")
        buf.write(u"\u00b6\7n\2\2\u00b6\u00b7\7g\2\2\u00b7\32\3\2\2\2\u00b8")
        buf.write(u"\u00b9\7*\2\2\u00b9\34\3\2\2\2\u00ba\u00bb\7+\2\2\u00bb")
        buf.write(u"\36\3\2\2\2\u00bc\u00bd\7]\2\2\u00bd \3\2\2\2\u00be\u00bf")
        buf.write(u"\7_\2\2\u00bf\"\3\2\2\2\u00c0\u00c1\7}\2\2\u00c1$\3\2")
        buf.write(u"\2\2\u00c2\u00c3\7\177\2\2\u00c3&\3\2\2\2\u00c4\u00c5")
        buf.write(u"\7>\2\2\u00c5(\3\2\2\2\u00c6\u00c7\7>\2\2\u00c7\u00c8")
        buf.write(u"\7?\2\2\u00c8*\3\2\2\2\u00c9\u00ca\7@\2\2\u00ca,\3\2")
        buf.write(u"\2\2\u00cb\u00cc\7@\2\2\u00cc\u00cd\7?\2\2\u00cd.\3\2")
        buf.write(u"\2\2\u00ce\u00cf\7-\2\2\u00cf\60\3\2\2\2\u00d0\u00d1")
        buf.write(u"\7-\2\2\u00d1\u00d2\7-\2\2\u00d2\62\3\2\2\2\u00d3\u00d4")
        buf.write(u"\7/\2\2\u00d4\64\3\2\2\2\u00d5\u00d6\7/\2\2\u00d6\u00d7")
        buf.write(u"\7/\2\2\u00d7\66\3\2\2\2\u00d8\u00d9\7,\2\2\u00d98\3")
        buf.write(u"\2\2\2\u00da\u00db\7(\2\2\u00db:\3\2\2\2\u00dc\u00dd")
        buf.write(u"\7\61\2\2\u00dd<\3\2\2\2\u00de\u00df\7\'\2\2\u00df>\3")
        buf.write(u"\2\2\2\u00e0\u00e1\7(\2\2\u00e1\u00e2\7(\2\2\u00e2@\3")
        buf.write(u"\2\2\2\u00e3\u00e4\7~\2\2\u00e4\u00e5\7~\2\2\u00e5B\3")
        buf.write(u"\2\2\2\u00e6\u00e7\7`\2\2\u00e7D\3\2\2\2\u00e8\u00e9")
        buf.write(u"\7#\2\2\u00e9F\3\2\2\2\u00ea\u00eb\7=\2\2\u00ebH\3\2")
        buf.write(u"\2\2\u00ec\u00ed\7.\2\2\u00edJ\3\2\2\2\u00ee\u00ef\7")
        buf.write(u"?\2\2\u00efL\3\2\2\2\u00f0\u00f1\7,\2\2\u00f1\u00f2\7")
        buf.write(u"?\2\2\u00f2N\3\2\2\2\u00f3\u00f4\7\61\2\2\u00f4\u00f5")
        buf.write(u"\7?\2\2\u00f5P\3\2\2\2\u00f6\u00f7\7\'\2\2\u00f7\u00f8")
        buf.write(u"\7?\2\2\u00f8R\3\2\2\2\u00f9\u00fa\7-\2\2\u00fa\u00fb")
        buf.write(u"\7?\2\2\u00fbT\3\2\2\2\u00fc\u00fd\7/\2\2\u00fd\u00fe")
        buf.write(u"\7?\2\2\u00feV\3\2\2\2\u00ff\u0100\7?\2\2\u0100\u0101")
        buf.write(u"\7?\2\2\u0101X\3\2\2\2\u0102\u0103\7#\2\2\u0103\u0104")
        buf.write(u"\7?\2\2\u0104Z\3\2\2\2\u0105\u0106\7/\2\2\u0106\u0107")
        buf.write(u"\7@\2\2\u0107\\\3\2\2\2\u0108\u0109\7\60\2\2\u0109^\3")
        buf.write(u"\2\2\2\u010a\u010b\7%\2\2\u010b\u010c\7k\2\2\u010c\u010d")
        buf.write(u"\7p\2\2\u010d\u010e\7e\2\2\u010e\u010f\7n\2\2\u010f\u0110")
        buf.write(u"\7w\2\2\u0110\u0111\7f\2\2\u0111\u0112\7g\2\2\u0112\u0113")
        buf.write(u"\7\"\2\2\u0113\u0114\7>\2\2\u0114\u0115\7u\2\2\u0115")
        buf.write(u"\u0116\7v\2\2\u0116\u0117\7f\2\2\u0117\u0118\7k\2\2\u0118")
        buf.write(u"\u0119\7q\2\2\u0119\u011a\7\60\2\2\u011a\u011b\7j\2\2")
        buf.write(u"\u011b\u011c\7@\2\2\u011c`\3\2\2\2\u011d\u011e\7e\2\2")
        buf.write(u"\u011e\u011f\7q\2\2\u011f\u0120\7p\2\2\u0120\u0121\7")
        buf.write(u"v\2\2\u0121\u0122\7k\2\2\u0122\u0123\7p\2\2\u0123\u0124")
        buf.write(u"\7w\2\2\u0124\u0125\7g\2\2\u0125b\3\2\2\2\u0126\u0127")
        buf.write(u"\7d\2\2\u0127\u0128\7t\2\2\u0128\u0129\7g\2\2\u0129\u012a")
        buf.write(u"\7c\2\2\u012a\u012b\7m\2\2\u012bd\3\2\2\2\u012c\u0131")
        buf.write(u"\5q9\2\u012d\u0130\5o8\2\u012e\u0130\5q9\2\u012f\u012d")
        buf.write(u"\3\2\2\2\u012f\u012e\3\2\2\2\u0130\u0133\3\2\2\2\u0131")
        buf.write(u"\u012f\3\2\2\2\u0131\u0132\3\2\2\2\u0132f\3\2\2\2\u0133")
        buf.write(u"\u0131\3\2\2\2\u0134\u0136\5o8\2\u0135\u0134\3\2\2\2")
        buf.write(u"\u0136\u0139\3\2\2\2\u0137\u0135\3\2\2\2\u0137\u0138")
        buf.write(u"\3\2\2\2\u0138\u013a\3\2\2\2\u0139\u0137\3\2\2\2\u013a")
        buf.write(u"\u013c\5]/\2\u013b\u0137\3\2\2\2\u013b\u013c\3\2\2\2")
        buf.write(u"\u013c\u013e\3\2\2\2\u013d\u013f\5o8\2\u013e\u013d\3")
        buf.write(u"\2\2\2\u013f\u0140\3\2\2\2\u0140\u013e\3\2\2\2\u0140")
        buf.write(u"\u0141\3\2\2\2\u0141h\3\2\2\2\u0142\u0145\5\17\b\2\u0143")
        buf.write(u"\u0145\5\21\t\2\u0144\u0142\3\2\2\2\u0144\u0143\3\2\2")
        buf.write(u"\2\u0145j\3\2\2\2\u0146\u014a\7$\2\2\u0147\u0149\13\2")
        buf.write(u"\2\2\u0148\u0147\3\2\2\2\u0149\u014c\3\2\2\2\u014a\u014b")
        buf.write(u"\3\2\2\2\u014a\u0148\3\2\2\2\u014b\u014d\3\2\2\2\u014c")
        buf.write(u"\u014a\3\2\2\2\u014d\u014e\7$\2\2\u014el\3\2\2\2\u014f")
        buf.write(u"\u0150\7)\2\2\u0150\u0151\13\2\2\2\u0151\u0152\7)\2\2")
        buf.write(u"\u0152n\3\2\2\2\u0153\u0154\t\2\2\2\u0154p\3\2\2\2\u0155")
        buf.write(u"\u0156\t\3\2\2\u0156r\3\2\2\2\u0157\u0159\t\4\2\2\u0158")
        buf.write(u"\u0157\3\2\2\2\u0159\u015a\3\2\2\2\u015a\u0158\3\2\2")
        buf.write(u"\2\u015a\u015b\3\2\2\2\u015b\u015c\3\2\2\2\u015c\u015d")
        buf.write(u"\b:\2\2\u015dt\3\2\2\2\u015e\u0160\7\17\2\2\u015f\u0161")
        buf.write(u"\7\f\2\2\u0160\u015f\3\2\2\2\u0160\u0161\3\2\2\2\u0161")
        buf.write(u"\u0164\3\2\2\2\u0162\u0164\7\f\2\2\u0163\u015e\3\2\2")
        buf.write(u"\2\u0163\u0162\3\2\2\2\u0164\u0165\3\2\2\2\u0165\u0166")
        buf.write(u"\b;\2\2\u0166v\3\2\2\2\u0167\u0168\7\61\2\2\u0168\u0169")
        buf.write(u"\7,\2\2\u0169\u016d\3\2\2\2\u016a\u016c\13\2\2\2\u016b")
        buf.write(u"\u016a\3\2\2\2\u016c\u016f\3\2\2\2\u016d\u016e\3\2\2")
        buf.write(u"\2\u016d\u016b\3\2\2\2\u016e\u0170\3\2\2\2\u016f\u016d")
        buf.write(u"\3\2\2\2\u0170\u0171\7,\2\2\u0171\u0172\7\61\2\2\u0172")
        buf.write(u"\u0173\3\2\2\2\u0173\u0174\b<\2\2\u0174x\3\2\2\2\u0175")
        buf.write(u"\u0176\7\61\2\2\u0176\u0177\7\61\2\2\u0177\u017b\3\2")
        buf.write(u"\2\2\u0178\u017a\n\5\2\2\u0179\u0178\3\2\2\2\u017a\u017d")
        buf.write(u"\3\2\2\2\u017b\u0179\3\2\2\2\u017b\u017c\3\2\2\2\u017c")
        buf.write(u"\u017e\3\2\2\2\u017d\u017b\3\2\2\2\u017e\u017f\b=\2\2")
        buf.write(u"\u017fz\3\2\2\2\17\2\u012f\u0131\u0137\u013b\u0140\u0144")
        buf.write(u"\u014a\u015a\u0160\u0163\u016d\u017b\3\b\2\2")
        return buf.getvalue()


class TinyCLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]


    Char = 1
    Else = 2
    For = 3
    If = 4
    Bool = 5
    Int = 6
    MTrue = 7
    MFalse = 8
    Float = 9
    Return = 10
    Void = 11
    While = 12
    LeftParen = 13
    RightParen = 14
    LeftBracket = 15
    RightBracket = 16
    LeftBrace = 17
    RightBrace = 18
    Less = 19
    LessEqual = 20
    Greater = 21
    GreaterEqual = 22
    Plus = 23
    PlusPlus = 24
    Minus = 25
    MinusMinus = 26
    Star = 27
    Ref = 28
    Div = 29
    Mod = 30
    AndAnd = 31
    OrOr = 32
    Caret = 33
    Not = 34
    Semi = 35
    Comma = 36
    Assign = 37
    StarAssign = 38
    DivAssign = 39
    ModAssign = 40
    PlusAssign = 41
    MinusAssign = 42
    Equal = 43
    NotEqual = 44
    Arrow = 45
    Dot = 46
    Include = 47
    Continue = 48
    Break = 49
    Identifier = 50
    Number = 51
    Boolean = 52
    String = 53
    Character = 54
    Whitespace = 55
    Newline = 56
    BlockComment = 57
    LineComment = 58

    modeNames = [ u"DEFAULT_MODE" ]

    literalNames = [ u"<INVALID>",
            u"'char'", u"'else'", u"'for'", u"'if'", u"'bool'", u"'int'", 
            u"'true'", u"'false'", u"'float'", u"'return'", u"'void'", u"'while'", 
            u"'('", u"')'", u"'['", u"']'", u"'{'", u"'}'", u"'<'", u"'<='", 
            u"'>'", u"'>='", u"'+'", u"'++'", u"'-'", u"'--'", u"'*'", u"'&'", 
            u"'/'", u"'%'", u"'&&'", u"'||'", u"'^'", u"'!'", u"';'", u"','", 
            u"'='", u"'*='", u"'/='", u"'%='", u"'+='", u"'-='", u"'=='", 
            u"'!='", u"'->'", u"'.'", u"'#include <stdio.h>'", u"'continue'", 
            u"'break'" ]

    symbolicNames = [ u"<INVALID>",
            u"Char", u"Else", u"For", u"If", u"Bool", u"Int", u"MTrue", 
            u"MFalse", u"Float", u"Return", u"Void", u"While", u"LeftParen", 
            u"RightParen", u"LeftBracket", u"RightBracket", u"LeftBrace", 
            u"RightBrace", u"Less", u"LessEqual", u"Greater", u"GreaterEqual", 
            u"Plus", u"PlusPlus", u"Minus", u"MinusMinus", u"Star", u"Ref", 
            u"Div", u"Mod", u"AndAnd", u"OrOr", u"Caret", u"Not", u"Semi", 
            u"Comma", u"Assign", u"StarAssign", u"DivAssign", u"ModAssign", 
            u"PlusAssign", u"MinusAssign", u"Equal", u"NotEqual", u"Arrow", 
            u"Dot", u"Include", u"Continue", u"Break", u"Identifier", u"Number", 
            u"Boolean", u"String", u"Character", u"Whitespace", u"Newline", 
            u"BlockComment", u"LineComment" ]

    ruleNames = [ u"Char", u"Else", u"For", u"If", u"Bool", u"Int", u"MTrue", 
                  u"MFalse", u"Float", u"Return", u"Void", u"While", u"LeftParen", 
                  u"RightParen", u"LeftBracket", u"RightBracket", u"LeftBrace", 
                  u"RightBrace", u"Less", u"LessEqual", u"Greater", u"GreaterEqual", 
                  u"Plus", u"PlusPlus", u"Minus", u"MinusMinus", u"Star", 
                  u"Ref", u"Div", u"Mod", u"AndAnd", u"OrOr", u"Caret", 
                  u"Not", u"Semi", u"Comma", u"Assign", u"StarAssign", u"DivAssign", 
                  u"ModAssign", u"PlusAssign", u"MinusAssign", u"Equal", 
                  u"NotEqual", u"Arrow", u"Dot", u"Include", u"Continue", 
                  u"Break", u"Identifier", u"Number", u"Boolean", u"String", 
                  u"Character", u"Digit", u"Nondigit", u"Whitespace", u"Newline", 
                  u"BlockComment", u"LineComment" ]

    grammarFileName = u"TinyC.g4"

    def __init__(self, input=None):
        super(TinyCLexer, self).__init__(input)
        self.checkVersion("4.5.2")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


