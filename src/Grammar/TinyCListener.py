# Generated from ./Grammar/TinyC.g4 by ANTLR 4.5.2
from antlr4 import *

# This class defines a complete listener for a parse tree produced by TinyCParser.
class TinyCListener(ParseTreeListener):

    # Enter a parse tree produced by TinyCParser#parse.
    def enterParse(self, ctx):
        pass

    # Exit a parse tree produced by TinyCParser#parse.
    def exitParse(self, ctx):
        pass


    # Enter a parse tree produced by TinyCParser#compilation_unit.
    def enterCompilation_unit(self, ctx):
        pass

    # Exit a parse tree produced by TinyCParser#compilation_unit.
    def exitCompilation_unit(self, ctx):
        pass


    # Enter a parse tree produced by TinyCParser#forward_declaration.
    def enterForward_declaration(self, ctx):
        pass

    # Exit a parse tree produced by TinyCParser#forward_declaration.
    def exitForward_declaration(self, ctx):
        pass


    # Enter a parse tree produced by TinyCParser#m_include.
    def enterM_include(self, ctx):
        pass

    # Exit a parse tree produced by TinyCParser#m_include.
    def exitM_include(self, ctx):
        pass


    # Enter a parse tree produced by TinyCParser#declaration.
    def enterDeclaration(self, ctx):
        pass

    # Exit a parse tree produced by TinyCParser#declaration.
    def exitDeclaration(self, ctx):
        pass


    # Enter a parse tree produced by TinyCParser#simpleDecl.
    def enterSimpleDecl(self, ctx):
        pass

    # Exit a parse tree produced by TinyCParser#simpleDecl.
    def exitSimpleDecl(self, ctx):
        pass


    # Enter a parse tree produced by TinyCParser#m_type.
    def enterM_type(self, ctx):
        pass

    # Exit a parse tree produced by TinyCParser#m_type.
    def exitM_type(self, ctx):
        pass


    # Enter a parse tree produced by TinyCParser#varType.
    def enterVarType(self, ctx):
        pass

    # Exit a parse tree produced by TinyCParser#varType.
    def exitVarType(self, ctx):
        pass


    # Enter a parse tree produced by TinyCParser#variable_decl.
    def enterVariable_decl(self, ctx):
        pass

    # Exit a parse tree produced by TinyCParser#variable_decl.
    def exitVariable_decl(self, ctx):
        pass


    # Enter a parse tree produced by TinyCParser#variable_id.
    def enterVariable_id(self, ctx):
        pass

    # Exit a parse tree produced by TinyCParser#variable_id.
    def exitVariable_id(self, ctx):
        pass


    # Enter a parse tree produced by TinyCParser#array_index.
    def enterArray_index(self, ctx):
        pass

    # Exit a parse tree produced by TinyCParser#array_index.
    def exitArray_index(self, ctx):
        pass


    # Enter a parse tree produced by TinyCParser#function_declaration.
    def enterFunction_declaration(self, ctx):
        pass

    # Exit a parse tree produced by TinyCParser#function_declaration.
    def exitFunction_declaration(self, ctx):
        pass


    # Enter a parse tree produced by TinyCParser#parameter_list.
    def enterParameter_list(self, ctx):
        pass

    # Exit a parse tree produced by TinyCParser#parameter_list.
    def exitParameter_list(self, ctx):
        pass


    # Enter a parse tree produced by TinyCParser#parameter.
    def enterParameter(self, ctx):
        pass

    # Exit a parse tree produced by TinyCParser#parameter.
    def exitParameter(self, ctx):
        pass


    # Enter a parse tree produced by TinyCParser#block.
    def enterBlock(self, ctx):
        pass

    # Exit a parse tree produced by TinyCParser#block.
    def exitBlock(self, ctx):
        pass


    # Enter a parse tree produced by TinyCParser#statement.
    def enterStatement(self, ctx):
        pass

    # Exit a parse tree produced by TinyCParser#statement.
    def exitStatement(self, ctx):
        pass


    # Enter a parse tree produced by TinyCParser#if_statement.
    def enterIf_statement(self, ctx):
        pass

    # Exit a parse tree produced by TinyCParser#if_statement.
    def exitIf_statement(self, ctx):
        pass


    # Enter a parse tree produced by TinyCParser#else_statement.
    def enterElse_statement(self, ctx):
        pass

    # Exit a parse tree produced by TinyCParser#else_statement.
    def exitElse_statement(self, ctx):
        pass


    # Enter a parse tree produced by TinyCParser#while_statement.
    def enterWhile_statement(self, ctx):
        pass

    # Exit a parse tree produced by TinyCParser#while_statement.
    def exitWhile_statement(self, ctx):
        pass


    # Enter a parse tree produced by TinyCParser#for_statement.
    def enterFor_statement(self, ctx):
        pass

    # Exit a parse tree produced by TinyCParser#for_statement.
    def exitFor_statement(self, ctx):
        pass


    # Enter a parse tree produced by TinyCParser#return_statement.
    def enterReturn_statement(self, ctx):
        pass

    # Exit a parse tree produced by TinyCParser#return_statement.
    def exitReturn_statement(self, ctx):
        pass


    # Enter a parse tree produced by TinyCParser#expression.
    def enterExpression(self, ctx):
        pass

    # Exit a parse tree produced by TinyCParser#expression.
    def exitExpression(self, ctx):
        pass


    # Enter a parse tree produced by TinyCParser#or_expr.
    def enterOr_expr(self, ctx):
        pass

    # Exit a parse tree produced by TinyCParser#or_expr.
    def exitOr_expr(self, ctx):
        pass


    # Enter a parse tree produced by TinyCParser#and_expr.
    def enterAnd_expr(self, ctx):
        pass

    # Exit a parse tree produced by TinyCParser#and_expr.
    def exitAnd_expr(self, ctx):
        pass


    # Enter a parse tree produced by TinyCParser#rel_expr.
    def enterRel_expr(self, ctx):
        pass

    # Exit a parse tree produced by TinyCParser#rel_expr.
    def exitRel_expr(self, ctx):
        pass


    # Enter a parse tree produced by TinyCParser#add_expr.
    def enterAdd_expr(self, ctx):
        pass

    # Exit a parse tree produced by TinyCParser#add_expr.
    def exitAdd_expr(self, ctx):
        pass


    # Enter a parse tree produced by TinyCParser#mult_expr.
    def enterMult_expr(self, ctx):
        pass

    # Exit a parse tree produced by TinyCParser#mult_expr.
    def exitMult_expr(self, ctx):
        pass


    # Enter a parse tree produced by TinyCParser#unary_expr.
    def enterUnary_expr(self, ctx):
        pass

    # Exit a parse tree produced by TinyCParser#unary_expr.
    def exitUnary_expr(self, ctx):
        pass


    # Enter a parse tree produced by TinyCParser#pf_expr.
    def enterPf_expr(self, ctx):
        pass

    # Exit a parse tree produced by TinyCParser#pf_expr.
    def exitPf_expr(self, ctx):
        pass


    # Enter a parse tree produced by TinyCParser#arg_list.
    def enterArg_list(self, ctx):
        pass

    # Exit a parse tree produced by TinyCParser#arg_list.
    def exitArg_list(self, ctx):
        pass


    # Enter a parse tree produced by TinyCParser#pr_expr.
    def enterPr_expr(self, ctx):
        pass

    # Exit a parse tree produced by TinyCParser#pr_expr.
    def exitPr_expr(self, ctx):
        pass


