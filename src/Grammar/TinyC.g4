grammar TinyC;

parse : compilation_unit EOF;

compilation_unit : (m_include | forward_declaration | declaration | function_declaration)*;

forward_declaration : m_type Identifier LeftParen parameter_list RightParen Semi;

m_include: Include;

declaration : simpleDecl;

simpleDecl : m_type variable_decl Semi?;

m_type: varType;

varType: (Char | Int | Void | Float | Bool) (Star)?;

variable_decl : variable_id (Assign expression)?;

variable_id : Identifier (array_index)?;

array_index : LeftBracket (expression)? RightBracket;

function_declaration : m_type Identifier LeftParen parameter_list RightParen ((LeftBrace blck=block RightBrace)|Semi);

parameter_list : (parameter (Comma parameter)*)? | Void;

parameter : m_type Identifier (array_index)?;

block : statement* ;

statement : if_statement (Semi)?
		  | while_statement (Semi)?
		  | for_statement (Semi)?
		  | return_statement Semi
		  | expression Semi
		  | declaration Semi
		  | Continue Semi
		  | Break Semi
		  ;


if_statement : If LeftParen expression RightParen (statement | LeftBrace block RightBrace) else_statement?;

else_statement : Else (statement | LeftBrace block RightBrace);

while_statement : While LeftParen expression RightParen (statement | LeftBrace block RightBrace);

for_statement : For LeftParen (e1=statement)? (Semi)? (e2=expression) Semi (e3=expression)? RightParen (statement | LeftBrace block RightBrace);

return_statement: Return (expression)?;

//In order to maintain an order of expressions, we follow the following pattern:

expression  : or_expr
			| pr_expr (Assign|StarAssign|DivAssign|ModAssign|PlusAssign|MinusAssign) expression
			;

//Or has highest Precedence
or_expr : and_expr
		| or_expr OrOr and_expr;

and_expr : rel_expr
		 | and_expr AndAnd rel_expr;

rel_expr : add_expr
		 | rel_expr (Less | LessEqual | Greater | GreaterEqual | Equal | NotEqual) add_expr;

add_expr : mult_expr
		 | add_expr (Plus | Minus) mult_expr;

mult_expr : unary_expr
		  | mult_expr ( Star | Div | Mod ) unary_expr;

unary_expr : pf_expr
		   | (Minus|Not|prepp= PlusPlus|premm=MinusMinus) unary_expr
		   | unary_expr (postpp= PlusPlus|postmm=MinusMinus);

pf_expr : Identifier LeftParen (arg_list)? RightParen
		| pr_expr;

arg_list: expression (Comma expression)*;

pr_expr : (Star|Ref)? Identifier (array_index)?
	| Number
	| String
	| Character
	| MTrue | MFalse
	| LeftParen expression RightParen
	| LeftBrace arg_list RightBrace // Array assignments
	;

/* Lexer rules */
Char : 'char';
Else : 'else';
For : 'for';
If : 'if';
Bool : 'bool';
Int : 'int';
MTrue : 'true';
MFalse : 'false';
Float : 'float';
Return : 'return';
Void : 'void';
While : 'while';
LeftParen : '(';
RightParen : ')';
LeftBracket : '[';
RightBracket : ']';
LeftBrace : '{';
RightBrace : '}';
Less : '<';
LessEqual : '<=';
Greater : '>';
GreaterEqual : '>=';
Plus : '+';
PlusPlus : '++';
Minus : '-';
MinusMinus : '--';
Star : '*';
Ref : '&';
Div : '/';
Mod : '%';
AndAnd : '&&';
OrOr : '||';
Caret : '^';
Not : '!';
Semi : ';';
Comma : ',';
Assign : '=';
StarAssign : '*=';
DivAssign : '/=';
ModAssign : '%=';
PlusAssign : '+=';
MinusAssign : '-=';
Equal : '==';
NotEqual : '!=';
Arrow : '->';
Dot : '.';
Include : '#include <stdio.h>';
Continue : 'continue';
Break : 'break';


Identifier : Nondigit (Digit | Nondigit)*;
Number : (Digit* Dot)? Digit+;
Boolean : MTrue | MFalse;
String : '\"'.*?'\"'; //Non greedy lookahead
Character : '\''.'\'';

fragment Digit : [0-9];
fragment Nondigit : [A-Za-z_];

/* taken from https://github.com/antlr/grammars-v4/blob/master/c/C.g4 */
Whitespace : [ \t]+ -> skip;
Newline : (   '\r' '\n'? | '\n' ) -> skip;
BlockComment : '/*' .*? '*/' -> skip;
LineComment : '//' ~[\r\n]* -> skip;
