#!/usr/bin/env bash
echo "Creating all files needed for Lexing and Parsing with Antlr"
java -jar ./Grammar/antlr-4.5.2-complete.jar -Dlanguage=Python2 ./Grammar/TinyC.g4 -visitor
