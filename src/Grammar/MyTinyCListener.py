# Generated from TinyC.g4 by ANTLR 4.5.2
from compiler.ast import Node

from antlr4 import *
import sys
import re

from AST.ASTNode import *
from AST.CompilationUnitNode import *
from AST.DeclarationNode import *
from AST.FunctionDeclarationNode import *
from AST.AssignmentNode import *
from AST.BlockNode import *
from AST.ExpressionNode import *
from AST.StatementNode import *
from AST.IdentifierNode import *
from AST.TypeNode import *
from AST.LiteralNode import *
from AST.ForwardDeclNode import *

from Exceptions.CustomExceptions import *
from Symboltable.Symbol import *
from Symboltable.SymbolTable import *
from Grammar.TinyCListener import TinyCListener
from Grammar.TinyCParser import *
from Grammar.TinyCLexer import *
from Util.IDGenerator import *
from Util.TreePrinter import *
from Util.Pathfinder import *

# This class defines a complete listener for a parse tree produced by TinyCParser.
class MyTinyCListener(TinyCListener):

    # Enter a parse tree produced by TinyCParser#parse.
    # Parse is just a dummy node to have a clean entry point.
    def enterParse(self, ctx):
        pass

    # Exit a parse tree produced by TinyCParser#parse.
    def exitParse(self, ctx):
        pass

    # Enter a parse tree produced by TinyCParser#compilation_unit.
    def enterCompilation_unit(self, ctx):
        # Create the ASTNode
        # The ST was already created in __init__
        node = CompilationUnitNode(None)
        self.root = node
        self.current = node

        # Check all CU children
        for i in ctx.getChildren():
            try:
                if isinstance(i, TinyCParser.SimpleDeclContext):
                    s = Symbol()
                    s.name = i.Identifier().getText()
                    s.type = i.m_type().getText()
                    s.kind = "m_var"
                    self.currentst.insert(s)
            except ReservedWordException as Rwe:
                Rwe.error(i.start.line, i.start.column)
            except RedeclarationException as Rde:
                Rde.error(i.start.line, i.start.column)


    # Exit a parse tree produced by TinyCParser#compilation_unit.
    def exitCompilation_unit(self, ctx):
        self.current = self.root
        self.currentst = self.globalst
        #Check whether all Forward Declarations have been defined.
        try:
            if not self.globalst.allfwdeclsdefined():
                raise ForwardDeclNotDefinedException()
        except ForwardDeclNotDefinedException as FDNDE:
            FDNDE.error()
        else:
            pass

        holdsmain = False

        if len(self.current.children)>0:
            for c in self.current.children:
                if isinstance(c, FunctionDeclarationNode):
                    if c.m_idname == "main":
                        holdsmain = True
        try:
            if not holdsmain:
                raise IllegalCompilationUnitException()
        except IllegalCompilationUnitException as ICUE:
            ICUE.error()
        else:
            pass

    # Enter a parse tree produced by TinyCParser#forward_declaration.
    def enterForward_declaration(self, ctx):
        node = ForwardDeclNode(self.current)
        node.m_label = ctx.getText()
        node.m_idname = ctx.Identifier().getText()

        partypelist = []
        parnamelist = []

        if (ctx.parameter_list().getChildCount() > 0):
            for c in ctx.parameter_list().children:
                if isinstance(c,TinyCParser.ParameterContext):
                    partype = c.m_type().getText()
                    parname = c.Identifier().getText()#Changed this as of forward decl
                    partypelist.append("m_" + partype)
                    parnamelist.append(parname)#Changed this as of ForwardDecl

        try:
            s = FunctionSymbol(node.m_idname, "m_fun", "m_forward", "m_fun")
            s.paramlist = partypelist
            s.m_parnamelist = parnamelist
            s.parcount = len(s.paramlist)
            f_return = ctx.m_type().getText()
            node.m_type = "m_" + f_return
            s.m_type = node.m_type
            s.m_forwardDecl = True
            tsym = self.currentst.lookupsymbol(node.m_idname)#tsym will hold the already declared symbol
            raise MultipleForwardDeclarationException(node.m_idname)
            self.currentst.insert(s)
        except VariableNotDeclaredException as VNDE:
            self.currentst.insert(s)
            pass
            #Node is not yet in the ST
        except MultipleForwardDeclarationException as MFDE:
            MFDE.error(ctx.start.line, ctx.start.column)
        else:
            pass
        self.current.addChild(node)
        self.current = node
        self.forward_busy = True

    # Exit a parse tree produced by TinyCParser#forward_declaration.
    def exitForward_declaration(self, ctx):
        self.current.children = []
        self.current = self.current.parent
        self.forward_busy = False
        pass


    # Enter a parse tree produced by TinyCParser#m_include.
    def enterM_include(self, ctx):
        # Create the proper AST Node
        node = IncludeNode(self.current,ctx.getText())
        self.current.addChild(node)
        self.current = node
        # Includes just add to the global scope
        # Don't use the insert method, as print and scan are reserved!
        # Will throw errors otherwise
        pfsymbol = FunctionSymbol("printf", "m_void", "m_fun_decl", "m_fun")
        pfsymbol.paramlist.append("VARARGS")
        sfsymbol = FunctionSymbol("scanf", "m_void", "m_fun_decl", "m_fun")
        sfsymbol.paramlist.append("VARARGS")
        self.globalst.symbols.append(pfsymbol)
        self.globalst.symbols.append(sfsymbol)


    # Exit a parse tree produced by TinyCParser#m_include.
    def exitM_include(self, ctx):
        self.current = self.current.parent

    # Enter a parse tree produced by TinyCParser#declaration.
    def enterDeclaration(self, ctx):
        if "=" in ctx.getText():
            node = AssignmentNode(self.current)
            node.m_assigntype = "="
            node.m_decl = True
            self.decl_busy = True
        else:
            node = DeclarationNode(self.current)

        self.current.addChild(node)
        self.current = node

    # Exit a parse tree produced by TinyCParser#declaration.
    def exitDeclaration(self, ctx):
        # CHILDREN: TypeNode (IdentifierNode | ArrayNode) (ExpressionNode)?
        # self.current is of type DeclNode | AssignmentNode
        tnode = self.current.children[0] # TypeNode
        _vtype = tnode.getType() #TODO: Change to m_vartype in tnode | getVarType
        idnode = self.current.children[1] #ArrayNode or IdentifierNode
        self.current.setType(_vtype)
        _id = idnode.m_idname
        self.current.setIdentifier(_id)
        idnode.m_type = _vtype
        self.current.m_idnode = idnode
        size = -1
        _vattribute = "m_none"
        try:
            if (not isinstance(idnode, IdentifierNode) and not isinstance(idnode, ArrayNode)):
                raise ExpressionNotAssignableException()

            if (isinstance(idnode, ArrayNode)):
                idnode.m_size = 0#Set as default, in order to check if already assigned

                if (len(self.current.children) == 2):
                    if ((idnode.m_index == None) or (not isinstance(idnode.m_index, IntegerLitNode))): # a[]; or a[b];
                        raise ExplicitTypeOrInitException(idnode.m_idname)
                    else:
                        idnode.m_size = idnode.m_index.getValue()

                if (len(self.current.children) == 3):
                    if not isinstance(self.current.children[2], (ArgListNode)):
                        raise ExpressionNotAssignableException()
                    self.current.children[2].m_idname = idnode.m_idname
                    #Setting size equal to the size of the init list
                    size = len(self.current.children[2].m_arguments)
                    if (idnode.m_index == None):
                        #No size specified in declaration a[];
                        idnode.m_size = size
                    elif (isinstance(idnode.m_index, IntegerLitNode)):
                        if (size > idnode.m_index.getValue()):
                            raise ExcessElementException(idnode.m_idname)
                        else:
                            #Setting size equal to the size of the declaration

                            idnode.m_size = idnode.m_index.getValue()
                    else:
                        raise VariableArraySizeInitException(idnode.m_idname)

                    for c in self.current.children[2].m_arguments:
                        if _vtype != c.getType():
                            raise TinyCTypeException(_vtype, c.getType())
                _vattribute = "m_arr"
                s = ArraySymbol(_id, _vtype+"[]", _vattribute, "m_var")
                s.m_arraysize = idnode.m_size
                self.currentst.insert(s)
            else: #isinstance(IdentifierNode)
                _vattribute = "m_none"
                if (tnode.m_ptr):
                    _vattribute = "m_ptr"
                    _vtype += "*"
                    self.current.m_idnode.m_ptr = True
                if len(self.current.children) == 3:
                    #Typecheck child 2 with type of child 1
                    if _vtype != self.current.children[2].getType():
                        raise TinyCTypeException(_vtype, self.current.children[2].getType())
                self.currentst.insert(Symbol(_id, _vtype, _vattribute, "m_var"))
        except ReservedWordException as Rwe:
            Rwe.error(ctx.start.line, ctx.start.column)
        except RedeclarationException as Rde:
            Rde.error(ctx.start.line, ctx.start.column)
        except TinyCTypeException as Tte:
            Tte.error(ctx.start.line, ctx.start.column)
        except ExplicitTypeOrInitException as Etie:
            Etie.error(ctx.start.line, ctx.start.column)
        except ExcessElementException as Eee:
            Eee.error(ctx.start.line, ctx.start.column)
        except VariableArraySizeInitException as Vasie:
            Vasie.error(ctx.start.line, ctx.start.column)
        except ExpressionNotAssignableException as Enae:
            Enae.error(ctx.start.line, ctx.start.column)
        else:
            pass
        if self.decl_busy:
            self.decl_busy = False
            oldTypeNode = self.current.children[0]
            self.current.removeChild(0)
            oldIDNode = self.current.children[0]
            self.current.removeChild(0)

            newnode = DeclarationNode(self.current)
            newnode.m_vartype = oldTypeNode.m_type
            newnode.m_idname = oldIDNode.m_idname
            newnode.m_idnode = oldIDNode
            if(_vattribute == "m_ptr"):
                newnode.m_idnode.m_ptr = True
            self.current.children.insert(0,newnode)
        else:
            self.current.removeChild(0)
            self.current.removeChild(0)
        #AssignmentNode should hold a type
        self.current.m_type = _vtype#CHANGED: SINCE code gen
        self.current = self.current.parent


    #TODO: Collapse into: TYPE IDENTIFIER(possibly more) EXPRESSION
    # Enter a parse tree produced by TinyCParser#simpleDecl.
    def enterSimpleDecl(self, ctx):
        pass

    # Exit a parse tree produced by TinyCParser#simpleDecl.
    def exitSimpleDecl(self, ctx):
        pass

    # Enter a parse tree produced by TinyCParser#m_type.
    def enterM_type(self, ctx):
        pass

    # Exit a parse tree produced by TinyCParser#m_type.
    def exitM_type(self, ctx):
        pass


    # Enter a parse tree produced by TinyCParser#varType.
    def enterVarType(self, ctx):
        parent = self.current
        for child in ctx.getChildren():
            if (child.getSymbol().type == TinyCLexer.Int):
                node = IntegerNode(parent)
                self.current.addChild(node)
                self.current = node
            elif (child.getSymbol().type == TinyCLexer.Char):
                node = CharNode(parent)
                self.current.addChild(node)
                self.current = node
            elif (child.getSymbol().type == TinyCLexer.Float):
                node = FloatNode(parent)
                self.current.addChild(node)
                self.current = node
            elif (child.getSymbol().type == TinyCLexer.Void):
                node = VoidNode(parent)
                self.current.addChild(node)
                self.current = node
            elif (child.getSymbol().type == TinyCLexer.Bool):
                node = BoolNode(parent)
                self.current.addChild(node)
                self.current = node
        if (ctx.Star() != None):
            node.m_ptr = True

    # Exit a parse tree produced by TinyCParser#varType.
    def exitVarType(self, ctx):
        self.current = self.current.parent

    # Enter a parse tree produced by TinyCParser#variable_decl.
    def enterVariable_decl(self, ctx):
        pass

    # Exit a parse tree produced by TinyCParser#variable_decl.
    def exitVariable_decl(self, ctx):
        pass


    # Enter a parse tree produced by TinyCParser#variable_id.
    def enterVariable_id(self, ctx):
        if (ctx.getChildCount() == 1):
            node = IdentifierNode(self.current, ctx.Identifier().getText())
        else:
            node = ArrayNode(self.current, ctx.Identifier().getText(), ctx.getText())
        self.current.addChild(node)
        self.current = node


    # Exit a parse tree produced by TinyCParser#variable_id.
    def exitVariable_id(self, ctx):
        node = self.current
        if isinstance(node, IdentifierNode):
            pass
        elif isinstance(node, ArrayNode):
            if (len(node.getChildren()) != 0):
                node.m_index = node.children[0]
                node.removeChild(0)
        self.current = self.current.parent

    # Enter a parse tree produced by TinyCParser#array_index.
    def enterArray_index(self, ctx):
        self.m_index = ctx.getText()
        pass

    # Exit a parse tree produced by TinyCParser#array_index.
    def exitArray_index(self, ctx):
        #TODO: Do something with the expression
#        self.my_parents.pop()
        pass


    # Enter a parse tree produced by TinyCParser#function_declaration.
    def enterFunction_declaration(self, ctx):
        # Create the new AST Node

        node = FunctionDeclarationNode(self.current)
        self.current.addChild(node)
        self.current = node

        f_name = ctx.Identifier().getText()
        # Extract returnvalue_type
        f_return = ctx.m_type().getText()

        partypelist = []
        parnamelist = []

        if (ctx.parameter_list().getChildCount() > 0):
            for c in ctx.parameter_list().children:
                if isinstance(c,TinyCParser.ParameterContext):
                    partype = c.m_type().getText()
                    parname = c.Identifier().getText()#Changed this as of forward decl
                    if c.array_index() != None:
                        partype = partype+"[]"
                    partypelist.append("m_" + partype)
                    parnamelist.append(parname)#Changed this as of ForwardDecl
        node.m_type = "m_" + f_return

        node.m_idname = f_name
        node.m_returnval = node.m_type
        self.current_f_return = node.m_type
        try:
            fsym = self.currentst.existsforwarddecl(node.m_idname)
            #TODO: Check whether the symbols parameters match the current one!
            for x in range(len(partypelist)):
                if fsym.paramlist[x] != partypelist[x]:
                    raise ForwardDeclarationParameterMismatch(node.m_idname)
        except VariableNotDeclaredException as VNDE:
            pass
        except ForwardDeclarationParameterMismatch as FDPM:
            FDPM.error(ctx.start.line, ctx.start.column)
            pass
        else:
            pass

        try:
            s = FunctionSymbol()
            s.name = node.m_idname
            s.m_type = node.m_type
            s.kind = "m_fun"
            s.attribute = "m_fun_decl"
            s.paramlist = partypelist
            s.m_parnamelist = parnamelist
            s.parcount = len(s.paramlist)
            self.currentst.insert(s)
        except ReservedWordException as Rwe:
            Rwe.error(ctx.start.line, ctx.start.column)
        except RedeclarationException as Rde:
            Rde.error(ctx.start.line, ctx.start.column)
        else:
            # Setup new ST for the Function
            cst = SymbolTable(self.currentst, "FunctionDeclST_{}".format(f_name))  # Create a new ST, with
            self.currentst.children.append(cst)  # Add the new one as a child to the old
            self.currentst = cst  # We are now at the new ST

    # Exit a parse tree produced by TinyCParser#function_declaration.
    def exitFunction_declaration(self, ctx):
        node = self.current
        #remove returnvalue node
        rval = node.children[0]
        node.removeChild(0)
        #set parameters in function declaration and remove parameterlistnode
        node.m_parameters = [p for p in node.getChild(0).getChildren()]
        node.removeChild(0)

        rval =  self.current.m_type
        f_returns = False
        functionblock = self.current.children[0]#contains the block with all statements

        if not isinstance(functionblock, BlockNode):
            f_returns = freturns(functionblock) or f_returns
        else:
            for c in functionblock.children:
                if isinstance(c, ReturnStmtNode):
                    f_returns = True
                elif isinstance(c, IfStmtNode):
                    f_returns = freturns(c) or f_returns
                else:
                    pass
        if not f_returns and rval != "m_void" and node.m_idname != "main":
            print "[C2P Warning] Control may reach end of non-void function: %s, at L[%i], C[%i]." % (self.current.m_idname, ctx.start.line, ctx.start.column)

        self.current = self.current.parent
        self.currentst = self.currentst.parent
        self.current_f_return = "_default_"

    # Enter a parse tree produced by TinyCParser#parameter_list.
    def enterParameter_list(self, ctx):
        node = ParamListNode(self.current)
        self.current.addChild(node)
        self.current = node

    # Exit a parse tree produced by TinyCParser#parameter_list.
    def exitParameter_list(self, ctx):
        self.current = self.current.parent


    # Enter a parse tree produced by TinyCParser#parameter.
    def enterParameter(self, ctx):
        node = ParameterNode(self.current)
        self.current.addChild(node)
        self.current = node


        id_node = IdentifierNode(node, ctx.Identifier().getText())
        node.addChild(id_node)



    # Exit a parse tree produced by TinyCParser#parameter.
    def exitParameter(self, ctx):
        node = self.current

        if(ctx.array_index() != None):
            try:
                s = ArraySymbol()
                s.name = ctx.Identifier().getText()
                s.m_type = self.current.children[1].getType()
                s.m_type += "[]"
                s.attribute = "m_arr"
                s.kind = "m_par"
                if ctx.array_index().expression() != None:
                    s.m_arraysize = int(ctx.array_index().expression().getText())
                else:
                    s.m_arraysize = sys.maxint#TODO: Give a generic solution.
                node.setType(s.m_type)
                node.setName(s.name)
                if not self.forward_busy:
                    self.currentst.insert(s)
            except RedeclarationException as Rde:
                Rde.error(ctx.start.line, ctx.start.column)
            except ReservedWordException as Rwe:
                Rwe.error(ctx.start.line, ctx.start.column)
            else:
                self.current = self.current.parent

        else:
            try:
                s = Symbol()
                s.name = ctx.Identifier().getText()
                s.m_type = self.current.children[1].getType()
                if (self.current.children[1].m_ptr):
                    s.m_type += "*"
                    s.attribute = "m_ptr"
                else:
                    s.attribute = "m_none"
                s.kind = "m_par"
                node.setType(s.m_type)
                node.setName(s.name)
                if not self.forward_busy:
                    self.currentst.insert(s)
            except RedeclarationException as Rde:
                Rde.error(ctx.start.line, ctx.start.column)
            except ReservedWordException as Rwe:
                Rwe.error(ctx.start.line, ctx.start.column)
            else:
                self.current = self.current.parent

    # Enter a parse tree produced by TinyCParser#block.
    def enterBlock(self, ctx):
        node = BlockNode(self.current)
        self.current.addChild(node)
        self.current = node

    # Exit a parse tree produced by TinyCParser#block.
    def exitBlock(self, ctx):
        self.current = self.current.parent

    # Enter a parse tree produced by TinyCParser#statement.
    def enterStatement(self, ctx):
        if ctx.Continue() != None:
            node = ContinueNode(self.current)
            self.current.addChild(node)
            self.current = node
        elif ctx.Break() != None:
            node = BreakNode(self.current)
            self.current.addChild(node)
            self.current = node

    # Exit a parse tree produced by TinyCParser#statement.
    def exitStatement(self, ctx):
        if isinstance(self.current, (ContinueNode, BreakNode)):
            self.current = self.current.parent

    # Enter a parse tree produced by TinyCParser#if_statement.
    def enterIf_statement(self, ctx):
        #Create the ST for the IF statement Block
        ifst = SymbolTable(self.currentst, "IfST")
        self.currentst.children.append(ifst)
        self.currentst = ifst

        node = IfStmtNode(self.current)
        self.current.addChild(node)
        self.current = node

    # Exit a parse tree produced by TinyCParser#if_statement.
    def exitIf_statement(self, ctx):
        try:
            bool_expr = self.current.children[0]
            if(bool_expr.getType() != "m_bool"):
                raise TinyCTypeException(bool_expr.getType(), "m_bool")
        except TinyCTypeException as Tte:
            Tte.error(ctx.start.line, ctx.start.column)
        else:
            pass
        self.current = self.current.parent
        self.currentst = self.currentst.parent

    # Enter a parse tree produced by TinyCParser#else_statement.
    def enterElse_statement(self, ctx):
        elsest = SymbolTable(self.currentst, "ElseST")
        self.currentst.children.append(elsest)
        self.currentst = elsest

        node = ElseStmtNode(self.current)
        self.current.addChild(node)
        self.current = node

    # Exit a parse tree produced by TinyCParser#else_statement.
    def exitElse_statement(self, ctx):
        self.current = self.current.parent
        self.currentst = self.currentst.parent


    # Enter a parse tree produced by TinyCParser#while_statement.
    def enterWhile_statement(self, ctx):
        whilest = SymbolTable(self.currentst, "WhileST")
        self.currentst.children.append(whilest)
        self.currentst = whilest

        node = WhileStmtNode(self.current)
        self.current.addChild(node)
        self.current = node

    # Exit a parse tree produced by TinyCParser#while_statement.
    def exitWhile_statement(self, ctx):
        try:
            bool_expr = self.current.children[0]
            if(bool_expr.getType() != "m_bool"):
                raise TinyCTypeException(bool_expr.getType(), "m_bool")
        except TinyCTypeException as Tte:
            Tte.error(ctx.start.line, ctx.start.column)
        else:
            pass
        self.current = self.current.parent
        self.currentst = self.currentst.parent


    # Enter a parse tree produced by TinyCParser#for_statement.
    def enterFor_statement(self, ctx):
        forst = SymbolTable(self.currentst, "ForST")
        self.currentst.children.append(forst)
        self.currentst = forst

        node = ForStmtNode(self.current)

        if ctx.e1 != None:
            node.m_e1 = True
        if ctx.e2 != None:
            node.m_e2 = True
        if ctx.e3 != None:
            node.m_e3 = True

        self.current.children.append(node)
        self.current = node

    # Exit a parse tree produced by TinyCParser#for_statement.
    def exitFor_statement(self, ctx):
        #Self.current is now a ForStmtNode, which has 3 children:
        #self.current.children[0] - Initiator
        #self.current.children[1] - Loop Condition
        #self.current.children[2] - Post Loop Expression
        # 0 and 2 are allowed to be empty
        try:
            if((self.current.m_e1 == True) and (self.current.m_e2 == True)):
                bool_expr = self.current.children[1]
            else:
                bool_expr = self.current.children[0]
            if(bool_expr.getType() != "m_bool"):
                raise TinyCTypeException(bool_expr.getType(), "m_bool")
        except TinyCTypeException as Tte:
            Tte.error(ctx.start.line, ctx.start.column)
        else:
            pass
        self.current = self.current.parent
        self.currentst = self.currentst.parent


    # Enter a parse tree produced by TinyCParser#return_statement.
    def enterReturn_statement(self, ctx):
        node = ReturnStmtNode(self.current)
        self.current.addChild(node)
        self.current = node
        self.current.m_type = self.current_f_return

    # Exit a parse tree produced by TinyCParser#return_statement.
    def exitReturn_statement(self, ctx):
        try:
            if len(self.current.children) == 0 and self.current.m_type != "m_void":
                raise ReturningVoidNonVoidFunctionException()
            elif len(self.current.children) != 0:
                child = self.current.children[0]
                if (child.m_type != self.current_f_return):
                    raise TinyCTypeException(self.current_f_return, child.m_type)
        except TinyCTypeException as Tte:
            Tte.error(ctx.start.line, ctx.start.column)
        except ReturningVoidNonVoidFunctionException as Rvnvfe:
            Rvnvfe.error(ctx.start.line, ctx.start.column)
        self.current = self.current.parent


    # Enter a parse tree produced by TinyCParser#expression.
    def enterExpression(self, ctx):
        #Split here between assignment expression and normal expression

        #TODO: global variable can not be an assignment: must be declaration!!
        if(ctx.getChildCount() >= 2):
            t = ctx.getChild(1).getSymbol().type
            if ((t == TinyCLexer.Assign) or (t == TinyCLexer.PlusAssign) or (t == TinyCLexer.MinusAssign) or (t == TinyCLexer.StarAssign) or (t == TinyCLexer.DivAssign) or (t == TinyCLexer.ModAssign)):
            #Assignment guaranteed
                node = AssignmentNode(self.current)
                if "+=" in ctx.getText():
                    node.m_assigntype = "+="
                    node.m_label = node.m_assigntype
                elif "-=" in ctx.getText():
                    node.m_assigntype = "-="
                    node.m_label = node.m_assigntype
                elif "*=" in ctx.getText():
                    node.m_assigntype = "*="
                    node.m_label = node.m_assigntype
                elif "/=" in ctx.getText():
                    node.m_assigntype = "/="
                    node.m_label = node.m_assigntype
                elif "%=" in ctx.getText():
                    node.m_assigntype = "%="
                    node.m_label = node.m_assigntype
                else:
                    node.m_assigntype = "="
                self.current.addChild(node)
                self.current = node
                node.lhs = ctx.getChild(0).getText()

    # Exit a parse tree produced by TinyCParser#expression.
    def exitExpression(self, ctx):
        if(ctx.getChildCount() >= 2):
            t = ctx.getChild(1).getSymbol().type
            if ((t == TinyCLexer.Assign) or (t == TinyCLexer.PlusAssign) or (t == TinyCLexer.MinusAssign) or (t == TinyCLexer.StarAssign) or (t == TinyCLexer.DivAssign) or (t == TinyCLexer.ModAssign)):
                try:
                    #self.current is an AssignmentNode
                    tnode = self.current.children[0]
                    if (not isinstance(tnode, IdentifierNode) and not isinstance(tnode, ArrayNode)):
                        raise ExpressionNotAssignableException();
                    if '[]' in tnode.m_type:
                        raise ArrayTypeNotAssignableException(tnode.m_idname, tnode.m_type)
                    #DO TYPE CHECKING FOR i = u
                    if(self.current.children[0].getType() != self.current.children[1].getType()):
                        raise TinyCTypeException(self.current.children[0].getType(),self.current.children[1].getType())
                    if(self.current.children[0].getType() == self.current.children[1].getType() == "m_char"):
                        if(t != TinyCLexer.Assign):
                            raise IllegalOperationException("Compound Assignment", "Numeric_Types")
                except TinyCTypeException as TCTE:
                    TCTE.error(ctx.start.line, ctx.start.column)
                except ExpressionNotAssignableException as Enae:
                    Enae.error(ctx.start.line, ctx.start.column)
                except ArrayTypeNotAssignableException as Atnae:
                    Atnae.error(ctx.start.line, ctx.start.column)
                except IllegalOperationException as Ioe:
                    Ioe.error(ctx.start.line, ctx.start.column)
                else:
                    self.current = self.current.parent

    # Enter a parse tree produced by TinyCParser#or_expr.
    def enterOr_expr(self, ctx):
        if (ctx.getChildCount() != 1):
            node = OrExprNode(self.current)
            self.current.addChild(node)
            self.current = node

    # Exit a parse tree produced by TinyCParser#or_expr.
    def exitOr_expr(self, ctx):
        node = self.current
        if (isinstance(node, OrExprNode) and len(node.getChildren()) == 2):
            try:
                for c in node.children:
                    #For all children
                    #Check that all types are the same#Check that all types are the same
                    if not (c.getType() == "m_bool"):
                        raise TinyCTypeException("m_bool", c.getType())
            except TinyCTypeException as Tte:
                Tte.error(ctx.start.line, ctx.start.column)
            else:
                node.m_type = "m_bool"
            self.current = self.current.parent

    # Enter a parse tree produced by TinyCParser#and_expr.
    def enterAnd_expr(self, ctx):
        if (ctx.getChildCount() != 1):
            node = AndExprNode(self.current)
            self.current.addChild(node)
            self.current = node

    # Exit a parse tree produced by TinyCParser#and_expr.
    def exitAnd_expr(self, ctx):
        node = self.current
        if (isinstance(node, AndExprNode) and len(node.getChildren()) == 2):
            try:
                for c in node.children:
                    #For all children
                    #Check that all types are the same
                    if not (c.getType() == "m_bool"):
                        raise TinyCTypeException("m_bool", c.getType())
            except TinyCTypeException as Tte:
                Tte.error(ctx.start.line, ctx.start.column)
            else:
                node.m_type = "m_bool"
            self.current = self.current.parent

    # Enter a parse tree produced by TinyCParser#rel_expr.
    def enterRel_expr(self, ctx):
        if (ctx.getChildCount() != 1):
            node = RelExprNode(self.current)
            self.current.addChild(node)
            self.current = node
            node.m_rel_operation = ctx.getChild(1).getText()

    # Exit a parse tree produced by TinyCParser#rel_expr.
    def exitRel_expr(self, ctx):
        node = self.current
        if (isinstance(node, RelExprNode) and len(node.getChildren()) == 2):
            t = node.children[0].getType()
            node.m_rel_operands = t
            try:
                for c in node.children:
                    #For all children
                    #Check that all types are the same
                    if not (t == c.getType()):
                        raise TinyCTypeException(t, c.getType())
            except TinyCTypeException as Tte:
                Tte.error(ctx.start.line, ctx.start.column)
            else:
                node.m_type = "m_bool"
            self.current = self.current.parent

    # Enter a parse tree produced by TinyCParser#add_expr.
    def enterAdd_expr(self, ctx):
        issum = False
        if (ctx.getChildCount() != 1):
            k = ctx.getChild(1)
            if k.getText() == '+':
                issum = True
            elif k.getText() == '-':
                issum = False
            if issum:
                node = AddExprNode(self.current)
                self.current.addChild(node)
                self.current = node
            else:
                node = SubtractExprNode(self.current)
                self.current.addChild(node)
                self.current = node

    # Exit a parse tree produced by TinyCParser#add_expr.
    def exitAdd_expr(self, ctx):
        node = self.current
        if ((isinstance(node, AddExprNode) or isinstance(node, SubtractExprNode)) and len(node.getChildren()) == 2):
            t = node.children[0].getType()
            try:
                for c in node.children:
                    #For all children
                    #Check that all types are the same#
                    if not (t == c.getType()):
                        raise TinyCTypeException(t, c.getType())
                    if not (c.getType() in ['m_float', 'm_int'] ):
                        raise IllegalOperationException("Numerical operator {}".format(node.m_label), "Numerical types")
            except TinyCTypeException as Tte:
                Tte.error(ctx.start.line, ctx.start.column)
            except IllegalOperationException as Ioe:
                Ioe.error(ctx.start.line, ctx.start.column)
            else:
                node.m_type = t
            self.current = self.current.parent

    # Enter a parse tree produced by TinyCParser#mult_expr.
    def enterMult_expr(self, ctx):
        if (ctx.getChildCount() != 1):
            k = ctx.getChild(1)
            sign = k.getText();

            if sign == '*':
                node = MultExprNode(self.current)
                self.current.addChild(node)
                self.current = node
            elif sign == '/':
                node = DivExprNode(self.current)
                self.current.addChild(node)
                self.current = node
            elif sign == '%':
                node = ModExprNode(self.current)
                self.current.addChild(node)
                self.current = node
            else:
                print "Unexpected sign in Multiplication Expression: ", sign

    # Exit a parse tree produced by TinyCParser#mult_expr.
    def exitMult_expr(self, ctx):
        node = self.current
        if ((isinstance(node, (MultExprNode, DivExprNode, ModExprNode))) and len(node.getChildren()) == 2):
            t = node.children[0].getType()
            try:
                for c in node.children:
                    #For all children
                    #Check that all types are the same
                    if not (t == c.getType()):
                        raise TinyCTypeException(t, c.getType())
                    if not (c.getType() in ['m_float', 'm_int'] ):
                        raise IllegalOperationException("Numerical operator {}".format(node.m_label), "Numerical types")
                    if (isinstance(node, ModExprNode) and c.getType() != "m_int"):
                        raise IllegalOperationException("Modulo operator %", "Integers")
            except TinyCTypeException as Tte:
                Tte.error(ctx.start.line, ctx.start.column)
            except IllegalOperationException as Ioe:
                Ioe.error(ctx.start.line, ctx.start.column)
            else:
                node.m_type = t
            self.current = self.current.parent


    # Enter a parse tree produced by TinyCParser#unary_expr.
    def enterUnary_expr(self, ctx):
        if (ctx.getChildCount() != 1):
            optype = "EMPTY"
            if(ctx.Minus() != None):
                optype = "-"
            elif (ctx.Not() != None):
                optype = "!"
            elif (ctx.prepp != None):
                optype = "pre++"
            elif (ctx.premm != None):
                optype = "pre--"
            elif (ctx.postpp != None):
                optype = "post++"
            elif (ctx.postmm != None):
                optype = "post--"
            node = UnaryExprNode(self.current, optype)
            self.current.addChild(node)
            self.current = node

    # # Exit a parse tree produced by TinyCParser#unary_expr.
    # def exitUnary_expr(self, ctx):
    #     if (isinstance(self.current, UnaryExprNode)):
    #         self.current.m_type = self.current.getChild(0).getType()
    #         self.current = self.current.parent

    def exitUnary_expr(self, ctx):
        try:
            if (isinstance(self.current, UnaryExprNode) and len(self.current.getChildren()) == 1):
                self.current.m_type = self.current.getChild(0).getType()
                if(self.current.m_optype in ["++", "--"] and self.current.m_type != "m_int"):
                    raise IllegalOperationException(self.current.m_optype, "Integer")
                if(self.current.m_optype == '-' and (self.current.m_type not in ["m_float", "m_int"])):
                    raise IllegalOperationException(self.current.m_optype, "Numericals")
                if (self.current.m_optype == '!' and (self.current.m_type != "m_bool")):
                    raise IllegalOperationException(self.current.m_optype, "Boolean")
                self.current = self.current.parent
        except IllegalOperationException as IOE:
            IOE.error(ctx.start.line, ctx.start.column)

    # Enter a parse tree produced by TinyCParser#pf_expr.
    def enterPf_expr(self, ctx):
        if (ctx.getChildCount() != 1):
            node = FunctionCallNode(self.current)
            self.current.addChild(node)
            self.current = node
            node.m_fcall_identifier = ctx.Identifier().getText()

    # Exit a parse tree produced by TinyCParser#pf_expr.
    def exitPf_expr(self, ctx):
        # self.current = DeclarationNode | AssignmentNode | FunctionCallNode

        # TOOD: This can be assignment node: rewrite this shit
        # sum(4,4) = 7 is possible, but shouldn't be since expression is not assignable
        node = self.current
        if (isinstance(node, FunctionCallNode)):
            t = node.m_fcall_identifier
            try:
                self.currentst.lookup(t)
            except VariableNotDeclaredException as Vnde:
                Vnde.error(ctx.start.line, ctx.start.column)
            else:
                for c in self.current.children:
                    self.current.m_parameters.append(c)
                self.current.getLabel()
                try:
                    if(len(self.current.m_parameters) > 0):
                        if((t != "scanf") and (t != "printf")):
                            arlnode = self.current.m_parameters[0]
                            fsymbol = self.currentst.lookupsymbol(t)
                            if(fsymbol.parcount != len(arlnode.m_arguments)):
                                raise WrongParameterCountException(fsymbol.parcount, len(arlnode.m_arguments), t)
                            i = 0
                            for p1 in arlnode.m_arguments:
                                if(p1.getType() != fsymbol.paramlist[i]):
                                    raise TinyCTypeException(p1.getType(), fsymbol.paramlist[i])
                                else:
                                    i += 1
                        else:
                            arlnode = self.current.m_parameters[0]#Contains the arg list for printf
                            pfstring = arlnode.m_arguments[0] #Contains the stringlitnode
                            outpstr = pfstring.getValue()
                            delimiters = arlnode.m_arguments[1:]#contains all the delimiters
                            if(not isinstance(pfstring, StringLitNode)):
                                raise TinyCTypeException(type(pfstring), StringLitNode)
                            #Now we have to parse the string, check the delimiters, and check if they match.
                            params = re.findall('%\d*[^\s]',outpstr)
                            paramcount = len(params)
                            if (t == "printf"):
                                if len(delimiters) != paramcount:
                                    raise WrongParameterCountException(paramcount, len(delimiters), "printf")
                                for i, param in enumerate(params):
                                    if ('s' in param) and (not isinstance(delimiters[i], StringLitNode)) and not (delimiters[i].m_type == "m_char[]"):
                                        raise TinyCTypeException(delimiters[i].getType(), "m_char[]")
                                    if ('c' in param) and (delimiters[i].getType() != "m_char"):
                                        raise TinyCTypeException(delimiters[i].getType(), "m_char")
                                    if ('f' in param) and (delimiters[i].getType() != "m_float"):
                                        raise TinyCTypeException(delimiters[i].getType(), "m_float")
                                    if ('d' in param or 'i' in param) and (delimiters[i].getType() != "m_int"):
                                        raise TinyCTypeException(delimiters[i].getType(), "m_int")
                                    if (param[-1] not in 'scfdi'):
                                        raise IllegalFormatException(param)
                            elif (t == "scanf"):
                                if len(delimiters) != paramcount:
                                    raise WrongParameterCountException(paramcount, len(delimiters), "scanf")
                                for i, param in enumerate(params):
                                    if ('s' in param) and (delimiters[i].m_type != "m_char[]"):
                                        raise TinyCTypeException(delimiters[i].getType(), "m_char[]")
                                    if ('c' in param) and (delimiters[i].getType() != "m_char*"):
                                        raise TinyCTypeException(delimiters[i].getType(), "m_char*")
                                    if ('f' in param) and (delimiters[i].getType() != "m_float*"):
                                        raise TinyCTypeException(delimiters[i].getType(), "m_float*")
                                    if ('d' in param or 'i' in param) and (delimiters[i].getType() != "m_int*"):
                                        raise TinyCTypeException(delimiters[i].getType(), "m_int*") 
                                    if (param[-1] not in 'scfdi'):
                                        raise IllegalFormatException(param)
                    else:
                        arlnode = None
                except TinyCTypeException as TCTE:
                    TCTE.error(ctx.start.line, ctx.start.column)
                except InvalidStringFormatException as ISFE:
                    ISFE.error(ctx.start.line, ctx.start.column)
                except WrongParameterCountException as WPCE:
                    WPCE.error(ctx.start.line, ctx.start.column)
                except IllegalFormatException as Ife:
                    Ife.error(ctx.start.line, ctx.start.column)
                if (arlnode != None):
                    self.current.m_parameters = [c for c in arlnode.m_arguments]
                else:
                    self.current.m_parameters = []
                self.current.setType(self.currentst.lookuptype(t))
                self.current.m_fcall_identifier = t
                self.current.children = []
                self.current = self.current.parent

    # Enter a parse tree produced by TinyCParser#arg_list.
    def enterArg_list(self, ctx):
        node = ArgListNode(self.current)
        self.current.addChild(node)
        self.current = node

    # Exit a parse tree produced by TinyCParser#arg_list.
    def exitArg_list(self, ctx):
        node = self.current
        node.m_arguments = [c for c in node.getChildren()]
        node.children = []
        self.current = self.current.parent

    # Enter a parse tree produced by TinyCParser#pr_expr.
    def enterPr_expr(self, ctx):
        node = self.current
        val = ctx.getText()
        if (ctx.Identifier() != None):
            idname = ctx.Identifier().getText()
            try:
                self.currentst.lookup(idname)
            except VariableNotDeclaredException as Vnde:
                Vnde.error(ctx.start.line, ctx.start.column)
            else:
                pass
            try:
                if (ctx.array_index() != None):
                    t = self.currentst.lookuptype(idname)
                    if not "[]" in t:
                        raise IllegalIndexException(idname)
                    arr_node = ArrayNode(node, idname, ctx.getText())
                    arr_node.m_type = self.currentst.lookuptype(idname)[:-2]
                    node.addChild(arr_node)
                    self.current = arr_node
                else:
                    id_node = IdentifierNode(node, val)
                    id_node.m_idname = idname
                    id_node.m_type = self.currentst.lookuptype(idname)
                    if (ctx.Star() != None):
                        id_node.m_ptr = True
                    if (ctx.Ref() != None):
                        id_node.m_ref = True
                    node.addChild(id_node)
                    self.current = id_node
            except VariableNotDeclaredException as Vnde:
                Vnde.error(ctx.start.line, ctx.start.column)
            except IllegalIndexException as Iie:
                Iie.error(ctx.start.line, ctx.start.column)
        elif (ctx.Number() != None):
            if '.' in val:
                fl_node = FloatLitNode(node, val)
                node.addChild(fl_node)
                self.current = fl_node
            else:
                int_node = IntegerLitNode(node, val)
                node.addChild(int_node)
                self.current = int_node
        elif (ctx.Character() != None):
            char_node = CharLitNode(node, val)
            node.addChild(char_node)
            self.current = char_node
        elif (ctx.String() != None):
            str_node = StringLitNode(node, val)
            node.addChild(str_node)
            self.current = str_node
        elif (ctx.MTrue() != None or ctx.MFalse() != None):
            bool_node = BoolLitNode(node, val)
            node.addChild(bool_node)
            self.current = bool_node

    # Exit a parse tree produced by TinyCParser#pr_expr.
    def exitPr_expr(self, ctx):
        if (isinstance(self.current, ArrayNode)):
            try:
                if ctx.Star() != None:
                    raise IllegalDereferenceException(self.current.m_idname)
                if ctx.Ref() != None:
                    raise IllegalReferenceException(self.current.m_idname)
                self.current.m_index = self.current.children[0]
                if (isinstance(self.current.m_index, IntegerLitNode)):
                    arraysize = self.currentst.lookuparraysize(self.current.m_idname)
                    if(self.current.m_index.getValue() < 0 or self.current.m_index.getValue() > arraysize - 1):
                        raise ArrayOutOfBoundsException()
                if (isinstance(self.current.m_index, UnaryExprNode)):#Can only be negative number, which is always out of bounds
                    raise ArrayOutOfBoundsException()
                _type = self.current.getChild(0).getType()
                if (_type != "m_int"):
                    raise TinyCTypeException(_type, "m_int")
            except TinyCTypeException as Tte:
                Tte.error(ctx.start.line, ctx.start.column)
            except ArrayOutOfBoundsException as AOOBE:
                AOOBE.error(ctx.start.line, ctx.start.column)
            except IllegalDereferenceException as Ide:
                Ide.error(ctx.start.line, ctx.start.column)
            except IllegalReferenceException as Ire:
                Ire.error(ctx.start.line, ctx.start.column)
            else:
                self.current.removeChild(0)
                self.current = self.current.parent
        elif (isinstance(self.current, IdentifierNode)):
            tnode = self.current
            try:
                t = self.currentst.lookuptype(self.current.m_idname)
                if ctx.Star() != None:
                    if not "*" in t:
                        raise IllegalDereferenceException(self.current.m_idname)
                if ctx.Ref() != None:
                    if "[]" in t or '*' in t:
                        raise IllegalReferenceException(self.current.m_idname)
                if (tnode.m_ptr and '*' not in tnode.m_type):
                    raise IndirectNotOnPointerException(self.current.m_idname, tnode.m_type)
                if (tnode.m_ref and '*' in tnode.m_type):
                    raise DoubleReferenceNotSupportedException(self.current.m_idname, tnode.m_type)
            except IndirectNotOnPointerException as Inope:
                Inope.error(ctx.start.line, ctx.start.column)
            except DoubleReferenceNotSupportedException as Drnse:
                Drnse.error(ctx.start.line, ctx.start.column)
            except IllegalDereferenceException as Ide:
                Ide.error(ctx.start.line, ctx.start.column)
            except IllegalReferenceException as Ire:
                Ire.error(ctx.start.line, ctx.start.column)
            else:
                if (tnode.m_ptr): tnode.m_type = tnode.m_type[:-1]
                if (tnode.m_ref): tnode.m_type += '*'
                self.current = self.current.parent
        elif (isinstance(self.current, LiteralNode)):
            self.current = self.current.parent

    def __init__(self):
        self.root = None#We have no AST yet so the root is empty, check CU
        self.current = None#Maintains link to the current AST Node

        self.globalst = SymbolTable(None, "CompilationUnitST")
        self.currentst = self.globalst

        self.current_f_return = "_default_"
        self.decl_busy = False
        self.forward_busy = False
    def getAST(self):
        return self.root
