# Generated from ./Grammar/TinyC.g4 by ANTLR 4.5.2
from antlr4 import *

# This class defines a complete generic visitor for a parse tree produced by TinyCParser.

class TinyCVisitor(ParseTreeVisitor):

    # Visit a parse tree produced by TinyCParser#parse.
    def visitParse(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TinyCParser#compilation_unit.
    def visitCompilation_unit(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TinyCParser#forward_declaration.
    def visitForward_declaration(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TinyCParser#m_include.
    def visitM_include(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TinyCParser#declaration.
    def visitDeclaration(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TinyCParser#simpleDecl.
    def visitSimpleDecl(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TinyCParser#m_type.
    def visitM_type(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TinyCParser#varType.
    def visitVarType(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TinyCParser#variable_decl.
    def visitVariable_decl(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TinyCParser#variable_id.
    def visitVariable_id(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TinyCParser#array_index.
    def visitArray_index(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TinyCParser#function_declaration.
    def visitFunction_declaration(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TinyCParser#parameter_list.
    def visitParameter_list(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TinyCParser#parameter.
    def visitParameter(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TinyCParser#block.
    def visitBlock(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TinyCParser#statement.
    def visitStatement(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TinyCParser#if_statement.
    def visitIf_statement(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TinyCParser#else_statement.
    def visitElse_statement(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TinyCParser#while_statement.
    def visitWhile_statement(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TinyCParser#for_statement.
    def visitFor_statement(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TinyCParser#return_statement.
    def visitReturn_statement(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TinyCParser#expression.
    def visitExpression(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TinyCParser#or_expr.
    def visitOr_expr(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TinyCParser#and_expr.
    def visitAnd_expr(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TinyCParser#rel_expr.
    def visitRel_expr(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TinyCParser#add_expr.
    def visitAdd_expr(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TinyCParser#mult_expr.
    def visitMult_expr(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TinyCParser#unary_expr.
    def visitUnary_expr(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TinyCParser#pf_expr.
    def visitPf_expr(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TinyCParser#arg_list.
    def visitArg_list(self, ctx):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by TinyCParser#pr_expr.
    def visitPr_expr(self, ctx):
        return self.visitChildren(ctx)


