from AST.ASTNode import ASTNode
from Util.AddressSpace import AddressSpace


class IdentifierNode(ASTNode):

    def __init__(self, parent, _idname = "_NONE_"):
        super(IdentifierNode, self).__init__(parent, "identifier")
        self.m_idname = _idname
        self.m_label = _idname
        self.m_type = "_default_"
        self.m_ptr = False
        self.m_ref = False

    def generateLabel(self):
        return self.m_label

    def getLabel(self):
        if self.m_ptr == True:
            return "* " + self.m_label
        else:
            return self.m_label

    def generateCode(self, fileStream, CodeRHS = True):
        if(CodeRHS):
            self.generateCodeR(fileStream)
        else:
            self.generateCodeL(fileStream)

    def generateCodeL(self, fileStream, decl = False):
        
        if decl:
            l, rel = AddressSpace.addVariable(self.m_idname)
        else:
            l, rel = AddressSpace.getAddress(self.m_idname)
        
        if self.m_ptr:
            ptype, pnull = 'a', 0
        else:
            ptype, pnull = AddressSpace.pType(self.m_type)
        
        # declaration of a variable
        # LHS pointer type does not change the way we initialize    
        if decl:
            fileStream.write("ldc {} {}\n".format(ptype, pnull))        
        #We know now that we are not in a declaration anymore => assignment LHS
        # if LHS in an assignment, do nothing (unless LHS is dereference)
        elif rel and self.m_ptr:
            # Of the form *Identifier = expression
            fileStream.write("lod a 0 {}\n".format(l))
        elif not rel and self.m_ptr:
            fileStream.write("ldo a {}\n".format(l))
        else:
            #Do nothing if we are not a pointer and not an assignment on LHS
            pass
        
    def generateCodeR(self, fileStream):
        l, rel = AddressSpace.getAddress(self.m_idname)
        ptype, _ = AddressSpace.pType(self.m_type)
        
        if rel:
            if self.m_ptr:
                fileStream.write("lod a 0 {}\n".format(l))
                fileStream.write("ind {}\n".format(ptype))
            elif self.m_ref:
                fileStream.write("lda 0 {}\n".format(l))
            else:
                fileStream.write("lod {} 0 {}\n".format(ptype, l))
        # rel == False
        elif self.m_ptr:
            fileStream.write("ldo a {}\n".format(l))
            fileStream.write("ind {}\n".format(ptype))
        elif self.m_ref:
            fileStream.write("ldc a {}\n".format(l))
            pass #TODO:
        else:
            fileStream.write("ldo %c %i\n" %(ptype, l))
            
