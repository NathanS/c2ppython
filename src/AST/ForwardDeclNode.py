from AST.ASTNode import ASTNode
from AST.CompilationUnitNode import CompilationUnitNode
from Util.AddressSpace import AddressSpace

class ForwardDeclNode(CompilationUnitNode):
    def __init__(self,parent):
        super(CompilationUnitNode, self).__init__(parent,"forward_decl")
        self.m_label = "Empty"
        self.m_idname = "_default_"
        self.m_return = "m_void"
        self.m_paramcount = 0
        self.m_parameters = []

    def generateCode(self, fileStream):
        #TODO: Should this generate code?
        pass
