from AST.ASTNode import ASTNode
from Util.LabelGenerator import LabelGenerator
from Util.AddressSpace import AddressSpace


class StatementNode(ASTNode):

    def __init__(self,parent, _name = "statement"):
        super(StatementNode, self).__init__(parent, _name)
        self.subtype = None

    def generateLabel(self):
        return self.m_label

    def generateCode(self, fileStream):
        pass
    
class ContinueNode(StatementNode):
    
    def __init__(self, parent):
        super(ContinueNode, self).__init__(parent, "continue")
        
    def generateLabel(self):
        return "continue"
    
    def generateCode(self, fileStream):
        label = AddressSpace.getTopCont()
        fileStream.write("ujp {}\n".format(label))

class BreakNode(StatementNode):
    
    def __init__(self, parent, _name = "break"):
        super(BreakNode, self).__init__(parent, "break")
        
    def generateLabel(self):
        return "break"
    
    def generateCode(self, fileStream):
        label = AddressSpace.getTopBreak()
        fileStream.write("ujp {}\n".format(label))


class IfStmtNode(StatementNode):

    def __init__(self, parent):
        super(IfStmtNode, self).__init__(parent,"if_statement")
        self.m_label = "IF"

    def generateCode(self, fileStream):
        #if e then st1 else st2
        #code e
        #fjp l1
        #code st1
        #ujp l2
        #code st2
        #First child is the Boolean Expression
        self.children[0].generateCode(fileStream)
        #Need to generate both labels right now to avoid nested loops
        iflab = LabelGenerator.generateLabel("if")
        elselab = LabelGenerator.generateLabel("else")
        fileStream.write("fjp %s\n" % iflab)
        self.children[1].generateCode(fileStream)
        #Second Child is the block with all statements
        #self.children[1].generateCode(fileStream)
        if(len(self.children) > 2):
            #We also have an ELSE!
            fileStream.write("ujp %s\n" % elselab)
            fileStream.write(iflab + ":\n")
            self.children[2].generateCode(fileStream)
            fileStream.write(elselab + ":\n")
        else:
            fileStream.write(iflab + ":\n")


class ElseStmtNode(StatementNode):

    def __init__(self, parent):
        super(ElseStmtNode,self).__init__(parent, "else_statement")
        self.m_label = "ELSE"

    def generateCode(self, fileStream):
        for c in self.children:
            c.generateCode(fileStream)

class WhileStmtNode(StatementNode):

    def __init__(self, parent):
        super(WhileStmtNode, self).__init__(parent, "while_statement")
        self.m_label = "WHILE"

    def generateCode(self, fileStream):
        whilelab1 = LabelGenerator.generateLabel("while")

        whilelab2 = LabelGenerator.generateLabel("while")

        AddressSpace.pushStack(whilelab1,whilelab2)
        fileStream.write(whilelab1 + ":\n")
        #Generate code for the while condition
        self.children[0].generateCode(fileStream)

        fileStream.write("fjp %s\n" % whilelab2)
        #generate code for the while block
        self.children[1].generateCode(fileStream)
        #Write the labels
        fileStream.write("ujp %s\n" % whilelab1)
        fileStream.write(whilelab2 + ":\n")
        AddressSpace.popStack()



class ReturnStmtNode(StatementNode):

    def __init__(self,parent):
        super(ReturnStmtNode, self).__init__(parent,"return_statement")
        self.m_type = "_default_"
        self.m_label = "RETURN"

    def generateCode(self, fileStream):
        for c in self.children:
            c.generateCode(fileStream)
        if self.m_type == "m_void":
            fileStream.write("retp\n")
        else:
            ptype, _ = AddressSpace.pType(self.m_type)
            fileStream.write("str {} {} 0\n".format(ptype, 0))
            fileStream.write("retf\n")

class ForStmtNode(StatementNode):

    def __init__(self,parent):
        super(ForStmtNode, self).__init__(parent,"for_statement")
        self.m_type = "_default_"
        self.m_label = "For"
        self.m_e1 = False
        self.m_e2 = False
        self.m_e3 = False

    def generateCode(self, fileStream):
        forlab1 = LabelGenerator.generateLabel("for")
        forlab2 = LabelGenerator.generateLabel("for")
        forlab3 = LabelGenerator.generateLabel("for")
        AddressSpace.pushStack(forlab3,forlab2)
        #If we have an initiator, we generate code for it once.
        if(self.m_e1):
            self.children[0].generateCode(fileStream)
    
        #Generate the first Label
        fileStream.write("{}:\n".format(forlab1))
    
        #Generate Code for the condition
        if(self.m_e1):
            self.children[1].generateCode(fileStream)
        else:
            self.children[0].generateCode(fileStream)
    
        fileStream.write("fjp %s\n" % forlab2)
    
        forexprcount = 1#because e2 is mandatory
        if(self.m_e1):
            forexprcount += 1
        if(self.m_e3):
            forexprcount += 1
        #Generate code for either the single statement, or the Block that will call its children
        self.children[forexprcount].generateCode(fileStream)
        fileStream.write("{}:\n".format(forlab3))
        #Generate code for the increment, check the correct location
        if(self.m_e3):
            if(self.m_e1):
                self.children[2].generateCode(fileStream)
            else:
                self.children[1].generateCode(fileStream)
        else:
            pass
        fileStream.write("ujp {}\n".format(forlab1))
        fileStream.write("{}:\n".format(forlab2))
