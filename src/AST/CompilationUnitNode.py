from AST.ASTNode import ASTNode
from AST.FunctionDeclarationNode import FunctionDeclarationNode
from Exceptions.CustomExceptions import IllegalCompilationUnitException


class CompilationUnitNode(ASTNode):


    def __init__(self, parent, _name = "compilation_unit"):
        super(CompilationUnitNode, self).__init__(parent, _name)
        self.include = None

    def generateCode(self, fileStream):
        # first initialize all globals
        for c in filter(lambda x : not isinstance(x,FunctionDeclarationNode), self.children):
            c.generateCode(fileStream)
        
        # jump to main function
        fileStream.write("ujp START\n")
        
        # codegen for all functions
        for c in filter(lambda x : isinstance(x,FunctionDeclarationNode), self.children):
            c.generateCode(fileStream)
            
        fileStream.write("START:\n")     
        fileStream.write("mst 0\n")   
        fileStream.write("cup 0 fun_main\n")
        
        #By this time compilation has finished!
        fileStream.write("hlt\n")
        fileStream.close()

class IncludeNode(CompilationUnitNode):

    def __init__(self,parent,_text):
        super(IncludeNode, self).__init__(parent,"include")
        self.m_label = _text

    def generateCode(self, fileStream):
        #The include node doesn't have to do anything for code generation.
        #It also never has children.
        pass
