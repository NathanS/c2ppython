from AST.ASTNode import ASTNode
from Util.AddressSpace import AddressSpace

class TypeNode(ASTNode):

    def __init__(self, parent, _name = "typenode"):
        super(TypeNode, self).__init__(parent, _name)
        self.m_ptr = False

    def generateCode(self, fileStream):
        pass

class ArrayNode(TypeNode):
    def __init__(self, parent, _idname = "_NONE_", _text = "_NONE_"):
        super(ArrayNode, self).__init__(parent, "array")
        self.m_idname = _idname
        self.m_index = None # type between brackets
        self.m_size = -1
        self.m_label = _text
        self.m_type = "_default_"

    def generateLabel(self):
        return self.m_label

    def generateCode(self, fileStream, CodeRHS = True):
        #By default, we will generate the code for the RHS, as that is where it is most often found.
        if(CodeRHS):
            self.generateCodeR(fileStream)
        else:
            self.generateCodeL(fileStream)

    #The only time an ArrayNode appears, is of the form a[x], on either side of the equation.
    #This means that this simplifies the code schema a lot
    def generateCodeL(self, fileStream, decl = False):
        #Generate codeL for Array. Code for the LHS of:
        # (T)? a[?] = ...
        ptype, pnull = AddressSpace.pType(self.m_type)

        if(decl):
            #Initialize at default values
            l, rel = AddressSpace.addVarArray(self.m_idname, self.m_size)
            fileStream.write("ldc a {}\n".format(l + 1))
            for _ in range(self.m_size):
                fileStream.write("ldc {} {}\n".format(ptype, pnull))
        else:
            l, rel = AddressSpace.getAddress(self.m_idname)
            if rel:
                fileStream.write("lda 0 {}\n".format(l))
            else:
                fileStream.write("ldc a {}\n".format(l))
            self.m_index.generateCode(fileStream)
            fileStream.write("inc i 1\n")
            fileStream.write("ixa 1\n")


    def generateCodeR(self, fileStream):
        #Generate CodeR for the Array when it appears in an expression
        l, rel = AddressSpace.getAddress(self.m_idname)
        ptype, _ = AddressSpace.pType(self.m_type)
        if(rel):
            fileStream.write("lod a 0 {}\n".format(l))
        else:
            fileStream.write("ldc a {}\n".format(l))
            
        self.m_index.generateCode(fileStream)
        fileStream.write("ixa 1\n")
        fileStream.write("ind {}\n".format(ptype))

class VoidNode(TypeNode):

    def __init__(self, parent):
        super(VoidNode, self).__init__(parent, "void")
        self.m_type = "m_void"

    def generateCode(self, fileStream):
        pass


class StringNode(TypeNode):

    def __init__(self, parent):
        super(StringNode, self).__init__(parent,"string")
        self.m_type = "m_string"

    def generateCode(self, fileStream):
        pass


class IntegerNode(TypeNode):

    def __init__(self, parent):
        super(IntegerNode, self).__init__(parent,"integer")
        self.m_type = "m_int"

    def generateCode(self, fileStream):
        pass


class FloatNode(TypeNode):

    def __init__(self, parent):
        super(FloatNode, self).__init__(parent,"float")
        self.m_type = "m_float"

    def generateCode(self, fileStream):
        pass


class CharNode(TypeNode):

    def __init__(self, parent):
        super(CharNode, self).__init__(parent, "character")
        self.m_type = "m_char"

    def generateCode(self, fileStream):
        pass


class BoolNode(TypeNode):

    def __init__(self, parent):
        super(BoolNode, self).__init__(parent, "boolean")
        self.m_type = "m_bool"

    def generateCode(self, fileStream):
        pass
