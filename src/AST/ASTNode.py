from Util.IDGenerator import IDGenerator


class ASTNode(object):

    def generateCode(self, fileStream):
        pass

    def __init__(self, parent, _name):
        self.parent = parent
        self.children = []
        self.m_type = "_default_"
        self.m_name = _name
        # Reference to location in code of this node
        self.reference = None
        self._id = IDGenerator.getID()
        # Label needed for Graphviz
        self.m_label = None

    def getInfo(self):
        return self.m_type
    
    def setType(self, _type):
        self.m_type = _type

    def getID(self):
        return self._id

    def __str__(self):
        return self.m_name + str(self._id)

    def addChild(self, child):
        self.children.append(child)

    def getType(self):
        return self.m_type
    
    def getName(self):
        return self.m_name
    
    def setName(self, _name):
        self.m_name = _name
    
    def getUniqueName(self):
        return str(self)
    
    def getLabel(self):
        if (self.m_label != None):
            return self.m_label
        else: return self.generateLabel()
    
    def generateLabel(self):
        self.m_label = self.m_name
        return self.m_label
    
    def getChildren(self):
        return self.children
    
    def getChild(self, index):
        return self.children[index] 
    
    def removeChild(self, index):
        self.children.pop(0)
