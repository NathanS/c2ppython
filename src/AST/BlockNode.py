from AST.ASTNode import ASTNode


class BlockNode(ASTNode):

    def __init__(self, parent):
        super(BlockNode, self).__init__(parent, "block")
        self.label = "Block"

    def generateCode(self, fileStream):
        #All children are statements

        for c in self.children:
            c.generateCode(fileStream)
