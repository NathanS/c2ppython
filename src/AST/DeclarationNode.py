from AST.CompilationUnitNode import CompilationUnitNode
from Util.AddressSpace import AddressSpace
from AST.TypeNode import *
from AST.IdentifierNode import *

# Statements from the form T id; (so without assignment)
class DeclarationNode(CompilationUnitNode):

    def __init__(self, parent, _name = "declaration"):
        super(DeclarationNode, self).__init__(parent, _name)
        self.m_vartype = None
        self.m_idname = None
        self.m_idnode = None
        self.m_ptr = False

    def setType(self, _type):
        self.m_vartype = _type

    def setIdentifier(self, _id):
        self.m_idname = _id

    def generateLabel(self):
        self.m_label = self.m_vartype + " " + self.m_idnode.getLabel()
        return self.m_label

    def generateCode(self, fileStream, codeRHS = True):
        self.m_idnode.generateCodeL(fileStream, True)#True for declaration, False for assignment


class ComplexDeclNode(DeclarationNode):

    def __init__(self, parent):
        super(ComplexDeclNode, self).__init__(parent, "compdecl")

    def generateCode(self, fileStream):
        for c in self.children:
            c.generateCode(fileStream)


class SimpleDeclNode(DeclarationNode):

    def __init__(self,parent):
        super(SimpleDeclNode, self).__init__(parent,"simpledecl")

    def generateCode(self, fileStream):
        pass
