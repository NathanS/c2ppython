from TypeNode import TypeNode

class LiteralNode(TypeNode):
    def __init__(self, parent, val):
        super(LiteralNode, self).__init__(parent, "literal")
        self.m_label = val
        self.m_value = val

    def getValue(self):
        return self.m_label



class StringLitNode(LiteralNode):

    def __init__(self, parent,val):
        super(StringLitNode, self).__init__(parent, val)
        self.m_type = "m_string"

    def getValue(self):
        return str(self.m_label)

    def generateLabel(self):
        return self.m_label

    def generateCode(self, fileStream):
        #Put all characters of the string on top of the stack
        for c in range(0,len(self.m_label)-1):
            fileStream.write("ldc c '%s'\n" % self.m_label[len(self.m_label)-c-1])


class IntegerLitNode(LiteralNode):

    def __init__(self, parent,val):
        super(IntegerLitNode, self).__init__(parent, val)
        self.m_type = "m_int"

    def generateLabel(self):
        return self.m_label

    def generateCode(self, fileStream):
        fileStream.write("ldc i " + self.m_label + "\n")

    def getValue(self):
        return int(self.m_label)


class FloatLitNode(LiteralNode):

    def __init__(self, parent,val):
        super(FloatLitNode, self).__init__(parent, val)
        self.m_type = "m_float"

    def getValue(self):
        return float(self.m_value)

    def generateLabel(self):
        return self.m_label

    def generateCode(self, fileStream):
        fileStream.write("ldc r " + self.m_label + "\n")


class CharLitNode(LiteralNode):

    def __init__(self, parent,val):
        super(CharLitNode, self).__init__(parent, val)
        self.m_type = "m_char"

    def getValue(self):
        return str(self.m_value)

    def generateLabel(self):
        return self.m_label

    def generateCode(self, fileStream):
        fileStream.write("ldc c " + self.m_label + "\n")


class BoolLitNode(LiteralNode):

    def __init__(self, parent,val):
        super(BoolLitNode, self).__init__(parent, val)
        self.m_type = "m_bool"

    def getValue(self):
        return str(self.m_value)

    def generateLabel(self):
        return self.m_label

    def generateCode(self, fileStream):
        if self.m_label == "true":
            fileStream.write("ldc b t\n")
        else:
            fileStream.write("ldc b f\n")
