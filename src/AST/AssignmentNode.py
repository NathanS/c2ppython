from AST.ASTNode import ASTNode
from Util.AddressSpace import AddressSpace
from AST.ExpressionNode import ArgListNode
from AST.TypeNode import ArrayNode

class AssignmentNode(ASTNode):

    def __init__(self, parent):
        super(AssignmentNode,self).__init__(parent, "assignment")
        self.lhs = None
        self.rhs = None
        self.m_type = "m_void"
        self.m_label = "="
        self.m_idname = "_default_"
        self.m_idnode = None
        self.m_decl = False
        self.m_assigntype = ""

    def generateLabel(self):
        self.m_label = self.m_type + " " + self.m_idnode.getLabel()
        return self.m_label

    def setIdentifier(self, _id):
        self.m_idname = _id

    def generateCode(self, fileStream):
        #Generate code for LHS of Assignment (IdentifierNode | ArrayNode | DeclarationNode)
        self.children[0].generateCode(fileStream, False)
        
        l, rel = AddressSpace.getAddress(self.children[0].m_idname)

        if not isinstance(self.children[1], ArgListNode):
            ptype, _ = AddressSpace.pType(self.children[1].m_type)
            if(self.m_assigntype == "+="):
                self.children[1].generateCode(fileStream)
                self.children[0].generateCodeR(fileStream)
                fileStream.write("add {}\n".format(ptype))
            elif(self.m_assigntype == "-="):
                self.children[0].generateCodeR(fileStream)
                self.children[1].generateCode(fileStream)
                fileStream.write("sub {}\n".format(ptype))
            elif(self.m_assigntype == "*="):
                self.children[1].generateCode(fileStream)
                self.children[0].generateCodeR(fileStream)
                fileStream.write("mul {}\n".format(ptype))
            elif(self.m_assigntype == "/="):
                self.children[0].generateCodeR(fileStream)
                self.children[1].generateCode(fileStream)
                fileStream.write("div {}\n".format(ptype))
            elif(self.m_assigntype == "%="):
                a = self.children[0]
                b = self.children[1]
                a.generateCode(fileStream)
                b.generateCode(fileStream)
                a.generateCode(fileStream)
                b.generateCode(fileStream)
                fileStream.write("div {}\n".format(ptype))
                fileStream.write("mul {}\n".format(ptype))
                fileStream.write("sub {}\n".format(ptype))
            elif(self.m_assigntype == "="):
                self.children[1].generateCode(fileStream)
            else:
                pass

            if rel:
                if (isinstance(self.children[0], ArrayNode) or self.children[0].m_ptr):
                    fileStream.write("sto {}\n".format(ptype))
                else:
                    fileStream.write("str {} 0 {}\n".format(ptype,l))
            else:
                if (isinstance(self.children[0], ArrayNode) or self.children[0].m_ptr):
                    fileStream.write("sto {}\n".format(ptype))
                else:
                    fileStream.write("sro {} {}\n".format(ptype, l))
        else:
            self.children[1].generateCode(fileStream)
        #Top of stack should now hold the evaluation of the expression on the rhs
        #And should hold the location of the variable on the LHS
        #All we need to do now is STO t
