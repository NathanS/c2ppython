from AST.ASTNode import ASTNode
#from CompilationUnitNode import CompilationUnitNode
from AST.BlockNode import BlockNode
from Util.AddressSpace import AddressSpace

class FunctionDeclarationNode(ASTNode):

    def __init__(self, parent, _name = "function_declaration"):
        super(FunctionDeclarationNode, self).__init__(parent, _name)
        self.m_returnval = "m_void"
        self.m_idname = "_default_"
        self.m_parameters = []

    def getInfo(self):
        return str(self.m_idname)

    def generateLabel(self):
        self.label = self.m_returnval + " " + self.m_idname + "("
        for p in self.m_parameters:
            self.label += p.getLabel() + " "
        self.label += ")"
        return self.label

    def generateCode(self, fileStream):
        fileStream.write("%s:\n" % ("fun_"+self.m_idname))
        fileStream.write("ssp {}\n".format(5+len(self.m_parameters)))
        #Code for parameters is on top of stack now
        for c in self.children:
            if isinstance(c, BlockNode):
                AddressSpace.addFunction(self.m_idname, len(self.m_parameters))
                for k in self.m_parameters:
                    AddressSpace.addVariable(k.children[0].m_idname)
                c.generateCode(fileStream)
                AddressSpace.removeFunction(self.m_idname)
        fileStream.write("retp\n")

    def __str__(self):
        return "{} {} {}".format(self.m_idname, self.m_parameters, self.m_returnval)


class ParamListNode(ASTNode):

    def __init__(self,parent):
        super(ParamListNode, self).__init__(parent,"param_list")

    def generateCode(self, fileStream):
        pass

class ParameterNode(ASTNode):

    def __init__(self,parent):
        super(ParameterNode, self).__init__(parent, "parameter")

    def generateCode(self, fileStream):
        pass

    def generateLabel(self):
        self.label = self.getType() + " " + self.getName()
        return self.label
