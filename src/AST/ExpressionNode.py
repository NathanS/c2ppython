from AST.ASTNode import ASTNode
from Util.AddressSpace import AddressSpace
from Util.LabelGenerator import LabelGenerator
from AST.IdentifierNode import IdentifierNode
from AST.TypeNode import ArrayNode
from AST.LiteralNode import IntegerLitNode, FloatLitNode

import re

class ExpressionNode(ASTNode):

    def __init__(self, parent, _name = "expression"):
        super(ExpressionNode, self).__init__(parent, _name)

    def generateLabel(self):
        return self.m_label

    def generateCode(self, fileStream):
        pass


class FunctionCallNode(ExpressionNode):

    def __init__(self, parent):
        super(FunctionCallNode,self).__init__(parent, "function_call")
        self.m_type = "_default_" #TODO: Choose default or void?
        self.m_parameters = []
        self.m_label = None
        self.m_fcall_identifier = "_default_"

    def generateLabel(self):
        self.m_label = self.m_fcall_identifier
        for param in self.m_parameters:
            self.m_label += param.getLabel()
        return self.m_label

    def generateCode(self, fileStream):
        #If we have printF or scanF, we
        if(self.m_fcall_identifier == "printf"):
            #Case 1 string, no formatting:
            if(len(self.m_parameters) == 1):
                strlitnode = self.m_parameters[0]
                my_str = strlitnode.m_label[1:-1]#Remove quotation marks
                x = 0
                if(len(my_str)-1) == 0:
                    fileStream.write("ldc c '%s'\n" % my_str[x])
                    fileStream.write("out c\n")
                else:
                    while x < len(my_str):
                        if (my_str[x] == "\\" and (my_str[x+1] == 'n')):
                            #We have to print a newline:
                            x += 2
                            fileStream.write("ldc c 10\n")
                            fileStream.write("out c\n")
                        else:
                            fileStream.write("ldc c '%s'\n" % my_str[x])
                            fileStream.write("out c\n")
                            x += 1
            #Case 2: Format String(%i, %s, %f, %c), followed by the parameters
            elif(len(self.m_parameters) > 1):
                strlitnode = self.m_parameters[0]
                stri = strlitnode.m_label[1:-1]#Remove quotation marks
                self.m_parameters.pop(0)
                params = re.findall('%\d*[^\s]',stri)
                x = 0
                while x < len(stri):
                    if (stri[x] == "\\" and (stri[x+1] == 'n')):
                        #We have to print a newline:
                        x += 2
                        fileStream.write("ldc c 10\n")
                        fileStream.write("out c\n")
                    elif len(params) == 0 or stri[x:x+len(params[0])] != params[0]:
                        #We just keep printing as normal
                        fileStream.write("ldc c '%s'\n" % stri[x])
                        fileStream.write("out c\n")
                        x += 1
                    else:
                        node = self.m_parameters.pop(0)
                        if 's' in params[0]:
                            x += len(params[0])
                            if isinstance(node, IdentifierNode):
                                req = AddressSpace.getArraySize(node.m_idname)
                                l, rel = AddressSpace.getAddress(node.m_idname)
                                for i in range(req):
                                    if(rel):
                                        fileStream.write("lod a 0 {}\n".format(l))
                                    else:
                                        fileStream.write("ldc a {}\n".format(l))
                                    fileStream.write("ldc i {}\n".format(i))
                                    fileStream.write("ixa 1\n")
                                    fileStream.write("ind c\n")
                                    fileStream.write("out c\n")
                            else:
                                node.generateCode(fileStream)
                                strlen = len(node.m_label[1:-1])
                                for _ in range(strlen):
                                    fileStream.write("out c\n")
                        else:
                            #print "MATCH"
                            #print params[0]
                            x += len(params[0])
                            
                            node.generateCode(fileStream)
                            ptype, _ = AddressSpace.pType(node.m_type)
                            fileStream.write("out {}\n".format(ptype))
                        params.pop(0)
                        
                                                
        elif(self.m_fcall_identifier == "scanf"):
            #Scanf takes 2 parameters: Scanf(a,b);
            #Where a = ""{%s, %i, %f, %c}""
            #Where b = Identifier
            #During AST building, we already typechecked, so we assume correctness

            idnode = self.m_parameters[1]#contains the identifier
            #Generate Code to read
            loop = False
            if (idnode.getType() == "m_int" or idnode.getType() == "m_int*"):
                optype = "i"
            elif (idnode.getType() == "m_float" or idnode.getType() == "m_float*"):
                optype = "r"
            elif (idnode.getType() == "m_char" or idnode.getType() == "m_char*"):
                optype = "c"
            elif idnode.getType() == "m_char[]":
                optype = "c"
                loop = True


            if loop:
                label1 = LabelGenerator.generateLabel("while")
                label2 = LabelGenerator.generateLabel("while")
                fileStream.write("ldc i 0\n")
                l, rel = AddressSpace.getAddress(label1)
                spacereq = AddressSpace.getArraySize(idnode.m_idname)
                fileStream.write(label1 + ":\n")
                fileStream.write("lod i 0 {}\n".format(l))
                fileStream.write("ldc i {}\n".format(spacereq))
                fileStream.write("les i\n")
                fileStream.write("fjp " + label2 + "\n")
                idnode.generateCodeL(fileStream, False)

                l1, rel1 = AddressSpace.getAddress(idnode.m_idname)
                if rel1:
                    fileStream.write("lda 0 {}\n".format(l1))
                else:
                    fileStream.write("ldc a {}\n".format(l1))

                fileStream.write("lod i 0 {}\n".format(l))
                fileStream.write("dpl i\n")
                fileStream.write("inc i 1\n")
                fileStream.write("str i 0 {}\n".format(l))
                fileStream.write("inc i 1\n")
                fileStream.write("ixa 1\n".format())

                fileStream.write("in c\n")
                fileStream.write("dpl c\n")
                fileStream.write("ldc c 27\n")
                fileStream.write("neq c\n")
                fileStream.write("fjp " + label2 + "\n")
                fileStream.write("sto c\n")
                fileStream.write("ujp {}\n".format(label1))
                fileStream.write(label2 + ":\n")
            else:
                idnode.generateCodeL(fileStream, False)#Not a decl
                fileStream.write("in %c\n" % optype)
                #Top of stack now holds: SP-2 Adress, SP-1 Value
                l, rel = AddressSpace.getAddress(idnode.m_idname)
                if(rel):
                    fileStream.write("str {} 0 {}\n".format(optype, l))
                else:
                    fileStream.write("sto {}\n".format(optype))
                #Generate code to store the value
        else:
            fileStream.write("mst 0\n")
            params = len(self.m_parameters)
            for p in self.m_parameters:
                p.generateCode(fileStream)
            fileStream.write("cup {} {}\n".format(params, "fun_"+self.m_fcall_identifier))



    def getStorageReq(self):
        #This method should parse m_parameters, and calculate the amount
        #of stackspace needed for the parameters.
        #sizeof(Array) = len(Array)-spaces
        #sizeof(int) == sizeof(char) == sizeof(float) == 1
        return len(self.m_parameters)

class AddExprNode(ExpressionNode):

    def __init__(self, parent):
        super(AddExprNode, self).__init__(parent, "add_expression")
        self.m_type = "_default_"  # This type holds the type of its operands
        self.m_label = "+"

    def generateCode(self, fileStream):
        for c in self.children:
            c.generateCode(fileStream)
        #Op1 and Op2 should now be at top of stack
        ptype, _ = AddressSpace.pType(self.m_type)
        fileStream.write("add {}\n".format(ptype))


class DivExprNode(ExpressionNode):

    def __init__(self, parent):
        super(DivExprNode, self).__init__(parent, "div_expression")
        self.m_type = "_default_"  # This type holds the type of its operands
        self.m_label = "/"

    def generateCode(self, fileStream):

        ptype, _ = AddressSpace.pType(self.m_type)
        for c in self.children:
            c.generateCode(fileStream)
        fileStream.write("div {}\n".format(ptype))
        #Op1 and Op2 should now be at top of stack

class SubtractExprNode(ExpressionNode):

    def __init__(self, parent):
        super(SubtractExprNode, self).__init__(parent, "subtract_expression")
        self.m_type = "_default_"  # This type holds the type of its operands
        self.m_label = "-"

    def generateCode(self, fileStream):
        for c in self.children:
            c.generateCode(fileStream)
        #Op1 and Op2 should now be at top of stack
        ptype, _ = AddressSpace.pType(self.m_type)
        fileStream.write("sub {}\n".format(ptype))

class MultExprNode(ExpressionNode):

    def __init__(self, parent):
        super(MultExprNode, self).__init__(parent, "mult_expression")
        self.m_label = "*"

    def generateCode(self, fileStream):
        for c in self.children:
            c.generateCode(fileStream)
        #Op1 and Op2 should now be at top of stack
        ptype, _ = AddressSpace.pType(self.m_type)
        fileStream.write("mul {}\n".format(ptype))

class ModExprNode(ExpressionNode):

    def __init__(self, parent):
        super(ModExprNode, self).__init__(parent, "mod_expression")
        self.m_label = '%'

    def generateCode(self, fileStream):

        ptype, _ = AddressSpace.pType(self.m_type)

        # For a % b = a - b * (a//b)
        a = self.children[0]
        b = self.children[1]

        a.generateCode(fileStream)

        b.generateCode(fileStream)

        a.generateCode(fileStream)
        b.generateCode(fileStream)
        fileStream.write("div {}\n".format(ptype)) # a/b
        fileStream.write("mul {}\n".format(ptype)) # b * (a/b)
        fileStream.write("sub {}\n".format(ptype)) # a - b * (a/b)





class AndExprNode(ExpressionNode):

    def __init__(self, parent):
        super(AndExprNode, self).__init__(parent, "and_expression")
        self.m_label = "AND"

    def generateCode(self, fileStream):
        for c in self.children:
            c.generateCode(fileStream)
        #Op1 and Op2 should now be at top of stack
        fileStream.write("and\n")


class OrExprNode(ExpressionNode):

    def __init__(self, parent):
        super(OrExprNode, self).__init__(parent, "or_expression")
        self.m_label = "OR"

    def generateCode(self, fileStream):
        for c in self.children:
            c.generateCode(fileStream)
        #Op1 and Op2 should now be at top of stack
        fileStream.write("or\n")

class RelExprNode(ExpressionNode):

    def __init__(self, parent):
        super(RelExprNode, self).__init__(parent, "rel_expression")
        self.m_rel_operation = "_default_"
        self.m_rel_operands = "_default_"

    def generateLabel(self):
        return self.m_rel_operation

    def generateCode(self, fileStream):
        for c in self.children:
            c.generateCode(fileStream)

        if(self.m_rel_operation == "=="):
            operation = "equ"
        elif(self.m_rel_operation == ">"):
            operation = "grt"
        elif(self.m_rel_operation == "<"):
            operation = "les"
        elif(self.m_rel_operation == "<="):
            operation = "leq"
        elif(self.m_rel_operation == ">="):
            operation = "geq"
        elif(self.m_rel_operation == "!="):
            operation = "neq"
        else:
            print "Encountered some error in generateCode of RelExpr"
        ptype, _ = AddressSpace.pType(self.m_rel_operands)
        fileStream.write("{} {}\n".format(operation, ptype))

class UnaryExprNode(ExpressionNode):

    def __init__(self, parent, optype):
        super(UnaryExprNode, self).__init__(parent, "unary_expression")
        self.m_optype = optype

    def generateLabel(self):
        if(self.m_optype == "-"):
            label = "-Negation-"
        elif(self.m_optype == "post++"):
            label =  "-PostIncrement-"
        elif(self.m_optype == "post--"):
            label = "-PostDecrement-"
        elif(self.m_optype == "pre++"):
            label =  "-PreIncrement-"
        elif(self.m_optype == "pre--"):
            label = "-PreDecrement-"
        elif(self.m_optype == "!"):
            label = "NOT"
        else:
            label = ""
        return label

    def generateCode(self, fileStream):
        ptype, _ = AddressSpace.pType(self.m_type)
        node = self
        lit = False
        while isinstance(node, UnaryExprNode):
            node = node.children[0]
        if(isinstance(node, (IntegerLitNode, FloatLitNode))):
            lit = True
            pass
        else:
            l, rel = AddressSpace.getAddress(node.m_idname)

        self.children[0].generateCode(fileStream)
        if ('++' in self.m_optype or '--' in self.m_optype):
            if isinstance(node, ArrayNode):
                store = "sto {}\n".format(ptype)
            elif isinstance(node, IdentifierNode):
                if rel:
                    store = "str {} 0 {}\n".format(ptype, l)
                else:
                    store = "sro {} {}\n".format(ptype, l)

            if(self.m_optype == "post++"):
                node.generateCode(fileStream, False)
                self.children[0].generateCode(fileStream)
                fileStream.write("inc {} 1\n".format(ptype))
            elif(self.m_optype == "post--"):
                node.generateCode(fileStream, False)
                self.children[0].generateCode(fileStream)
                fileStream.write("dec {} 1\n".format(ptype))
            elif(self.m_optype == "pre++"):
                fileStream.write("inc {} 1\n".format(ptype))
                node.generateCode(fileStream, False)
                self.children[0].generateCode(fileStream)
            elif(self.m_optype == "pre--"):
                fileStream.write("dec {} 1\n".format(ptype))
                node.generateCode(fileStream, False)
                self.children[0].generateCode(fileStream)
            fileStream.write(store)

        #Generate code for the child first, whether we want to Negate it, or increment/decrement
        #it as a post fix, we first have to evaluate the value, then apply our operation to it.
        elif(self.m_optype == "-"):
            fileStream.write("neg {}\n".format(ptype))
        elif(self.m_optype == "!"):
            fileStream.write("not\n")


class PostfixExprNode(ExpressionNode):

    def __init__(self, parent):
        super(PostfixExprNode, self).__init__(parent, "postfix_expression")

    def generateCode(self, fileStream):
        pass

class PrimaryExprNode(ExpressionNode):

    def __init__(self, parent):
        super(PrimaryExprNode, self).__init__(parent, "primary_expression")

    def generateCode(self, fileStream):
        pass

class ArgListNode(ASTNode):
    def __init__(self, parent):
        super(ArgListNode, self).__init__(parent, "argList")
        self.m_arguments = []
        self.m_idname = "_default_"

    def generateLabel(self):
        self.m_label = "( "
        for c in self.m_arguments:
            self.m_label += c.getLabel() + " "
        self.m_label += ")"
        return self.m_label

    def generateCode(self, fileStream):
        l, rel = AddressSpace.getAddress(self.m_idname)
        for i, c in enumerate(self.m_arguments):
            ptype, _ = AddressSpace.pType(c.m_type)
            #fileStream.write("ldc a {}\n".format(l+i))
            c.generateCode(fileStream)
            if rel:
                fileStream.write("str {} 0 {}\n".format(ptype,l+i+1))
            else:
                fileStream.write("sro {} {}\n".format(ptype, l+i+1))
