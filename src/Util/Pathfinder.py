from AST.StatementNode import *

class Path(object):
    def __init__(self, start):
        self.m_start = start
        self.m_path = []
        self.m_path.append(start)
        self.m_paths = []

    def appendPath(self, n):
        self.m_path.append(n)

    def checkReturn(self):
        for c in self.m_path:
            if isinstance(c, ReturnStmtNode):
                return True
        return False
    def build(self):
        pass


def buildPaths(node):
    #Node is the blocknode of the function:
    paths = []
    for c in node.children:
        if isinstance(c, (IfStmtNode, ElseStmtNode, ForStmtNode, WhileStmtNode)):
            p = Path(c)
            paths.append(p)
    #paths now holds all possible starting points for paths
    for path in paths:
        path.build()
        p = path.checkReturn()
        if not p:
            return False

def freturns(node):
    ifret = False
    elseret = False
    for c in node.children[1].children:#Check the block
        if isinstance(c, ReturnStmtNode):
            ifret = True
        elif isinstance(c, IfStmtNode):
            ifret = ifret or freturns(c)
        else:
            pass
    if len(node.children) == 3:
        for k in node.children[2].children[0].children:
            if isinstance(k, ReturnStmtNode):
                elseret = True
            elif isinstance(k, IfStmtNode):
                elseret = elseret or freturns(k)
            else:
                pass
    return ifret and elseret
