class AddressSpace(object):
    _locspace = [0]
    _entries = [[]]
    _functions = []
    _stack = []

    @staticmethod
    def addFunction(name, space):
        f = FunctionEntry(name, space)
        AddressSpace._functions.insert(0,f)
        AddressSpace._locspace.insert(0,5)
        AddressSpace._entries.insert(0,[])

    @staticmethod
    def addVariable(name):
        if len(AddressSpace._functions) == 0:
            e = VarEntry(name, AddressSpace._locspace[0], False)
        else:
            e = VarEntry(name, AddressSpace._locspace[0], True, AddressSpace._functions[0])
        loc = AddressSpace._locspace[0]
        AddressSpace._locspace[0] += 1
        AddressSpace._entries[0].insert(0, e)
        return loc, len(AddressSpace._functions) != 0

    @staticmethod
    def addVarArray(name, space):
        if len(AddressSpace._functions) == 0:
            e = VarArrayEntry(name, AddressSpace._locspace[0], False, space, None)
        else:
            e = VarArrayEntry(name, AddressSpace._locspace[0], True, space, AddressSpace._functions[0])
        loc = AddressSpace._locspace[0]
        AddressSpace._locspace[0] += space+1
        AddressSpace._entries[0].insert(0, e)
        return loc, len(AddressSpace._functions) != 0

    @staticmethod
    def getArraySize(name):
        for entries in AddressSpace._entries:
            if len(entries) > 0:
                for c in entries:
                    if c.m_name == name:
                        return c.m_spacereq

    @staticmethod
    def removeFunction(name):
        AddressSpace._entries.pop(0)
        AddressSpace._functions.pop(0)
        AddressSpace._locspace.pop(0)

    @staticmethod
    def getAddress(name, index = 0):
        for entries in AddressSpace._entries:
            if len(entries) > 0:
                for c in entries:
                    if c.m_name == name:
                        return c.m_address, c.m_relative
        if(index == 0):
            return AddressSpace.addVariable(name)
        else:
            return AddressSpace.addVarArray(name, index)


    @staticmethod
    def pType(_type):
        if  '*' in _type or '[]' in _type: return "a", 0
        elif _type == "m_int": return "i", 0
        elif _type == "m_float": return "r", 0.0
        elif _type == "m_char": return "c", 0
        elif _type == "m_bool": return "b", "t"
        else:
            print "Error at pType: {} !".format(_type)

    @staticmethod
    def loadEntry(name):
        pass
    
    @staticmethod
    def pushStack(_cont, _break):
        AddressSpace._stack.insert(0, (_cont,_break))
    
    @staticmethod
    def popStack():
        AddressSpace._stack.pop(0)
    
    @staticmethod
    def getTopBreak():
        return AddressSpace._stack[0][1]
    
    @staticmethod
    def getTopCont():
        return AddressSpace._stack[0][0]


class Entry(object):
    def __init__(self, name):
        self.m_name = name

class VarEntry(Entry):
    def __init__(self, name, a, rel, function = None):
        super(VarEntry, self).__init__(name)
        self.m_address = a
        self.m_relative = rel
        self.m_function = function

    def __str__(self):
        return "name = {} with address {}, relative is {}".format(self.m_name, self.m_address, self.m_relative)

class VarArrayEntry(Entry):
    def __init__(self, name, a, rel, spacereq, function = None):
        super(VarArrayEntry, self).__init__(name)
        self.m_address = a
        self.m_relative = rel
        self.m_function = function
        self.m_spacereq = spacereq

    def __str__(self):
        return "name = {} with address = {}, relative is = {}, requiring space = {}".format(self.m_name, self.m_address, self.m_relative, self.m_spacereq)


class FunctionEntry(Entry):
    def __init__(self, name, space):
        super(FunctionEntry, self).__init__(name)
        self.m_space = space
        #TODO: Depth needed?

    def __str__(self):
        return "name = {} with space {}".format(self.m_name, self.m_space)
