#This class will generate the labels for the conditional jumps in the P-code

class LabelGenerator(object):

    m_ifLabelCount = 0
    m_whileLabelCount = 0
    m_elseLabelCount = 0
    m_forLabelCount = 0

    @staticmethod
    def generateLabel(ltype):
        returnlabel = ""
        if (ltype == "if"):
            returnlabel = "IF" + str(LabelGenerator.m_ifLabelCount)
            LabelGenerator.m_ifLabelCount += 1
        elif (ltype == "else"):
            returnlabel = "ELSE" + str(LabelGenerator.m_elseLabelCount)
            LabelGenerator.m_elseLabelCount += 1
        elif (ltype == "while"):
            returnlabel = "WHILE" + str(LabelGenerator.m_whileLabelCount)
            LabelGenerator.m_whileLabelCount += 1
        elif (ltype == "for"):
            returnlabel = "FOR" + str(LabelGenerator.m_forLabelCount)
            LabelGenerator.m_forLabelCount += 1
        return returnlabel
