# Contains the method to print our own AST tree using Graphviz
import graphviz as gv
import functools
from AST.DeclarationNode import DeclarationNode
from AST.StatementNode import *


def generateNode(node, graph):
    # file is the file Handle
    # node is the node to write from
    graph.node(node.getUniqueName(),node.getLabel())

    for child in node.getChildren():
        graph = generateNode(child,graph)
        graph.edge(node.getUniqueName(),child.getUniqueName())
    return graph

def generateImage(root, filename):
    # Root is the ASTNode that is the root
    # file is the file.gv to write to
    graph = gv.Digraph(format="gv")
    graph = generateNode(root,graph)
    graph.render(filename+".gv")

def typePrinter(root):
    print "CurrentNodeType:[%s]" % type(root)
    for c in root.children:
        if type(c) != None:
            print "\t",type(c)
    for c in root.children:
        if type(c) != None:
            typePrinter(c)
