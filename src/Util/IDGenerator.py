class IDGenerator(object):
    _id = 0
    @staticmethod
    def getID():
        new_id = IDGenerator._id
        IDGenerator._id += 1
        return new_id
    
