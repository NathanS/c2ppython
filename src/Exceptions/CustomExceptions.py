class BaseCompilerException(Exception):
    def __init__(self):
        pass

class TerminationException(BaseCompilerException):
    def __init__(self):
        pass

class ReservedWordException(BaseCompilerException):
    def __init__(self, k):
        self.ln = -1  # Holds the line Number
        self.keyword = k

    def error(self, l, c):
        print "Redeclaration of reserved word [%s] at line %i, column %i." % (self.keyword, l, c)
        raise TerminationException

class IllegalCompilationUnitException(BaseCompilerException):
    def __init__(self):
        pass

    def error(self):
        print "Compilation Unit does not hold a main function, which is mandatory. Aborting."
        raise TerminationException

class RedeclarationException(BaseCompilerException):
    def __init__(self, k):
        self.ln = -1  # Holds the line Number
        self.keyword = k

    def error(self, l, c):
        print "Redeclaration of variable [%s] at line %i, column %i." % (self.keyword, l, c)
        raise TerminationException

class MultipleForwardDeclarationException(BaseCompilerException):
    def __init__(self, k):
        self.ln = -1  # Holds the line Number
        self.keyword = k

    def error(self, l, c):
        print "Redeclaration of ForwardDecl [%s] at line %i, column %i." % (self.keyword, l, c)
        raise TerminationException

class VariableNotDeclaredException(BaseCompilerException):
    def __init__(self, k):
        self.ln = -1  # Holds the line Number
        self.keyword = k

    def error(self, l, c):
        print "Variable [%s] at l:%i, c:%i. Was not declared." % (self.keyword, l, c)
        raise TerminationException

class TinyCTypeException(BaseCompilerException):
    def __init__(self, t1, t2):
        self._t1 = t1
        self._t2 = t2

    def error(self, l, c):
        print "Type error at l:%i,c:%i. Type %s does not match %s" % (l, c, self._t1, self._t2)
        raise TerminationException

class TinyCParserException(BaseCompilerException):
    def __init__(self):
        pass

class ExplicitTypeOrInitException(BaseCompilerException):
    def __init__(self, k):
        self.ln = -1  # Holds the line Number
        self.keyword = k

    def error(self, l, c):
        print "Definition error of variable [%s] at l:%i, c:%i. Variable with array type needs an explicit size or an initializer" % (self.keyword, l, c)
        raise TerminationException

class ExcessElementException(BaseCompilerException):
    def __init__(self, k):
        self.keyword = k

    def error(self, l, c):
        print "Size error of variable [%s] at l:%i, c:%i. Excess elements in Array initializer" % (self.keyword, l, c)
        raise TerminationException

class VariableArraySizeInitException(BaseCompilerException):
    def __init__(self, k):
        self.keyword = k

    def error(self, l, c):
        print "Declaration error of variable [%s] at l:%i, c:%i. Array with variable size may not be initialized" % (self.keyword, l, c)
        raise TerminationException

class ExpressionNotAssignableException(BaseCompilerException):
    def __init__(self):
        pass

    def error(self, l, c):
        print "Assignment error at l:%i, c:%i. Expression is not assignable" %(l, c)
        raise TerminationException

class MatchingFunctionCallException(BaseCompilerException):
    def __init__(self,k):
        self.keyword = k
        pass

    def error(self, l, c):
        print "l:%i, c:%i. No matching function for call to '%s'" %(l, c, self.keyword)
        raise TerminationException

class InvalidStringFormatException(BaseCompilerException):
    def __init__(self):
        pass

    def error(self, l, c):
        print "l:%i, c:%i. Invalid String Format for Scanf" %(l, c)
        raise TerminationException

class ArrayOutOfBoundsException(BaseCompilerException):
    def __init__(self):
        pass

    def error(self, l, c):
        print "l:%i, c:%i. Array is out of bounds." %(l, c)
        raise TerminationException

class IllegalOperationException(BaseCompilerException):
    def __init__(self, optype, types):
        self.m_optype = optype
        self.m_types = types
        pass

    def error(self, l, c):
        print "l:%i, c:%i. Operation: %s, can only be called on %s." %(l, c, self.m_optype, self.m_types)
        raise TerminationException

class WrongParameterCountException(BaseCompilerException):
    def __init__(self, n1, n2, t):
        self.expected = n1
        self.received = n2
        self.fname = t

    def error(self, l, c):
        print "At L:[%i], C:[%i]: Expected [%i] parameters, received [%i] for %s." %(l, c, self.expected, self.received, self.fname)
        raise TerminationException
class ForwardDeclarationParameterMismatch(BaseCompilerException):
    def __init__(self, t):
        self.fname = t

    def error(self, l, c):
        print "At L:[%i], C:[%i]: Parameters of %s do not match Forwarddeclaration of %s." %(l, c, self.fname, self.fname)
        raise TerminationException

class ForwardDeclNotDefinedException(BaseCompilerException):
    def __init__(self):
        pass

    def error(self):
        print "[Error] All Forward Declarations must be defined within the Compilation Unit."
        raise TerminationException

class IndirectNotOnPointerException(BaseCompilerException):
    def __init__(self, name, _type):
        self.m_name = name
        self.m_type = _type

    def error(self, l, c):
        print "l:%i, c:%i. Cannot indirect variable [%s] of non-pointer type %s" %(l, c, self.m_name, self.m_type)
        raise TerminationException

class DoubleReferenceNotSupportedException(BaseCompilerException):
    def __init__(self, name, _type):
        self.m_name = name
        self.m_type = _type

    def error(self, l, c):
        print "l:%i, c:%i. Variable [%s] of type %s cannot be referenced again (Not supported)" %(l, c, self.m_name, self.m_type)
        raise TerminationException

class ArrayTypeNotAssignableException(BaseCompilerException):
    def __init__(self, name, _type):
        self.m_name = name
        self.m_type = _type

    def error(self, l, c):
        print "l:%i, c:%i. Variable [%s] of array type %s is not assignable" %(l, c, self.m_name, self.m_type)
        raise TerminationException

class ReturningVoidNonVoidFunctionException(BaseCompilerException):
    def __init__(self):
        pass

    def error(self, l, c):
        print "l:%i, c:%i. Returning Void in Non Void Function." %(l, c)
        raise TerminationException

class IllegalIndexException(BaseCompilerException):
    def __init__(self, name):
        self.m_name = name
        pass

    def error(self, l, c):
        print "l:%i, c:%i. Illegal Indexing on identifier of non-array type: %s" %(l, c, self.m_name)
        raise TerminationException

class IllegalDereferenceException(BaseCompilerException):
    def __init__(self, name):
        self.m_name = name
        pass

    def error(self, l, c):
        print "l:%i, c:%i. Illegal dereferencing on identifier of non-pointer type: %s" %(l, c, self.m_name)
        raise TerminationException

class IllegalReferenceException(BaseCompilerException):
    def __init__(self, name):
        self.m_name = name
        pass

    def error(self, l, c):
        print "l:%i, c:%i. Illegal Referencing on identifier of non-base type: %s" %(l, c, self.m_name)
        raise TerminationException

class IllegalFormatException(BaseCompilerException):
    def __init__(self, name):
        self.m_name = name
    
    def error(self, l, c):
        print "l:%i, c:%i. Illegal format exception: %s" %(l, c, self.m_name)
        raise TerminationException
