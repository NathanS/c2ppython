from Compiler.Compiler import Compiler
import filecmp
from Tests.Test import Test, PcodeTest
from Tests.Case import Case
import sys


def run(cases):
    total_failed = 0
    total_ran = 0
    num_tests = sum([x.size() for x in cases])
    print "\n"
    print "[==========] Running %d test(s) from %d test case(s)."%(num_tests, len(cases))
    print "[----------] Global test environment set-up"
    for case in cases:
        tests_passed = 0
        tests_ran = 0
        print "[----------] Running %d tests from test case %s"%(case.size(), case.getName())
        for test in case.getTests():
            print "[   RUN    ] %s"%(test.getName())
            if (test.execute()):
                print "[       OK ] %s"%(test.getName())
                tests_passed += 1
            else:
                print "[   FAILED ] %s"%(test.getName())
                total_failed += 1
            total_ran += 1
            tests_ran += 1

        print "[----------] %d / %d tests from test case %s passed"%(tests_passed, tests_ran, case.getName())
        print ""
    print "[==========] %d tests ran"%(total_ran)
    if (total_failed != 0):
        print "[   FAILED ] A total of %d tests failed"%(total_failed)
    else:
        print "[  PASSED  ] All tests succeeded"


def main():
    cases = []

    #start of case
    tests = []
    tests.append(Test("CorrectFile1 - Minimal", "CorrectTinyCFile.C", "CorrectTinyCFile.out"))
    tests.append(Test("CorrectFile2 - All Types", "CorrectTinyCFile2.C", "CorrectTinyCFile2.out"))
    tests.append(Test("CorrectFile3 - If Else Loop", "CorrectTinyCFile3.C", "CorrectTinyCFile3.out"))
    tests.append(Test("CorrectFile4 - While Loop", "CorrectTinyCFile4.C", "CorrectTinyCFile4.out"))
    tests.append(Test("CorrectFile5 - Empty", "CorrectTinyCFile5.C", "CorrectTinyCFile5.out"))
    tests.append(Test("CorrectFile6 - Include", "CorrectTinyCFile6.C", "CorrectTinyCFile6.out"))
    tests.append(Test("CorrectFile7 - Global Variable", "CorrectTinyCFile7.C", "CorrectTinyCFile7.out"))
    tests.append(Test("CorrectFile8 - Sum", "CorrectTinyCFile8.C", "CorrectTinyCFile8.out"))
    tests.append(Test("CorrectFile9 - Prod", "CorrectTinyCFile9.C", "CorrectTinyCFile9.out"))
    tests.append(Test("CorrectFile10 - Sub", "CorrectTinyCFile10.C", "CorrectTinyCFile10.out"))
    tests.append(Test("CorrectFile11 - Division", "CorrectTinyCFile11.C", "CorrectTinyCFile11.out"))
    tests.append(Test("CorrectFile12 - Nested Sum", "CorrectTinyCFile12.C", "CorrectTinyCFile12.out"))
    tests.append(Test("CorrectFile13 - Nested Div", "CorrectTinyCFile13.C", "CorrectTinyCFile13.out"))
    tests.append(Test("CorrectFile14 - Nested Sub", "CorrectTinyCFile14.C", "CorrectTinyCFile14.out"))
    tests.append(Test("CorrectFile15 - Nested Mult", "CorrectTinyCFile15.C", "CorrectTinyCFile15.out"))
    tests.append(Test("CorrectFile16 - Nested Sum_Div_Mult_Sub", "CorrectTinyCFile16.C", "CorrectTinyCFile16.out"))
    tests.append(Test("CorrectFile17 - Function No Param No Return", "CorrectTinyCFile17.C", "CorrectTinyCFile17.out"))
    tests.append(Test("CorrectFile18 - Function No Param Float Return", "CorrectTinyCFile18.C", "CorrectTinyCFile18.out"))
    tests.append(Test("CorrectFile19 - Function PList Float Return", "CorrectTinyCFile19.C", "CorrectTinyCFile19.out"))
    tests.append(Test("CorrectFile20 - Function PList Float Return FCall", "CorrectTinyCFile20.C", "CorrectTinyCFile20.out"))
    tests.append(Test("CorrectFile21 - Array Decl", "CorrectTinyCFile21.C", "CorrectTinyCFile21.out"))
    tests.append(Test("CorrectFile22 - Array Decl + Assign", "CorrectTinyCFile22.C", "CorrectTinyCFile22.out"))
    tests.append(Test("CorrectFile23 - Array Element Call", "CorrectTinyCFile23.C", "CorrectTinyCFile23.out"))
    tests.append(Test("CorrectFile24 - Array Elements Expression", "CorrectTinyCFile24.C", "CorrectTinyCFile24.out"))
    tests.append(Test("CorrectFile25 - ArrayDeclInitList", "CorrectTinyCFile25.C", "CorrectTinyCFile25.out"))
    tests.append(Test("CorrectFile26 - Array Elements Expression PrintF", "CorrectTinyCFile26.C", "CorrectTinyCFile26.out"))
    tests.append(Test("CorrectFile27 - Pointer Assignments", "CorrectTinyCFile27.C", "CorrectTinyCFile27.out"))
    tests.append(Test("CorrectFile28 - PostIncrement", "CorrectTinyCFile28.C", "CorrectTinyCFile28.out"))
    tests.append(Test("CorrectFile29 - PostDecrement", "CorrectTinyCFile29.C", "CorrectTinyCFile29.out"))
    tests.append(Test("CorrectFile30 - UnaryMinus", "CorrectTinyCFile30.C", "CorrectTinyCFile30.out"))
    tests.append(Test("CorrectFile31 - ReferenceAssignment", "CorrectTinyCFile31.C", "CorrectTinyCFile31.out"))
    tests.append(Test("CorrectFile32 - DereferencePointer", "CorrectTinyCFile32.C", "CorrectTinyCFile32.out"))
    tests.append(Test("CorrectFile33 - VarParFunction", "CorrectTinyCFile33.C", "CorrectTinyCFile33.out"))
    tests.append(Test("CorrectFile34 - Opt: For Loop, 1 expression.", "CorrectTinyCFile34.C", "CorrectTinyCFile34.out"))
    tests.append(Test("CorrectFile35 - Opt: For Loop, mult expression.", "CorrectTinyCFile35.C", "CorrectTinyCFile35.out"))
    tests.append(Test("CorrectFile36 - Opt: For Loop, empty e1.", "CorrectTinyCFile36.C", "CorrectTinyCFile36.out"))
    tests.append(Test("CorrectFile37 - Opt: For Loop, empty e3.", "CorrectTinyCFile37.C", "CorrectTinyCFile37.out"))
    tests.append(Test("CorrectFile38 - Opt: Continue.", "CorrectTinyCFile38.C", "CorrectTinyCFile38.out"))
    tests.append(Test("CorrectFile39 - Opt: Break.", "CorrectTinyCFile39.C", "CorrectTinyCFile39.out"))
    tests.append(Test("CorrectFile40 - Opt: Continue & Break.", "CorrectTinyCFile40.C", "CorrectTinyCFile40.out"))

    cases.append(Case("CorrectTinyC", tests))

    tests = []
    tests.append(PcodeTest("CorrectFile1 - Minimal", "CorrectTinyCFile.out.p", "CorrectTinyCFile.CORRECTP"))
    tests.append(PcodeTest("CorrectFile2 - All Types", "CorrectTinyCFile2.out.p", "CorrectTinyCFile2.CORRECTP"))
    tests.append(PcodeTest("CorrectFile3 - If Else Loop", "CorrectTinyCFile3.out.p", "CorrectTinyCFile3.CORRECTP"))
    tests.append(PcodeTest("CorrectFile4 - While Loop", "CorrectTinyCFile4.out.p", "CorrectTinyCFile4.CORRECTP"))
    tests.append(PcodeTest("CorrectFile5 - Empty", "CorrectTinyCFile5.out.p", "CorrectTinyCFile5.CORRECTP"))
    tests.append(PcodeTest("CorrectFile6 - Include", "CorrectTinyCFile6.out.p", "CorrectTinyCFile6.CORRECTP"))
    tests.append(PcodeTest("CorrectFile7 - Global Variable", "CorrectTinyCFile7.out.p", "CorrectTinyCFile7.CORRECTP"))
    tests.append(PcodeTest("CorrectFile8 - Sum", "CorrectTinyCFile8.out.p", "CorrectTinyCFile8.CORRECTP"))
    tests.append(PcodeTest("CorrectFile9 - Prod", "CorrectTinyCFile9.out.p", "CorrectTinyCFile9.CORRECTP"))
    tests.append(PcodeTest("CorrectFile10 - Sub", "CorrectTinyCFile10.out.p", "CorrectTinyCFile10.CORRECTP"))
    tests.append(PcodeTest("CorrectFile11 - Division", "CorrectTinyCFile11.out.p", "CorrectTinyCFile11.CORRECTP"))
    tests.append(PcodeTest("CorrectFile12 - Nested Sum", "CorrectTinyCFile12.out.p", "CorrectTinyCFile12.CORRECTP"))
    tests.append(PcodeTest("CorrectFile13 - Nested Div", "CorrectTinyCFile13.out.p", "CorrectTinyCFile13.CORRECTP"))
    tests.append(PcodeTest("CorrectFile14 - Nested Sub", "CorrectTinyCFile14.out.p", "CorrectTinyCFile14.CORRECTP"))
    tests.append(PcodeTest("CorrectFile15 - Nested Mult", "CorrectTinyCFile15.out.p", "CorrectTinyCFile15.CORRECTP"))
    tests.append(PcodeTest("CorrectFile16 - Nested Sum_Div_Mult_Sub", "CorrectTinyCFile16.out.p", "CorrectTinyCFile16.CORRECTP"))
    tests.append(PcodeTest("CorrectFile17 - Function No Param No Return", "CorrectTinyCFile17.out.p", "CorrectTinyCFile17.CORRECTP"))
    tests.append(PcodeTest("CorrectFile18 - Function No Param Float Return", "CorrectTinyCFile18.out.p", "CorrectTinyCFile18.CORRECTP"))
    tests.append(PcodeTest("CorrectFile19 - Function PList Float Return", "CorrectTinyCFile19.out.p", "CorrectTinyCFile19.CORRECTP"))
    tests.append(PcodeTest("CorrectFile20 - Function PList Float Return FCall", "CorrectTinyCFile20.out.p", "CorrectTinyCFile20.CORRECTP"))
    tests.append(PcodeTest("CorrectFile21 - Array Decl", "CorrectTinyCFile21.out.p", "CorrectTinyCFile21.CORRECTP"))
    tests.append(PcodeTest("CorrectFile22 - Array Decl + Assign", "CorrectTinyCFile22.out.p", "CorrectTinyCFile22.CORRECTP"))
    tests.append(PcodeTest("CorrectFile23 - Array Element Call", "CorrectTinyCFile23.out.p", "CorrectTinyCFile23.CORRECTP"))
    tests.append(PcodeTest("CorrectFile24 - Array Elements Expression", "CorrectTinyCFile24.out.p", "CorrectTinyCFile24.CORRECTP"))
    tests.append(PcodeTest("CorrectFile25 - ArrayDeclInitList", "CorrectTinyCFile25.out.p", "CorrectTinyCFile25.CORRECTP"))
    tests.append(PcodeTest("CorrectFile26 - Array Elements Expression PrintF", "CorrectTinyCFile26.out.p", "CorrectTinyCFile26.CORRECTP"))
    tests.append(PcodeTest("CorrectFile27 - Pointer Assignments", "CorrectTinyCFile27.out.p", "CorrectTinyCFile27.CORRECTP"))
    tests.append(PcodeTest("CorrectFile28 - PostIncrement", "CorrectTinyCFile28.out.p", "CorrectTinyCFile28.CORRECTP"))
    tests.append(PcodeTest("CorrectFile29 - PostDecrement", "CorrectTinyCFile29.out.p", "CorrectTinyCFile29.CORRECTP"))
    tests.append(PcodeTest("CorrectFile30 - UnaryMinus", "CorrectTinyCFile30.out.p", "CorrectTinyCFile30.CORRECTP"))
    tests.append(PcodeTest("CorrectFile31 - ReferenceAssignment", "CorrectTinyCFile31.out.p", "CorrectTinyCFile31.CORRECTP"))
    tests.append(PcodeTest("CorrectFile32 - DereferencePointer", "CorrectTinyCFile32.out.p", "CorrectTinyCFile32.CORRECTP"))
    tests.append(PcodeTest("CorrectFile33 - VarParFunction", "CorrectTinyCFile33.out.p", "CorrectTinyCFile33.CORRECTP"))
    tests.append(PcodeTest("CorrectFile34 - Opt: For Loop, 1 expression.", "CorrectTinyCFile34.out.p", "CorrectTinyCFile34.CORRECTP"))
    tests.append(PcodeTest("CorrectFile35 - Opt: For Loop, mult expression.", "CorrectTinyCFile35.out.p", "CorrectTinyCFile35.CORRECTP"))
    tests.append(PcodeTest("CorrectFile36 - Opt: For Loop, empty e1.", "CorrectTinyCFile36.out.p", "CorrectTinyCFile36.CORRECTP"))
    tests.append(PcodeTest("CorrectFile37 - Opt: For Loop, empty e3.", "CorrectTinyCFile37.out.p", "CorrectTinyCFile37.CORRECTP"))
    tests.append(PcodeTest("CorrectFile38 - Opt: Continue.", "CorrectTinyCFile38.out.p", "CorrectTinyCFile38.CORRECTP"))
    tests.append(PcodeTest("CorrectFile39 - Opt: Break.", "CorrectTinyCFile39.out.p", "CorrectTinyCFile39.CORRECTP"))
    tests.append(PcodeTest("CorrectFile40 - Opt: Continue & Break.", "CorrectTinyCFile40.out.p", "CorrectTinyCFile40.CORRECTP"))
    cases.append(Case("CorrectTinyC - Pcode", tests))


    tests = []
    tests.append(Test("Parameter-Parameter", "redecl-parpar.C", "redecl-parpar.out"))
    tests.append(Test("Parameter-Variable", "redecl-parvar.C", "redecl-parvar.out"))
    tests.append(Test("Variable-Variable", "redecl-varvar.C", "redecl-varvar.out"))
    tests.append(Test("Function-Parameter", "redecl-funcpar.C", "redecl-funcpar.out"))
    tests.append(Test("Function-Function", "redecl-funcfunc.C", "redecl-funcfunc.out"))
    tests.append(Test("Function-Variable", "redecl-funcvar.C", "redecl-funcvar.out"))
    cases.append(Case("Redeclaration", tests))

    tests = []

    tests.append(Test("ReservedWordAsParameter", "ReservedWordParameter.C", "ReservedWordParameter.out"))
    tests.append(Test("ReservedWordAsVariable", "ReservedWordVariable.C", "ReservedWordVariable.out"))
    tests.append(Test("ReservedWordAsFunction", "ReservedWordFunction.C", "ReservedWordFunction.out"))

    cases.append(Case("ReservedWords", tests))

    tests = []
    tests.append(Test("MismatchIntChar", "TypeMismatchIntChar.C", "TypeMismatchIntChar.out"))
    tests.append(Test("MismatchIntFloat", "TypeMismatchIntFloat.C", "TypeMismatchIntFloat.out"))
    tests.append(Test("MismatchCharFloat", "TypeMismatchCharFloat.C", "TypeMismatchCharFloat.out"))
    tests.append(Test("While Condition Not Boolean", "TypeMismatchWhile.C", "TypeMismatchWhile.out"))
    tests.append(Test("If Condition Not Boolean", "TypeMismatchIf.C", "TypeMismatchIf.out"))
    tests.append(Test("Array Element Incorrect Index", "IncorrectIndex.C", "IncorrectIndex.out"))

    tests.append(Test("Char Assignment To Integer Array", "CharAssIntArray.C", "CharAssIntArray.out"))
    tests.append(Test("Float Assignment To Integer Array", "FloatAssIntArray.C", "FloatAssIntArray.out"))
    tests.append(Test("Char Assignment To Float Array", "CharAssFloatArray.C", "CharAssFloatArray.out"))
    tests.append(Test("Int Assignment To Float Array", "IntAssFloatArray.C", "IntAssFloatArray.out"))
    tests.append(Test("Float Assignment To Char Array", "FloatAssCharArray.C", "FloatAssCharArray.out"))
    tests.append(Test("Int Assignment To Char Array", "IntAssCharArray.C", "IntAssCharArray.out"))

    cases.append(Case("TypeMismatching", tests))

    tests = []
    tests.append(Test("VariableNotDeclared", "VariableNotDeclared.C", "VariableNotDeclared.out"))
    tests.append(Test("FunctionNotDeclared", "FunctionNotDeclared.C", "FunctionNotDeclared.out"))
    tests.append(Test("PrintFNotDeclared", "PrintFNotDeclared.C", "PrintFNotDeclared.out"))
    tests.append(Test("ScanFNotDeclared", "ScanFNotDeclared.C", "ScanFNotDeclared.out"))
    tests.append(Test("Main not Defined", "MissingMain.C", "MissingMain.out"))
    cases.append(Case("UndeclaredVariables", tests))

    tests = []
    tests.append(Test("FunctionCallTest1 - Correct Assigment", "FunctionCallTest1.C", "FunctionCallTest1.out"))
    tests.append(Test("FunctionCallTest2 - Wrong Type Assignment", "FunctionCallTest2.C", "FunctionCallTest2.out"))
    tests.append(Test("FunctionCallTest3 - Incorrect Parameter to FunctionCall", "FunctionCallTest3.C", "FunctionCallTest3.out"))
    tests.append(Test("FunctionCallTest4 - Correct Scanf, undeclared var", "FunctionCallTest4.C", "FunctionCallTest4.out"))
    tests.append(Test("FunctionCallTest5 - Correct Printf", "FunctionCallTest5.C", "FunctionCallTest5.out"))
    tests.append(Test("FunctionCallTest6 - Printf Wrong Type for Delimeter", "FunctionCallTest6.C", "FunctionCallTest6.out"))
    tests.append(Test("FunctionCallTest7 - Scanf Undeclared Var", "FunctionCallTest7.C", "FunctionCallTest7.out"))
    tests.append(Test("FunctionCallTest8 - Scanf wrong delimeter", "FunctionCallTest8.C", "FunctionCallTest8.out"))
    tests.append(Test("FunctionCallTest9 - Correct Scanf", "FunctionCallTest9.C", "FunctionCallTest9.out"))
    tests.append(Test("FunctionCallTest10 - PrintF, Too Little Param", "PrintFTooLittleParam.C", "PrintFTooLittleParam.out"))
    tests.append(Test("FunctionCallTest11 - PrintF, Too Many Param", "PrintFSurplusParam.C", "PrintFSurplusParam.out"))
    tests.append(Test("FunctionCallTest12 - PrintF, Char Array", "PrintFArrays.C", "PrintFArrays.out"))
    tests.append(Test("FunctionCallTest13 - ScanF, Char Array", "ScanFArrays.C", "ScanFArrays.out"))
    cases.append(Case("FunctionCallTesting", tests))

    tests = []
    tests.append(PcodeTest("FunctionCallTest1 - Correct Assigment", "FunctionCallTest1.out.p", "FunctionCallTest1.CORRECTP"))
    tests.append(PcodeTest("FunctionCallTest5 - PrintF", "FunctionCallTest5.out.p", "FunctionCallTest5.CORRECTP"))
    tests.append(PcodeTest("FunctionCallTest9 - Scanf", "FunctionCallTest9.out.p", "FunctionCallTest9.CORRECTP"))
    tests.append(PcodeTest("FunctionCallTest12 - Printf", "PrintFArrays.out.p", "PrintFArrays.CORRECTP"))
    tests.append(PcodeTest("FunctionCallTest13 - Scanf", "ScanFArrays.out.p", "ScanFArrays.CORRECTP"))

    cases.append(Case("FunctionCallTesting - Pcode", tests))


    tests = []
    tests.append(Test("Array Out of Bounds - Initializer", "ExcessElementsInitializer.C", "ExcessElementsInitializer.out"))
    tests.append(Test("Integer Array No Size - Initializer", "IntArrayNoSizeInitList.C", "IntArrayNoSizeInitList.out"))
    tests.append(Test("Array Out of Bounds - IntegerDeclaration", "ExcessElementsArrayInt.C", "ExcessElementsArrayInt.out"))
    tests.append(Test("Array Out of Bounds - CharDeclaration", "ExcessElementsArrayChar.C", "ExcessElementsArrayChar.out"))
    tests.append(Test("Array Out of Bounds - FloatDeclaration", "ExcessElementsArrayFloat.C", "ExcessElementsArrayFloat.out"))

    cases.append(Case("Array Testing", tests))

    tests = []
    tests.append(Test("ForwardDeclaration without Definition", "FwdeclNoDef.C", "FwdeclNoDef.out"))
    tests.append(Test("FwDecl Wrong Param in Fwddecl", "FwdeclWrongParam.C", "FwdeclWrongParam.out"))
    tests.append(Test("FwDecl Wrong Param in Definition", "FwdeclWrongParam2.C", "FwdeclWrongParam2.out"))
    tests.append(Test("FwDecl Wrong Param in Fcall", "FwdeclWrongParam3.C", "FwdeclWrongParam3.out"))

    cases.append(Case("Forward Declaration Testing", tests))



    tests = []
    tests.append(Test("Opt: Boolean Test 1", "BooleanTest1.C", "BooleanTest1.out"))
    tests.append(Test("Opt: Boolean Test 2", "BooleanTest2.C", "BooleanTest2.out"))
    tests.append(Test("Opt: Boolean Test 3", "BooleanTest3.C", "BooleanTest3.out"))
    tests.append(Test("Opt: Boolean Test 4", "BooleanTest4.C", "BooleanTest4.out"))
    
    cases.append(Case("Opt: boolean Testing", tests))
    
    tests = []
    tests.append(PcodeTest("Opt: Boolean Test 1", "BooleanTest1.out.p", "BooleanTest1.CORRECTP"))
    tests.append(PcodeTest("Opt: Boolean Test 2", "BooleanTest2.out.p", "BooleanTest2.CORRECTP"))
    tests.append(PcodeTest("Opt: Boolean Test 4", "BooleanTest4.out.p", "BooleanTest4.CORRECTP"))

    cases.append(Case("Opt: Boolean Testing - Pcode", tests))

    tests = []
    tests.append(Test("SyntaxError1", "SyntaxError1.C", "SyntaxError1.out"))
    tests.append(Test("SyntaxError2", "SyntaxError2.C", "SyntaxError2.out"))
    tests.append(Test("SyntaxError3", "SyntaxError3.C", "SyntaxError3.out"))
    tests.append(Test("SyntaxError4", "SyntaxError4.C", "SyntaxError4.out"))
    tests.append(Test("SyntaxError5", "SyntaxError5.C", "SyntaxError5.out"))
    tests.append(Test("SyntaxError6", "SyntaxError6.C", "SyntaxError6.out"))
    tests.append(Test("SyntaxError7", "SyntaxError7.C", "SyntaxError7.out"))
    tests.append(Test("SyntaxError8", "SyntaxError8.C", "SyntaxError8.out"))
    tests.append(Test("SyntaxError9", "SyntaxError9.C", "SyntaxError9.out"))
    tests.append(Test("SyntaxError10", "SyntaxError10.C", "SyntaxError10.out"))

    cases.append(Case("SyntaxErrors", tests))

    tests = []
    tests.append(Test("CompoundOperations1 - Integers", "CompoundOperations1.C", "CompoundOperations1.out"))
    tests.append(Test("CompoundOperations2 - Floats", "CompoundOperations2.C", "CompoundOperations2.out"))
    tests.append(Test("CompoundOperations3 - ArrInt", "CompoundOperations3.C", "CompoundOperations3.out"))
    tests.append(Test("CompoundOperations4 - ArrFloat", "CompoundOperations4.C", "CompoundOperations4.out"))
    
    cases.append(Case("CompoundOpeartions", tests))
    
    tests = []
    tests.append(PcodeTest("CompoundOperations1 - Integers P", "CompoundOperations1.out.p", "CompoundOperations1.CORRECTP"))
    tests.append(PcodeTest("CompoundOperations2 - Floats P", "CompoundOperations2.out.p", "CompoundOperations2.CORRECTP"))
    tests.append(PcodeTest("CompoundOperations3 - ArrInt P", "CompoundOperations3.out.p", "CompoundOperations3.CORRECTP"))
    tests.append(PcodeTest("CompoundOperations4 - ArrFloat P", "CompoundOperations4.out.p", "CompoundOperations4.CORRECTP"))
    
    cases.append(Case("CompoundOperations - Pcode", tests))



    run(cases)

if __name__ == "__main__":
    main()
