# Create a symbol table akin to this example. http://www.cs.cornell.edu/courses/cs412/2008sp/lectures/lec12.pdf
# General idea is to start with a Parent ST which represents the Global scope.
# For each block/procedure, we add a child ST to that ST to represent scoping.

# List Implementation
# When building the Symbol tree, Start with the inner most block,
# Add all semantic values from the block into the ST and create a
# new ST for the next outer block, and fill.
from Exceptions.CustomExceptions import *


class SymbolTable(object):

    def __init__(self, parent=None, context = "_default_"):
        self.children = []
        self.parent = parent
        self.symbols = []
        self.reservedWords = ["if", "else", "then", "for", "while", "do",
        "break", "continue", "int", "float", "char", "bool", "void", "scanf", "printf"]
        self.context = context #context holds the AST element to which it refers


    def insert(self, symbol):
        if(symbol.name not in self.reservedWords):
            if not self.checkCurrentScope(symbol.name):

                self.symbols.append(symbol)
            else:
                raise RedeclarationException(symbol.name)
        else:
            raise ReservedWordException(symbol.name)

    def remove(self, symbol):
        self.symbols = filter(lambda x: x.name != symbol.name, self.symbols)

    def lookup(self, name):
        # lookup a symbol by its name, not sure what to return_statement
        for sym in self.symbols:
            if sym.name == name:
                return True
        if self.parent != None:
            return self.parent.lookup(name)
        else:
            raise VariableNotDeclaredException(name)

    def checkCurrentScope(self, varname):
        for sym in self.symbols:
            if ((sym.name == varname) and (not sym.m_forwardDecl)):
                return True
        return False

    def show(self):
        print "[ST = %s]" % self.context
        for s in self.symbols:
            print str(s)
        for c in self.children:
            c.show()

    def lookuptype(self,name):
        for sym in self.symbols:
            if sym.name == name:
                return sym.m_type
        if self.parent != None:
            return self.parent.lookuptype(name)
        else:
            raise VariableNotDeclaredException(name)

    def lookupsymbol(self, sname):
        for sym in self.symbols:
            if sym.name == sname:
                return sym
        if self.parent != None:
            return self.parent.lookupsymbol(sname)
        else:
            raise VariableNotDeclaredException(sname)

    def lookupparcount(self, sname):
        for sym in self.symbols:
            if sym.name == sname:
                return sym.parcount
        if self.parent != None:
            return self.parent.lookupparcount(sname)
        else:
            raise VariableNotDeclaredException(sname)

    def lookuparraysize(self,sname):
        for sym in self.symbols:
            if sym.name == sname:
                return sym.m_arraysize
        if self.parent != None:
            return self.parent.lookuparraysize(sname)
        else:
            raise VariableNotDeclaredException(sname)

    def existsforwarddecl(self, fname):
        for sym in self.symbols:
            if(sym.name == fname):
                if(sym.m_forwardDecl):
                    return sym
        if self.parent != None:
            return self.parent.existsforwarddecl(fname)
        else:
            raise VariableNotDeclaredException(fname)

    def checkfordefinition(self, name):
        for sym in self.symbols:
            if sym.name == name and sym.attribute == "m_fun_decl":
                return True
        return False

    def allfwdeclsdefined(self):
        l = []
        for sym in self.symbols:
            if sym.attribute == "m_forward":
                l.append(sym.name)
        for pl in l:
            if not self.checkfordefinition(pl):
                return False
        return True
