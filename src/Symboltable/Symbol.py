# class Kind(Enum):
#     m_fun = 1
#     m_var = 2
#     m_par = 3
#
# class BaseType(Enum):
#     m_int = 1
#     m_char = 2
#     m_bool = 3
#     m_float = 4
#
#
# class Attribute(Enum):
#     m_none = 1
#     m_const = 2


class Symbol(object):

    def __hash__(self):
        return 1

    def __eq__(self, other):
        return (self.name == other.name)

    def __cmp__(self, other):
        return (self.name == other.name)

    def __init__(self, name="_default_", mtype="_default_", attribute="_default_", kind="_default_"):
        self.name = name
        self.m_type = mtype
        self.attribute = attribute
        self.kind = kind
        # Should find a more elegant solution, these types are only to be used
        # When the Entry is a method
        self.retval = None
        self.paramlist = []
        self.m_forwardDecl = False
        self.parcount = 0

    def __str__(self):
        return "[" + self.name + "," + self.m_type + "," + self.attribute + "," + self.kind + "]"

class FunctionSymbol(Symbol):

    def __hash__(self):
        return 1

    def __eq__(self, other):
        return (self.name == other.name)

    def __cmp__(self, other):
        return (self.name == other.name)

    def __init__(self, name="_default_", mtype="m_fun", attribute="_default_", kind="_default_"):
        self.name = name
        self.m_type = mtype
        self.attribute = attribute
        self.kind = kind
        # Should find a more elegant solution, these types are only to be used
        # When the Entry is a method
        self.retval = None
        self.paramlist = []
        self.m_parnamelist = []
        self.parcount = 0
        self.m_forwardDecl = False

    def __str__(self):
        return "[" + self.name + "," + self.m_type + "," + self.attribute + "," + self.kind + "]"

class ArraySymbol(Symbol):

    def __hash__(self):
        return 1

    def __eq__(self, other):
        return (self.name == other.name)

    def __cmp__(self, other):
        return (self.name == other.name)

    def __init__(self, name="_default_", mtype="m_fun", attribute="_default_", kind="_default_"):
        self.name = name
        self.m_type = mtype
        self.attribute = attribute
        self.kind = kind
        # Should find a more elegant solution, these types are only to be used
        # When the Entry is a method
        self.m_arraysize = 0
        self.m_forwardDecl = False

    def __str__(self):
        return "[" + self.name + "," + self.m_type + "," + self.attribute + "," + self.kind + "]"
