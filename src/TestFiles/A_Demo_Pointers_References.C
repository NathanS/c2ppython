#include <stdio.h>

int sumInt(int* a, int* b) {
	return *a + *b;
}
int sum2(int a, int b) {
	return a + b;
}

int main(){
  int a2 = 4;
	int b2 = 5;
	int *ptr = &a2;
	int *qtr = &b2;
	int jill;
	jill = sumInt(&a2,&b2);
  printf("Output after sumInt:%i\n", jill);
	jill = sumInt(ptr,qtr);
  printf("Output after sumInt:%i\n", jill);
	jill = sum2(a2,b2);
  printf("Output after sumInt:%i\n", jill);
	jill = sum2(*ptr,*qtr);
  printf("Output after sumInt:%i\n", jill);
}
