#include <stdio.h>
 
int factorial(int a);
 
int main() {
    int i;
    int n;
    int c;
    
    printf("Enter the number of rows you wish to see in pascal triangle\n");
    scanf("%i",&n);
              
    for (i = 0; i < n; i++) {
        for (c = 0; c <= (n - i - 2); c++) printf(" ");
        for (c = 0 ; c <= i; c++) {
            printf("%i ",factorial(i)/(factorial(c)*factorial(i-c)));
        }
        printf("\n");
    }
    return 0;
}
 
int factorial(int n) {
    int result = 1;
           
    for (int c = 1; c <= n; c++) result = result*c;
              
    return result;
}
