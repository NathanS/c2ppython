#include <stdio.h>


int main(){
  bool m_true = true;
  bool m_false = false;

  if(m_true){
    printf("Condition is true - Booleans are functional!\n");
  }
  if(!m_true){
    printf("Condition is false - Booleans are functional once more!\n");
  }
  if(m_false){
    printf("Condition is false - Booleans are functional!\n");
  }
  if(!m_false){
    printf("Condition is true - Booleans are functional!\n");
  }
}
