#include <stdio.h>

float mult(float a, float b);

int sumInt(int* a, int* b) {
	return *a + *b;
}
int sum2(int a, int b) {
	return a + b;
}


int sum(int a, int b){
  //printf("Calculating the sum of: %i and %i.\n", a, b);
  int result = 0;
  result = a + b;
  return result;
}

int magic(int a, char b, float c){
  if( a > 5){
    return 5;
  }
  else{
    return 1;
  }
}

int total(int a[10]){
	return a[0] + a[1] + a[2] + a[3] + a[4]+ a[5]+ a[6]+ a[7]+ a[8]+ a[9];
}

int total2(int a[], int size){
	int result = 0;
	for(int i = 0; i < size; i = i+1){
		result = result + a[i];
	}
	return result;
}

int main(){
  char a[50];
  int b[10] = {1,2,3,4,5,6,7,8,9,10};
  float c[5] = {1.2, 1.3, 1.4, 1.5, 2000.0};
  int i = 10;

  printf("Please input a string!\n");
  scanf("%s", a);
  printf("I'm reading your string back to you! %s\n", a);

  printf("The result of multiplying %f and %f is %f.\n", c[1], c[4], mult(c[1], c[4]));
  printf("The result of adding %i and %i is %i.\n", b[1], b[4], sum(b[1], b[4]));

  while(i > 5){
    i = i - 1;
    printf("You still don't meet the requirements to exit the while-loop!\n");
  }
  printf("You met the requirements to exit the while-loop!\n");


	printf("Entering a For loop!\n");
  for(int k = 0;k < 10; k = k+1){
    printf("Another Iteration!\n");
    printf("Incrementing k each time. K is now: %i\n", k);
  }

  int a2 = 4;
	int b2 = 5;
	int *ptr = &a2;
	int *qtr = &b2;
	int jill;
	jill = sumInt(&a2,&b2);
  printf("Output after sumInt:%i\n", jill);
	jill = sumInt(ptr,qtr);
  printf("Output after sumInt:%i\n", jill);
	jill = sum2(a2,b2);
  printf("Output after sumInt:%i\n", jill);
	jill = sum2(*ptr,*qtr);
  printf("Output after sumInt:%i\n", jill);

	int counter = 0;
	while(true){
		printf("This loop is always True! Hopefully we can break free.\n");
		counter = counter + 1;
		if(counter == 3){
			printf("Counter reached the value 3. Breaking free!\n");
			break;
		}
	}
	printf("Total of B: %i\n", total(b));
	printf("Total of B: %i\n", total2(b,10));

	bool m_true = true;
	bool m_false = false;

	if(m_true){
		printf("Booleans are functional!\n");
	}
	if(!m_false){
		printf("Booleans are functional once more!\n");
	}

}

float mult(float a, float b){
  //printf("Calculating the product of: %f and %f.\n", a, b);
  float result = 0.0;
  result = a * b;
  return result;
}
