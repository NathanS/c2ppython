import sys, getopt
import pickle

from Compiler.Compiler import Compiler


# c2p.py FLAGS FILES
def main(argv):
    # -saveast is the only supported flag
    save = False
    flag = ""
    loadfile = ""
    load = False

    # Handle args
    if "-l" in argv or "-loadast" in argv:
        filetoparse = "Dummy1"
        filetowrite = "Dummy2"
        loadfile = argv[1]
        load = True
    else:
        filetoparse = argv[0]
        filetowrite = argv[1]

    if "-s" in argv or "-saveast" in argv:
        save = True


    # Do actual compilation


    c = Compiler(filetoparse, filetowrite, save, load, loadfile)
    c.compile(verbose = False)


if __name__ == "__main__":
    main(sys.argv[1:])
