echo "Generating all files required for parsing with ANTLR4."
cd Grammar
java -jar antlr-4.5.2-complete.jar -Dlanguage=Python2 TinyC.g4 -visitor
cd ..
echo "Generated all files required for the C to P Compiler with ANTLR4."
