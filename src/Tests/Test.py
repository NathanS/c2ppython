from Compiler.Compiler import Compiler
import sys, filecmp, os

def cmp_lines(path_1, path_2):
    l1 = l2 = ' '
    with open(path_1, 'Ur') as f1, open(path_2, 'Ur') as f2:
        while l1 != '' and l2 != '':
			l1 = f1.readline()
			l2 = f2.readline()
			if l1 != l2:
				return False
	return True

class Test(object):

    def __init__(self, name, inputfile, expectedoutput):
        inputfile = os.path.join("Tests", inputfile)
        expectedoutput = os.path.join("Tests", expectedoutput)
        self.name = name
        self.inputfile = inputfile

        self.expectedoutput = expectedoutput

    def getName(self):
        return self.name

    def getInput(self):
        return self.inputfile

    def getOutput(self):
        return self.expectedoutput

    def execute(self):
		# c = Compiler(self.getInput(), self.getOutput()+".p")
		# temp = sys.stdout
		# f = open('test.out', 'Ur')
		# sys.stdout = f
		# c.compile(verbose = False)
		# sys.stdout.close()
		# sys.stdout = temp
		# return filecmp.cmp('test.out', self.getOutput())
		c = Compiler(self.getInput(), self.getOutput()+".p")
		temp = sys.stdout
		f = open('test.out', 'w')
		sys.stdout = f
		c.compile(verbose = False)
		sys.stdout = temp
		f.close()
		f = open('test.out', 'Ur')
		return cmp_lines('test.out', self.getOutput())

class PcodeTest(object):

    def __init__(self, name, inputfile, expectedoutput):
        inputfile = os.path.join("Tests", inputfile)
        expectedoutput = os.path.join("Tests", expectedoutput)
        self.name = name
        self.inputfile = inputfile
        self.expectedoutput = expectedoutput

    def getName(self):
        return self.name

    def getInput(self):
        return self.inputfile

    def getOutput(self):
        return self.expectedoutput

    def execute(self):
        return cmp_lines(self.getInput(), self.getOutput())
