class Case(object):

    def __init__(self, name, tests):
        self.name = name
        self.tests = tests
    
    def getName(self):
        return self.name
    
    def getTests(self):
        return self.tests
    
    def size(self):
        return len(self.tests)
        

        