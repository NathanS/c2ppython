import sys, os, platform
import pickle

from antlr4 import *
from antlr4.tree.Trees import Trees

from Exceptions.CustomExceptions import *
from Grammar.MyTinyCListener import MyTinyCListener
from Grammar.MyTinyCVisitor import MyTinyCVisitor
from Grammar.TinyCLexer import TinyCLexer
from Grammar.TinyCParser import TinyCParser
from Symboltable.SymbolTable import *
from Util import TreePrinter
from Util.TreePrinter import typePrinter


class Compiler(object):

    def __init__(self, filefrom, fileto, save = False, load = False, loadfile = None, verbose = False, show = False):
        # From = C File "from" to compile into P File "to"
        self.sourcefile = filefrom
        self.outputfile = fileto
        self.m_symboltable = SymbolTable()
        self.m_AST = None
        self.flag_save_AST = save
        self.flag_verbose = verbose
        self.flag_ShowASTImage = show
        self.flag_load = load
        self.flag_loadfile = loadfile


    def saveAST(self, filename):
        with open(filename, 'wb') as output:
            pickle.dump(self.m_AST, output, pickle.HIGHEST_PROTOCOL)

    def loadAST(self, filename):
        with open(filename, 'rb') as _input:
            p = pickle.load(_input)
            return p


    def compile(self, verbose):
        if not self.flag_load:
            try:
                m_input = FileStream(self.sourcefile)
            except IOError as e:
                print "The file you specified: %s, could not be found. Terminating." % (self.sourcefile)
                sys.exit(1)
            lexer = TinyCLexer(m_input)
            stream = CommonTokenStream(lexer)

            #Antlr4 uses stderr to report Errors from within the parser
            #We write them to a file, and check the file to ensure whether
            #We need to start compiling or Not

            opfile = self.sourcefile + "P" ".out"#test.C.out
            temp = sys.stderr
            f = open(opfile, 'w')
            sys.stderr = f

            try:
                parser = TinyCParser(stream)
                tree = parser.parse()

                f.close()
                #Redirect the stderr because Antlr4 uses this to
                #Report Parsing errors with its own ErrorHandler
                sys.stderr = temp
                if os.stat(opfile).st_size > 0:
                    raise TinyCParserException()
            except TinyCParserException as Tpe:
                    f = open(opfile, 'r')
                    print "Antlr4 Parser encountered the following errors:"
                    print f.read()
                    f.close()
                    os.remove(opfile)
                    pass
            else:
                if verbose:
                    print "This Is the ParseTree:"
                    print(Trees.toStringTree(tree, None, parser))
                #Calling system to remove the file
                os.remove(opfile)
                #Reset the stderr output
                sys.stderr = temp
                listener = MyTinyCListener()
                walker = ParseTreeWalker()
                try:
                    walker.walk(listener, tree)
                except TerminationException:
                    pass
                else:
                    TreePrinter.generateImage(listener.root, self.sourcefile)
                    print "Compilation Terminated without Errors."
                    if verbose:
                        print "Compilation Finished, showing ST"
                        listener.globalst.show()
                    try:
                        #outpfile = open(self.outputfile + ".pcode","w")
                        outpfile = open(self.outputfile, "w")
                    except:
                        print "Found an Error. Autobots out."
                        sys.exit(1)
                    else:
                        self.m_AST = listener.root
                        if verbose:
                            #TreePrinter.typePrinter(self.m_AST)
                            pass
                        self.initCodeGen(outpfile)
        else:
            self.m_AST = self.loadAST(self.flag_loadfile)
            self.initCodeGen(open("LoadedTree.p", "wr"))
    def initCodeGen(self, fileStream):
        #filestream has already been opened
        try:
            #self.m_AST is the root of the generated AST
            if self.flag_save_AST:
                self.saveAST(self.sourcefile + "_AST.pkl")
                #self.m_AST = self.loadAST(self.sourcefile+ "_AST.pkl")
            self.m_AST.generateCode(fileStream)
        except pickle.PickleError as e:
            print "I caught an error, lets hope it's not a pickle one."
            print e
        else:
            fileStream.close()
